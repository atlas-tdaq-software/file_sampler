
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DATALEN (sizeof(unsigned long))

int main (int argc, char** argv)
  {
    int fdr, fdw;

    long bc, ns;
    unsigned long data;

    if ( argc != 3 )
      {
	printf ( "usage: %s <filein> <fileout>\n", argv[0]);
	exit (1);
      }
    fdr = open ( argv[1], O_RDONLY);
    fdw = open ( argv[2], O_WRONLY | O_CREAT, 00644);
    ns = -1;
    bc = -1;
    if ((fdr != -1) && (fdw != -1))
      {
	      size_t nr;
	      do {
		ns ++;
	        nr = read(fdr, &data, DATALEN);
	      } while ((nr == DATALEN) && (data != 0xaa1234aa));
	      ns *= DATALEN;
	      if (nr == DATALEN)
		{
		  bc ++; write(fdw, &data, DATALEN);
		} else ns += nr;
	      do {
		bc ++;
		if (bc < 100) printf("%lx ", data);
	        nr = read(fdr, &data, DATALEN);
		write(fdw, &data, nr);
	      } while (nr == DATALEN);
	      bc *= DATALEN;
	      bc += nr;
	      bc += ns;
      }
    close(fdr);
    close(fdw);
    printf("got %ld skipped %ld wrote %ld bytes\n", bc, ns, bc-ns);
    exit(0);
  }
