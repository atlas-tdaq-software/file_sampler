
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DATALEN (sizeof(unsigned long))

int main (int argc, char** argv)
{
    int fdr, fdw;
    unsigned char level, selected;
    unsigned long nr, nw, marker, evnr;
    if ( argc < 3 )
    {
	printf ( "usage: %s <filein> <fileout> [<LEVEL (9ABCDE)>]\n", argv[0]);
	exit (1);
    }
    fdr = open ( argv[1], O_RDONLY);
    fdw = open ( argv[2], O_WRONLY | O_CREAT, 00644);
    level = (argc > 3) ? argv[3][0] : 'a';
    switch (level)
    {
    case '9':		selected = 0; marker = 0x99123499; break;
    case 'a': case 'A':	selected = 1; marker = 0xaa1234aa; break;
    case 'b': case 'B':	selected = 2; marker = 0xbb1234bb; break;
    case 'c': case 'C':	selected = 3; marker = 0xcc1234cc; break;
    case 'd': case 'D':	selected = 4; marker = 0xdd1234dd; break;
    case 'e': case 'E':	selected = 5; marker = 0xdd1234dd; break;
    default:		selected = 1; marker = 0xaa1234aa; break;
    }
    printf("level %c - selected %d marker %lx\n", level, selected, marker);
    nr = 0;
    nw = 0;
    evnr = 0;
    if ((fdr != -1) && (fdw != -1))
    {
	size_t ng;
	unsigned long data, evsize, k, i, j, nelem, source_id;
	unsigned long dummy, ndummies, frgsize;
	unsigned long * offsets, * sizes, ads;
	do
        {
	    do
            {
		nr += (ng = read(fdr, &data, DATALEN));
            }
	    while ((ng == DATALEN) && (data != marker));
	    if (ng == DATALEN) {
                if (selected == 5)
                {
                    evnr ++;
                    nr += (ng = read(fdr, &frgsize, DATALEN));
                    nr += (ng = read(fdr, &dummy, DATALEN));
                    nr += (ng = read(fdr, &dummy, DATALEN));
                    nr += (ng = read(fdr, &dummy, DATALEN));
                    nr += (ng = read(fdr, &ndummies, DATALEN));
                    for ( ; ndummies; ndummies--)
                        nr += (ng = read(fdr, &dummy, DATALEN));
                    nr += (ng = read(fdr, &nelem, DATALEN));

                    offsets = malloc( (nelem+1) * DATALEN);
                    sizes = malloc( nelem * DATALEN);

                    for (i=0; i<nelem; i++)
                    {
                        nr += (ng = read(fdr, &dummy, DATALEN));
                        offsets[i] = dummy;
                    }
                    offsets[nelem] = frgsize;
                    for (i=0; i<nelem; i++)
                        sizes[i] = offsets[i+1] - offsets[i];
                    nr += (ng = read(fdr, &ndummies, DATALEN));
                    for ( ; ndummies; ndummies--)
                        nr += (ng = read(fdr, &dummy, DATALEN));
                    ads = nr/DATALEN;
                    for (i=0; i<nelem; i++)
                    {
                        for (j=0; j<4; j++)
                        {
                            nr += (ng = read(fdr, &data, DATALEN));
                            nw += write(fdw, &data, ng);
                        }
                        source_id = data;
                        printf("%lu ads %lu size %lu source 0x%lx\n",
                               evnr, ads, sizes[i], source_id);
                        ads += sizes[i];
                        for (; j<sizes[i]; j++)
                        {
                            nr += (ng = read(fdr, &data, DATALEN));
                            nw += write(fdw, &data, ng);
                        }
                    }
                    free( sizes);
                    free( offsets);
                }
            } else {
		evnr ++;
		ads = nr/DATALEN; ads --;
		nw += write(fdw, &data, ng);
		nr += (ng = read(fdr, &evsize, DATALEN));
		nw += write(fdw, &evsize, ng);
		for (k = 2; k < 5; k++)
                {
		    nr += (ng = read(fdr, &data, DATALEN));
		    nw += write(fdw, &data, ng);
                }
		source_id = data;
		printf("%lu ads %lu size %lu source 0x%lx\n",
                       evnr, ads, evsize, source_id);
		for (; (k < evsize) && (ng == DATALEN); k++)
                {
		    nr += (ng = read(fdr, &data, DATALEN));
		    nw += write(fdw, &data, ng);
                }
            }
        }
	while (ng == DATALEN);
    }
    close(fdr);
    close(fdw);
    printf("got %lu skipped %lu wrote %lu bytes\n", nr, nr-nw, nw);
    exit(0);
}
