
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DATALEN (sizeof(unsigned long))

int main (int argc, char** argv)
  {
    int fdr, fdw;
    unsigned char level;
    unsigned long nr, nw, marker;
    if ( argc < 3 )
      {
	printf ( "usage: %s <filein> <fileout> [<LEVEL (9ABCDE)>]\n", argv[0]);
	exit (1);
      }
    fdr = open ( argv[1], O_RDONLY);
    fdw = open ( argv[2], O_WRONLY | O_CREAT, 00644);
    level = (argc == 4) ? argv[3][0] : 'a';
    switch (level)
      {
	case '9':		marker = 0x99123499; break;
	case 'a': case 'A':	marker = 0xaa1234aa; break;
	case 'b': case 'B':	marker = 0xbb1234bb; break;
	case 'c': case 'C':	marker = 0xcc1234cc; break;
	case 'd': case 'D':	marker = 0xdd1234dd; break;
	case 'e': case 'E':	marker = 0xee1234ee; break;
	default:		marker = 0xaa1234aa; break;
      }
    printf("level is %c marker %lx\n", level, marker);
    nr = 0;
    nw = 0;
    if ((fdr != -1) && (fdw != -1))
      {
	size_t ng;
	unsigned long data, evsize, k;
	do
	  {
	    do
	      {
		nr += (ng = read(fdr, &data, DATALEN));
	      }
	    while ((ng == DATALEN) && (data != marker));
	    if (ng == DATALEN)
	      {
		nw += write(fdw, &data, ng);
		nr += (ng = read(fdr, &evsize, DATALEN));
		nw += write(fdw, &evsize, ng);
		for (k = 2; (k < evsize) && (ng == DATALEN); k++)
		  {
		    nr += (ng = read(fdr, &data, DATALEN));
		    nw += write(fdw, &data, ng);
		  }
	      }
	  }
	while (ng == DATALEN);
      }
    close(fdr);
    close(fdw);
    printf("got %lu skipped %lu wrote %lu bytes\n", nr, nr-nw, nw);
    exit(0);
  }
