#ifndef _DREAM_DAQ_FILE_THREAD_H_
#define _DREAM_DAQ_FILE_THREAD_H_

#include <cstring>
#include <cstdio>
#include <pthread.h>
#include <iostream>
#include <fstream>
#include <string>
#include <list>

#define BUFSIZE 65536

class fileThread
 {
  public:
   fileThread(const char* argv[], int sig);
   ~fileThread();
   void* getstdout();
   void* getstderr();
   int fflush();
   int putstring(std::string s);
   int ostring(std::string& s);
   int estring(std::string& s);
   std::string whoami();
   void ctrlc();
   bool waitMessage(const char* msg);
   int checkOut();

  private:
   std::string m_name;
   int m_sig;
   int m_pid;
   int m_pipe[3];
   std::ofstream m_fout, m_ferr;
   std::string fname_out, fname_err;
   char* bf_out, * bf_err;
   size_t sz_out, sz_err;
   std::string o_carry, e_carry;

   pthread_t m_thout, m_therr;
   pthread_mutex_t m_mtout, m_mterr;

   std::list<std::string> m_olist, m_elist;

   void stop();

 };

#endif // _DREAM_DAQ_FILE_THREAD_H_
