
#include <cstdlib>
#include <sstream>
#include <boost/regex.hpp>
#include "fileThread.h"

int popenRWE(int *rwepipe, const char *exe, const char *const argv[]);
int pcloseRWE(int pid, int *rwepipe);
int ptermRWE(int pid);
int pintRWE(int pid);
int psigRWE(int pid, int sig);

extern "C" void* f_throut(void* args)
 {
  fileThread* sthr = (fileThread*)args;
  return sthr->getstdout();
 }

extern "C" void* f_threrr(void* args)
 {
  fileThread* sthr = (fileThread*)args;
  return sthr->getstderr();
 }

fileThread::fileThread( const char* argv[], int sig )
 {
  m_sig = sig;

  sz_out = BUFSIZE;
  sz_err = BUFSIZE;
  bf_out = (char*)malloc(sz_out);
  bf_err = (char*)malloc(sz_err);
  o_carry = "";
  e_carry = "";

  pthread_mutex_init ( &m_mtout, NULL ); 
  pthread_mutex_init ( &m_mterr, NULL ); 

  pthread_attr_t attr;
  pthread_attr_init ( &attr );
  pthread_attr_setdetachstate ( &attr, PTHREAD_CREATE_JOINABLE );

  m_pid = popenRWE( m_pipe, argv[0], argv );

  std::ostringstream pid2str;
  pid2str << m_pid;
  m_name = std::string(argv[0])+"_"+pid2str.str();

  fname_out = m_name + ".out";
  m_fout.open(fname_out.c_str()); // , std::fstream::out);
  // m_fout.close();
  fname_err = m_name + ".err";
  m_ferr.open(fname_err.c_str()); // , std::fstream::out);
  // m_ferr.close();

  // m_fout = fdopen(m_pipe[1], "r");
  // m_ferr = fdopen(m_pipe[2], "r");

  int outstt = pthread_create ( &m_thout, &attr, f_throut, (void*)this );
  int errstt = pthread_create ( &m_therr, &attr, f_threrr, (void*)this );

  pthread_attr_destroy ( &attr );

  if ( outstt < 0 ) throw outstt;
  if ( errstt < 0 ) throw errstt;
 }

fileThread::~fileThread()
 {
  this->stop();
  // fclose(m_fout);
  // fclose(m_ferr);
  pcloseRWE( m_pid, m_pipe );
  free(bf_out);
  free(bf_err);
  pthread_mutex_destroy ( &m_mtout );
  pthread_mutex_destroy ( &m_mterr );
  m_fout.close();
  m_ferr.close();
  m_olist.clear();
  m_elist.clear();
 }

void* fileThread::getstdout()
 {
  while(1)
   {
    pthread_testcancel();
    // ssize_t nc = getline( &bf_out, &sz_out, m_fout);
    ssize_t nc = read( m_pipe[1], bf_out, sz_out );
    if (nc > 0) 
     {
      if (bf_out[nc] != '\0')
       {
        bf_out[nc] = '\0';
// std::cout << m_name << " stdout read gave " << nc << " bytes non-zero terminated\n";
// std::cout << m_name << " stdout read gave [" << bf_out << "]\n";
       }
      char* beg = bf_out;
      char* end = index( beg, '\n');
      while (end != NULL)
       {
        size_t len = end-beg;
        std::string s = o_carry + std::string(beg, len);
        pthread_mutex_lock(&m_mtout);
        m_olist.push_back(s);
        o_carry = "";
        pthread_mutex_unlock(&m_mtout);
        beg = end+1;
        end = index( beg, '\n');
       }
      if (beg != index(bf_out, '\0'))
       {
        pthread_mutex_lock(&m_mtout);
        o_carry += bf_out;
        pthread_mutex_unlock(&m_mtout);
       }
     }
    else
     {
      pthread_testcancel();
      usleep(10000);
     }
   }
  return NULL;
 }
  
void* fileThread::getstderr()
 {
  while(1)
   {
    pthread_testcancel();
    // ssize_t nc = getline( &bf_err, &sz_err, m_ferr);
    ssize_t nc = read( m_pipe[2], bf_err, sz_err );
    if (nc > 0) 
     {
      if (bf_err[nc] != '\0')
       {
        bf_err[nc] = '\0';
// std::cout << m_name << " stderr read gave " << nc << " bytes non-zero terminated\n";
// std::cout << m_name << " stderr read gave [" << bf_err << "]\n";
       }
      char* beg = bf_err;
      char* end = index( beg, '\n');
      while (end != NULL)
       {
        size_t len = end-beg;
        std::string s = e_carry + std::string(beg, len);
        pthread_mutex_lock(&m_mterr);
        m_olist.push_back(s);
        e_carry = "";
        pthread_mutex_unlock(&m_mterr);
        beg = end+1;
        end = index( beg, '\n');
       }
      if (beg != index(bf_err, '\0'))
       {
        pthread_mutex_lock(&m_mterr);
        e_carry += bf_err;
        pthread_mutex_unlock(&m_mterr);
       }
     }
    else
     {
      pthread_testcancel();
      usleep(1000);
     }
   }
  return NULL;
 }

void fileThread::ctrlc()
 {
  pintRWE( m_pid );
 }

void fileThread::stop()
 {
  // ptermRWE( m_pid );
  psigRWE( m_pid, m_sig );
  usleep(100000);
  
  if (m_thout != pthread_t(0))
   {
    pthread_cancel ( m_thout );
    pthread_join ( m_thout, NULL );
    m_thout = pthread_t(0);
   }
  if (m_therr != pthread_t(0))
   {
    pthread_cancel ( m_therr );
    pthread_join ( m_therr, NULL );
    m_therr = pthread_t(0);
   }
 }

int fileThread::fflush()
 {
  pthread_mutex_lock(&m_mtout);
  m_olist.clear();
  pthread_mutex_unlock(&m_mtout);
  pthread_mutex_lock(&m_mterr);
  m_elist.clear();
  pthread_mutex_unlock(&m_mterr);
  return 0;
 }

int fileThread::ostring(std::string& s)
 {
  if (m_olist.size())
   {
    pthread_mutex_lock(&m_mtout);
    s = m_olist.front();
    m_olist.pop_front();
    pthread_mutex_unlock(&m_mtout);
    // m_fout.open(fname_out.c_str(), std::fstream::ate);
    m_fout << "[" << s << "]" << std::endl;
    m_fout.flush();
    // m_fout.close();
    return 0;
   }
  return -1;
 }

int fileThread::estring(std::string& s)
 {
  if (m_elist.size())
   {
    pthread_mutex_lock(&m_mterr);
    s = m_elist.front();
    m_elist.pop_front();
    pthread_mutex_unlock(&m_mterr);
    // m_ferr.open(fname_err.c_str(), std::fstream::ate);
    m_ferr << "[" << s << "]" << std::endl;
    m_ferr.flush();
    // m_ferr.close();
    return 0;
   }
  return -1;
 }

int fileThread::putstring(std::string s)
 {
  s += '\n';
  return write ( m_pipe[0], s.c_str(), s.length());
 }

std::string fileThread::whoami()
 {
  return m_name;
 }

bool fileThread::waitMessage(const char* msg)
 {
  int ret;
  const boost::regex rgx ( msg );
  boost::cmatch w;
  bool b;
  do
   {
    std::string s;
    ret = this->ostring(s);
    b = (ret==0) ? boost::regex_match ( s.c_str(), w, rgx ) : false;
   }
  while((ret==0) && (!b));
  return b;
 }

int fileThread::checkOut()
 {
  int ret;
  do
   {
    std::string s;
    ret = this->ostring(s);
   }
  while(ret==0);
  do
   {
    std::string s;
    ret = this->estring(s);
   }
  while(ret==0);
  return 0;
 }

