
#include <cstdlib>
#include <csignal>
#include <cstdint>
#include "fileThread.h"

volatile sig_atomic_t exit_now(0);

int usleep( uint32_t usec)
 {
  struct timespec wt;
  time_t sc = usec/1000000;
  long usc = usec%1000000;
  wt.tv_sec = sc;
  wt.tv_nsec = usc * 1000;
  return nanosleep ( &wt, NULL );
 }

void sghandler(int sig)
{
  exit_now = 1;
  signal( sig, sghandler );
}

#include <sys/select.h>
#include <cstdio>

int kbhit(void)
 {
  struct timeval tv;
  fd_set read_fd;

  /* Do not wait at all, not even a microsecond */
  tv.tv_sec=0;
  tv.tv_usec=0;

  /* Must be done first to initialize read_fd */
  FD_ZERO(&read_fd);

  /* Makes select() ask if input is ready:
   * 0 is the file descriptor for stdin */
  FD_SET(0,&read_fd);

  /* The first parameter is the number of the
   * largest file descriptor to check + 1. */
  if (select(1, &read_fd, NULL, NULL, &tv) == -1)
    return 0; // An error occured

  /* read_fd now holds a bit map of files that are
   * readable. We test the entry for the standard
   * input (file 0). */
  if (FD_ISSET(0,&read_fd)) /* Character pending on stdin */
    return 1;

  /* no characters were pending */
  return 0;
 }

int main( int /* argc */ , char** /* argv */ )
 {
  char* partname = getenv("TDAQ_PARTITION");
  char* ohserv = getenv("GNAM_OH_SERVER");
  char* smplnm = getenv("GNAM_SAMPLER_NAME");
  char* gnamnm = getenv("GNAM_PROC_NAME");
  if (partname == NULL)
   {
    std::cout << "${TDAQ_PARTITION} variable not set\n";
    exit(1);
   }
  if (ohserv == NULL)
   {
    std::cout << "${GNAM_OH_SERVER} variable not set\n";
    exit(1);
   }
  if (smplnm == NULL)
   {
    std::cout << "${GNAM_SAMPLER_NAME} variable not set\n";
    exit(1);
   }
  if (gnamnm == NULL)
   {
    std::cout << "${GNAM_PROC_NAME} variable not set\n";
    exit(1);
   }

  // std::cout << "Partition is " << partname << std::endl;
  // std::cout << "OH Server is " << ohserv << std::endl;
  // std::cout << "Sampler name is " << smplnm << std::endl;
  // std::cout << "Gnam name is " << gnamnm << std::endl;

  // if (argc == 1) exit(1);
  signal ( SIGTERM, sghandler );
  signal ( SIGINT, sghandler );
  signal ( SIGQUIT, sghandler );

  std::string cmd1[1] = { "ipc_server" };
  std::cout << "starting process: \'" << cmd1[0] << "\'\n";
  const char* argv1[2] = { cmd1[0].c_str(), NULL };
  fileThread* ipcsrvm = new fileThread(argv1, SIGHUP);
  // usleep(2000000);
  std::string wstr("(.*) ipc_server for partition \"initial\" has been started.");
  while (!ipcsrvm->waitMessage(wstr.c_str())) usleep(10000);
  usleep(10000);
  std::cout << " --> process running\n";

  std::string cmd2[3] = { "ipc_server", "-p", partname };
  std::cout << "starting process: \'" << cmd2[0] << " " << cmd2[1] << " " << cmd2[2] << "\'\n";
  const char* argv2[4] = { cmd2[0].c_str(), cmd2[1].c_str(), cmd2[2].c_str(), NULL };
  fileThread* ipcsrvp = new fileThread(argv2, SIGHUP);
  // usleep(2000000);
  // ipcsrvp->checkOut();
  wstr = "(.*) ipc_server for partition \"" + std::string(partname) + "\" has been started.";
  while (!ipcsrvp->waitMessage(wstr.c_str())) usleep(10000);
  usleep(10000);
  std::cout << " --> process running\n";

  std::string cmd3[3] = { "emon_conductor", "-p", partname };
  std::cout << "starting process: \'" << cmd3[0] << " " << cmd3[1] << " " << cmd3[2] << "\'\n";
  const char* argv3[4] = { cmd3[0].c_str(), cmd3[1].c_str(), cmd3[2].c_str(), NULL };
  fileThread* emonc = new fileThread(argv3, SIGINT);
  // usleep(2000000);
  // emonc->checkOut();
  wstr = "(.*) Emon conductor for partition \"" + std::string(partname) + "\" has been started.";
  while (!emonc->waitMessage(wstr.c_str())) usleep(10000);
  usleep(10000);
  std::cout << " --> process running\n";

  std::string cmd4[5] = { "is_server", "-p", partname, "-n", ohserv };
  std::cout << "starting process: \'" << cmd4[0] << " " << cmd4[1] << " " << cmd4[2] << " " << cmd4[3] << " " << cmd4[4] << "\'\n";
  const char* argv4[6] = { cmd4[0].c_str(), cmd4[1].c_str(), cmd4[2].c_str(),
                           cmd4[3].c_str(), cmd4[4].c_str(), NULL };
  fileThread* ohsrv = new fileThread(argv4, SIGINT);
  // usleep(2000000);
  // ohsrv->checkOut();
  wstr = "(.*) the '" + std::string(ohserv) +
         "' IS server started in the partition '" + std::string(partname) + "'";
  while (!ohsrv->waitMessage(wstr.c_str())) usleep(10000);
  usleep(10000);
  std::cout << " --> process running\n";

  std::string cmd5[5] = { "gnamSampler", "-i", "-x", "-n", smplnm };
  std::cout << "starting process: \'" << cmd5[0] << " " << cmd5[1] << " " << cmd5[2] << " " << cmd5[3] << " " << cmd5[4] << "\'\n";
  const char* argv5[6] = { cmd5[0].c_str(), cmd5[1].c_str(), cmd5[2].c_str(),
                           cmd5[3].c_str(), cmd5[4].c_str(), NULL };
  fileThread* gsamplr = new fileThread(argv5, SIGTERM);
  usleep(10000);
  std::cout << " --> process running\n";

  std::string cmd6[4] = { "gnam", "-i", "-n", gnamnm };
  std::cout << "starting process: \'" << cmd6[0] << " " << cmd6[1] << " " << cmd6[2] << " " << cmd6[3] << "\'\n";
  const char* argv6[5] = { cmd6[0].c_str(), cmd6[1].c_str(), cmd6[2].c_str(),
                           cmd6[3].c_str(), NULL };
  fileThread* gnamproc = new fileThread(argv6, SIGINT);
  usleep(10000);
  std::cout << " --> process running\n";

  time_t tstart(time(NULL));
  while(1)
   {
    time_t tnow;
    if (exit_now != 0) break;
    tnow = time(NULL);
    char buff[65536];
    if (kbhit())
     {
      tstart = tnow;
      std::string s;
      std::cin.getline(buff,65536);
std::cout << "[" << buff << "]\n";
      if (strcasecmp(buff,"Quit") == 0) break;
      gnamproc->putstring(std::string(buff));
     }
    if (tnow >= tstart + 120)
     {
      tstart = tnow;
      gnamproc->putstring(std::string("user"));
     }
    gnamproc->checkOut();
    gsamplr->checkOut();
    ohsrv->checkOut();
    emonc->checkOut();
    ipcsrvp->checkOut();
    ipcsrvm->checkOut();
    usleep(10000);
   }
  std::cout << "Exiting now.\n";
  // gnamproc->ctrlc();
  // usleep(1000000);

  std::cout << "deleting gnamproc.\n";
  delete gnamproc;
  usleep(10000);
  std::cout << "deleting gsamplr.\n";
  delete gsamplr;
  usleep(10000);
  std::cout << "deleting ohsrv.\n";
  delete ohsrv;
  usleep(10000);
  std::cout << "deleting emonc.\n";
  delete emonc;
  usleep(10000);
  std::cout << "deleting ipcsrvp.\n";
  delete ipcsrvp;
  usleep(10000);
  std::cout << "deleting ipcsrvm.\n";
  delete ipcsrvm;
  usleep(10000);
  std::cout << "ALL DONE\n";

  return 0;
 }
