#!/bin/bash -f

if [ "$GNAM_EXP_ENV" = "Done" ] ; then
  echo "GNAM exp environment already set up"
  return
fi

if [ "$X509_VOMS_DIR" == "" ] ; then
  source /afs/cern.ch/atlas/offline/external/GRID/ddm/DQ2Clients/setup.sh
fi
export X509_USER_PROXY=${HOME}/proxy

echo -n "*** WARNING *** refresh GRID proxy ? [N/y] "
read line
if [ "$line" = "y" ] || [ "$line" = "Y" ] ; then
  voms-proxy-init -voms atlas -out ${X509_USER_PROXY} -valid 120:00
  #voms-proxy-init -voms atlas -out ${X509_USER_PROXY}
  echo -e "\n *** voms-proxy-info ***"
  voms-proxy-info -file ${X509_USER_PROXY}
fi

if [ -z "$3" ] ; then
  echo "usage $0 EXP_SCRIPT PART_NAME FILE_LIST [GRID_PROTOCOL] [OUT_DIR] [IFACE]"
fi

E_CONFFILE=3

date 

CRATE=MDTCalib
CRATE=MDT-EA02
idCRATE=${CRATE:7:1}
PART_NAME=part_MDTddcTest

if [ "$CMTCONFIG" == "x86_64-slc5-gcc43-opt" ] || [ "$CMTCONFIG" == "x86_64-slc5-gcc43-dbg" ]; then
  TDAQ_ARCH=x86_64-slc5-gcc43-opt
  LCG_ARCH=${TDAQ_ARCH}
  TDAQ_ARCH_DBG=x86_64-slc5-gcc43-dbg
  LCG_ARCH_DBG=${TDAQ_ARCH_DBG}
elif [ "$CMTCONFIG" == "" ] || [ "$CMTCONFIG" == "i686-slc5-gcc43-opt" ] || [ "$CMTCONFIG" == "i686-slc5-gcc43-dbg" ]; then
  TDAQ_ARCH=i686-slc5-gcc43-opt
  LCG_ARCH=${TDAQ_ARCH}
  TDAQ_ARCH_DBG=i686-slc5-gcc43-dbg
  LCG_ARCH_DBG=${TDAQ_ARCH_DBG}
else
  echo "*** UNSUPPORTED TDAQ RELEASE ***"
  return
fi

if [ "$TDAQ_TDAQ_RELEASE" == "" ] ; then
TDAQ_RELEASE=tdaq-04-00-01
  echo -e "\n source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQ_RELEASE} ${TDAQ_ARCH}"
  source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQ_RELEASE} ${TDAQ_ARCH}
  g++ -v
fi
MY_INST_DIR=${HOME}/public/${TDAQ_RELEASE}/installed

SCRIPTS_DIR=${MY_INST_DIR}/share/bin
if [ "$SCRIPTS_DIR" = "." ] ; then
  SCRIPTS_DIR=${PWD}
else
  ROOT_DIR=${SCRIPTS_DIR:0:1}
  if [ "${ROOT_DIR}" != "/" ] ; then
    SCRIPTS_DIR=${PWD}/${SCRIPTS_DIR}
  fi
fi
INST_SHARE=$(dirname $SCRIPTS_DIR)
WRK_DIR=${INST_SHARE}/data/MDTGnamScripts
DDC_DIR=${INST_SHARE}/data/MDTddc
INST_DIR=$(dirname $INST_SHARE)

INPUT_PATH=${WRK_DIR}
OUTPUT_PATH=${WRK_DIR}/gnam_output

IPC_DIR=${OUTPUT_PATH}/ipc_refs
DB_DIR=${OUTPUT_PATH}/databases
FILE_DIR=${WRK_DIR}/file_lists
TEMPL_DIR=${WRK_DIR}/templates

mkdir -p ${IPC_DIR}
mkdir -p ${DB_DIR}
mkdir -p ${OUTPUT_PATH}/${CRATE}
pid=$$
host=`hostname -s`
UNIQUE_NAME=${host}_${pid}
OUT_DIR=${OUTPUT_PATH}/${CRATE}/${UNIQUE_NAME}
mkdir -p ${OUT_DIR}

cd ${OUT_DIR}

export TDAQ_IPC_INIT_REF=file:${IPC_DIR}/${CRATE}_${UNIQUE_NAME}.ref

if [ "$CRATE" != "MDTCalib" ]; then
  SCHEMA_FILE=${TEMPL_DIR}/schema/MDTGnam.schema.xml
  Region=${CRATE:4:2}
else
  SCHEMA_FILE=${TEMPL_DIR}/schema/MDTCalibGnam.schema.xml
  Region=Calib
fi

SKEL_DB_FILE=${TEMPL_DIR}/segments/MDTddcTestSegmentSkel.data.xml
SEGM_FILE=MDTddcTestSegment.data.xml

if [ "$Region" =  "EA" ]; then
  DET_FILE="MDTddcTestEA"
  ExtRegion="EndcapA"
elif [ "$Region" = "EC" ]; then
  DET_FILE="MDTddcTestEC"
  ExtRegion="EndcapC"
elif [ "$Region" = "BC" ]; then
  DET_FILE="MDTddcTestBC"
  ExtRegion="BarrelC"
elif [ "$Region" = "BA" ]; then
  DET_FILE="MDTddcTestBA"
  ExtRegion="BarrelA"
elif [ "$Region" = "Calib" ]; then
  DET_FILE="MDTddcTestCalib"
  ExtRegion="Calib"
else 
  echo "Region not correctly selected "
fi


echo -e "\n Copying partition files..."
cp -pf ${TEMPL_DIR}/partitions/part_MDTddcTest.data.xml ${DB_DIR}/

echo -e "\n Copying schema files..."
cp -pf ${TEMPL_DIR}/schema/GnamSampler.schema.xml ${DB_DIR}/
cp -pf ${TEMPL_DIR}/schema/mdtddc_info.xml ${DB_DIR}/
cp -pf ${TEMPL_DIR}/schema/mdtddc.schema.xml ${DB_DIR}/
cp -pf ${SCHEMA_FILE} ${DB_DIR}/

echo -e "\n Copying hw/sw files..."
cp -pfr ${TEMPL_DIR}/hw/mdt_detectors.data.xml ${DB_DIR}/
cp -pfr ${TEMPL_DIR}/hw/computers.data.xml ${DB_DIR}/
cp -pfr ${TEMPL_DIR}/sw/mdt_sw_repository.data.xml ${DB_DIR}/mdt_sw_repository.data.xml

echo -e "\n Manipulating and copying segments ... "
cp -pf ${TEMPL_DIR}/segments/MDTrcdObjects.data.xml ${DB_DIR}/
cp -pf ${TEMPL_DIR}/segments/MDTrcdDdcObjects.data.xml ${DB_DIR}/

sed -e "s^\${Region}^${Region}^g"  -e "s^\${ExtRegion}^${ExtRegion}^g"  < ${SKEL_DB_FILE} > ${DB_DIR}/${SEGM_FILE}
echo "${DB_DIR}/${SEGM_FILE}"

if [ "$Region" !=  "Calib" ]; then
  sed -e "s^\${MDT-Crate}^${CRATE}^g" -e "s^\${GNAM_MDTDDC_PATH}^${DDC_DIR}^g" -e "s^\${MDT-RCD}^${idCRATE}^g"  < ${TEMPL_DIR}/segments/${DET_FILE}Skel.data.xml >  ${DB_DIR}/${DET_FILE}.data.xml
  echo ${DB_DIR}/${DET_FILE}.data.xml
else
  cp -pfr ${TEMPL_DIR}/segments/${DET_FILE}.data.xml  ${DB_DIR}/${DET_FILE}.data.xml 
fi

echo -e "\n **** Database ready for running ..... "

PROC_NAME=${CRATE}-GnamMon
OH_SERVER=Histogramming-MDT

FILE_LIST=${FILE_DIR}/scratchCalib91060.list
FILE_LIST=/afs/cern.ch/user/d/dsalvato/public/187552.srm
FILE_LIST=/afs/cern.ch/user/r/robertof/public/DAQ/online/file_sampler/gnamexp/listanuova.srm
FILE_LIST=/afs/cern.ch/user/r/robertof/public/DAQ/online/file_sampler/gnamexp/listanuova.txt
if [ ! -f $FILE_LIST ]; then
  echo "Error: file ${FILE_LIST} doesn't exist!"
ls $FILE_DIR
  return
fi

SAMPLER_NAME=${CRATE}-Sampler

# MDTCalibGnam specific
/sbin/ifconfig ctrl0 &> /dev/null
if [ $? -eq 0 ]; then
  IFACE=ctrl0
else
  /sbin/ifconfig eth0 &> /dev/null
  if [ $? -eq 0 ]; then
    IFACE=eth0
  else
    IFACE=lo
  fi
fi

# 4 Oct 2011 take care: only in 17.0.4 there is fReadXRootD.cxx !!
# ByteStreamStoragePluginsPath=/afs/cern.ch/atlas/software/builds/AtlasEvent/17.0.4/Event/ByteStreamStoragePlugins
PLUGIN_VERSION=AtlasEvent/17.0.4/InstallArea/${TDAQ_ARCH}/lib
PLUGIN_VERSION_DBG=AtlasEvent/17.0.4/InstallArea/${TDAQ_ARCH_DBG}/lib
if [ -d "/opt/exp_soft/atlas/prod/releases/rel_15-6/${PLUGIN_VERSION}" ] ; then
  PLUGIN_LIB_PATH=/opt/exp_soft/atlas/prod/releases/rel_15-6/${PLUGIN_VERSION}
  LCG_EXT_PATH=/opt/exp_soft/atlas/TDAQ/sw/lcg/external
elif [ -d "/afs/cern.ch/atlas/software/builds/${PLUGIN_VERSION}" ] ; then
  PLUGIN_LIB_PATH=/afs/cern.ch/atlas/software/builds/${PLUGIN_VERSION}
  LCG_EXT_PATH=/afs/cern.ch/sw/lcg/external
else
  PLUGIN_LIB_PATH=/afs/cern.ch/atlas/software/builds/${PLUGIN_VERSION_DBG}
  LCG_EXT_PATH=/afs/cern.ch/sw/lcg/external
fi

# use latest ROOT version (libXrdPosix.so from version "5.26.00d_python2.6" does crash from time to time)
# ROOT_LIB_PATH=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.30.04/${TDAQ_ARCH}/root/lib
# unlikely GNAM breaks down with latest ROOT version(s) ...

# ++++++++++++++++++++++++++++++++++ lcg 61c ++++++++++++++++++++++++++++++++ #
LCG_ARCH=${TDAQ_ARCH}
LCG_EXT_PATH=/afs/cern.ch/sw/lcg/external
LCG_APP_PATH=/afs/cern.ch/sw/lcg/app

# pick-up the right versions of boost, castor, cgsi-gsoap, dcap, dpm, gfal, globus, root, voms-api-c
BOOST_LIB_PATH=${LCG_EXT_PATH}/Boost/1.44.0_python2.6/${LCG_ARCH}/lib
CASTOR_LIB_PATH=${LCG_EXT_PATH}/castor/2.1.8-10/${LCG_ARCH}/usr/lib
CGSI_LIB_PATH=${LCG_EXT_PATH}/Grid/cgsi-gsoap/1.3.3-1/${LCG_ARCH}/usr/lib
# libdcap1.2.44.so -> dcache_client 1.9.3p1
DCAP_LIB_PATH=${LCG_EXT_PATH}/dcache_client/1.9.3p1/${LCG_ARCH}/dcap/lib
DPM_LIB_ARCH=`echo ${LCG_ARCH} | sed 's/i686-slc5-gcc43-opt/lib/; s/x86_64-slc5-gcc43-opt/lib64/'`
DPM_LIB_PATH=${LCG_EXT_PATH}/Grid/DPM/1.7.4-7sec/${LCG_ARCH}/${DPM_LIB_ARCH}
GFAL_LIB_PATH=${LCG_EXT_PATH}/Grid/gfal/1.11.8-2/${LCG_ARCH}/lib
GLOBUS_LIB_PATH=${LCG_EXT_PATH}/Grid/globus/4.0.7-VDT-1.10.1/${LCG_ARCH}/globus/lib
###ROOT_LIB_PATH=${LCG_APP_PATH}/releases/ROOT/5.30.00/${TDAQ_ARCH}/root/lib
ROOT_LIB_PATH=${LCG_APP_PATH}/releases/ROOT/5.34.02/${TDAQ_ARCH}/root/lib
VOMS_LIB_PATH=${LCG_EXT_PATH}/Grid/voms-api-c/1.9.17-1/${LCG_ARCH}/lib
# ---------------------------------- lcg 61c -------------------------------- #

# +++++++++++++++++++++++++ common for all releases +++++++++++++++++++++++++ #
# 4 Oct 2011 take care: only in 17.0.4 there is fReadXRootD.cxx !!
# ByteStreamStoragePluginsPath=/afs/cern.ch/atlas/software/builds/AtlasEvent/17.0.4/Event/ByteStreamStoragePlugins
PLUGIN_VERSION=AtlasEvent/17.2.6/InstallArea/${TDAQ_ARCH}/lib
PLUGIN_LIB_PATH=/afs/cern.ch/atlas/software/builds/${PLUGIN_VERSION}

export LCG_LIBRARY_PATH=${PLUGIN_LIB_PATH}:${BOOST_LIB_PATH}:${CASTOR_LIB_PATH}:${CGSI_LIB_PATH}:\
${DCAP_LIB_PATH}:${DPM_LIB_PATH}:${GFAL_LIB_PATH}:${GLOBUS_LIB_PATH}:${ROOT_LIB_PATH}:${VOMS_LIB_PATH}
export LCG_RFIO_TYPE=rfio
# ------------------------- common for all releases ------------------------- #

# ++++++++++++++++++++++++++++++ for dpm access +++++++++++++++++++++++++++++ #
# that is for site not using castor
# dirty tricks for dpm access
MY_DPM_LIB_SHIFT=${MY_INST_DIR}/${TDAQ_ARCH}/dpm/lib
mkdir -p ${MY_DPM_LIB_SHIFT}
ln -fs ${DPM_LIB_PATH}/libdpm.so ${MY_DPM_LIB_SHIFT}/libshift.so
ln -fs ${DPM_LIB_PATH}/libdpm.so ${MY_DPM_LIB_SHIFT}/libshift.so.2.1
ln -fs ${DPM_LIB_PATH}/liblcgdm.so ${MY_DPM_LIB_SHIFT}/liblcgdm.so

export LD_LIBRARY_PATH=${DPM_LIB_SHIFT}:${ROOT_LIB_PATH}:${PLUGIN_LIB_PATH}:${GFAL_LIB_PATH}:${CGSI_LIB_PATH}:${GLOBUS_LIB_PATH}:${VOMS_LIB_PATH}:${BOOST_LIB_PATH}:${CASTOR_LIB_PATH}:${DCAP_LIB_PATH}:${LD_LIBRARY_PATH}
export PATH=.:${INST_DIR}/${TDAQ_ARCH}/bin:${PATH}
#export LD_LIBRARY_PATH=${INST_DIR}/${TDAQ_ARCH}/lib:${MY_DPM_LIB_SHIFT}:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${INST_DIR}/${TDAQ_ARCH}/lib:${LD_LIBRARY_PATH}
export TDAQ_PARTITION=${PART_NAME}
export APPLICATION=${CRATE}
export TDAQ_DB_PATH="${TDAQ_DB_PATH}:${INST_SHARE}/data"
export TDAQ_DB_DATA="${DB_DIR}/${TDAQ_PARTITION}.data.xml"
#export TDAQ_DB_PATH="${TDAQ_DB_PATH}:${DB_DIR}"
#export TDAQ_DB_DATA="${DB_DIR}/${PART_NAME}.data.xml"
export TDAQ_DB="oksconfig:${TDAQ_DB_DATA}"
export GNAM_OH_SERVER=${OH_SERVER}
export GNAM_SAMPLER_TYPE=${SAMPLER_NAME}
export GNAM_SAMPLER_NAME=${SAMPLER_NAME}
export GNAM_SAMPL_NAME=${APPLICATION}-Sampler
export GNAM_PROC_NAME=${PROC_NAME}
export GNAM_FILE_LIST=${FILE_LIST}
export GNAM_SAMPLER_FILE_LIST=${FILE_LIST}
export GNAM_INPUT_PATH=${INPUT_PATH}
export GNAM_OUTPUT_PATH=${OUTPUT_PATH}
export GNAM_CALIB_IFACE=${IFACE}
# that is ok only for site not using castor
# export LCG_RFIO_TYPE=dpm
export LCG_RFIO_TYPE=rfio

export SHELL_PROMPT="gnam on `hostname -s` "
PS1=$SHELL_PROMPT

# ${EXP_SCRIPT}

echo -e "*** EXIT ***\n"

cd -
pwd

export GNAM_EXP_ENV="Done"

return

# exit 0
