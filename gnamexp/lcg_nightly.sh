#!/bin/bash -f

function split()
 {
  bbt=`echo $1 | sed 's/:/ /g'`
  set -- $bbt
  i=0
  until [ -z "$1" ]  
  do
    let i=$i+1
    echo "$i $1"
    shift
  done
 }

if [ "$1" != "i686-slc5-gcc43-opt" ] && [ "$1" != "x86_64-slc5-gcc43-opt" ]; then
  echo -e "usage: \n        source lcg_nightly.sh i686-slc5-gcc43-opt"
  echo -e "   or: \n        source lcg_nightly.sh x86_64-slc5-gcc43-opt"
  return
fi

# +++++++++++++++++++++++++++ define tdaq version +++++++++++++++++++++++++++ #
# +++++++++++++++++++++++++++++++++ nightly +++++++++++++++++++++++++++++++++ #
# dqm-common-00-18-03 / tdaq-common-01-18-04 / LCGCMT_61c / offline-17.2.x

TDAQ_RELEASE=nightly
TDAQ_ARCH=$1
shift
source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh ${TDAQ_RELEASE} ${TDAQ_ARCH}
MY_INST_DIR=${HOME}/public/${TDAQ_RELEASE}/installed
# --------------------------------- nightly --------------------------------- #
# --------------------------- define tdaq version --------------------------- #

# ++++++++++++++++++++++++++++ setup grid access ++++++++++++++++++++++++++++ #
if [ "$X509_VOMS_DIR" == "" ] ; then
  source /afs/cern.ch/atlas/offline/external/GRID/ddm/DQ2Clients/setup.sh
fi
export X509_USER_PROXY=${HOME}/proxy
echo -n "*** WARNING *** refresh GRID proxy ? [N/y] "
read line
if [ "$line" = "y" ] || [ "$line" = "Y" ] ; then
  voms-proxy-init -voms atlas -out ${X509_USER_PROXY} -valid 96:00
fi
echo -e "\n*** voms-proxy-info ***"
voms-proxy-info -file ${X509_USER_PROXY}
date
# ---------------------------- setup grid access ---------------------------- #

# ++++++++++++++++++++++++++++++++++ lcg 61c ++++++++++++++++++++++++++++++++ #
LCG_ARCH=${TDAQ_ARCH}
LCG_EXT_PATH=/afs/cern.ch/sw/lcg/external
LCG_APP_PATH=/afs/cern.ch/sw/lcg/app

# pick-up the right versions of boost, castor, cgsi-gsoap, dcap, dpm, gfal, globus, root, voms-api-c
BOOST_LIB_PATH=${LCG_EXT_PATH}/Boost/1.44.0_python2.6/${LCG_ARCH}/lib
CASTOR_LIB_PATH=${LCG_EXT_PATH}/castor/2.1.8-10/${LCG_ARCH}/usr/lib
CGSI_LIB_PATH=${LCG_EXT_PATH}/Grid/cgsi-gsoap/1.3.3-1/${LCG_ARCH}/usr/lib
# libdcap1.2.44.so -> dcache_client 1.9.3p1
DCAP_LIB_PATH=${LCG_EXT_PATH}/dcache_client/1.9.3p1/${LCG_ARCH}/dcap/lib
DPM_LIB_ARCH=`echo ${LCG_ARCH} | sed 's/i686-slc5-gcc43-opt/lib/; s/x86_64-slc5-gcc43-opt/lib64/'`
DPM_LIB_PATH=${LCG_EXT_PATH}/Grid/DPM/1.7.4-7sec/${LCG_ARCH}/${DPM_LIB_ARCH}
GFAL_LIB_PATH=${LCG_EXT_PATH}/Grid/gfal/1.11.8-2/${LCG_ARCH}/lib
GLOBUS_LIB_PATH=${LCG_EXT_PATH}/Grid/globus/4.0.7-VDT-1.10.1/${LCG_ARCH}/globus/lib
###ROOT_LIB_PATH=${LCG_APP_PATH}/releases/ROOT/5.30.05/${TDAQ_ARCH}/root/lib
ROOT_LIB_PATH=${LCG_APP_PATH}/releases/ROOT/5.34.02/${TDAQ_ARCH}/root/lib
VOMS_LIB_PATH=${LCG_EXT_PATH}/Grid/voms-api-c/1.9.17-1/${LCG_ARCH}/lib
# ---------------------------------- lcg 61c -------------------------------- #

# +++++++++++++++++++++++++ common for all releases +++++++++++++++++++++++++ #
# 4 Oct 2011 take care: only in 17.0.4 there is fReadXRootD.cxx !!
# ByteStreamStoragePluginsPath=/afs/cern.ch/atlas/software/builds/AtlasEvent/17.0.4/Event/ByteStreamStoragePlugins
PLUGIN_VERSION=AtlasEvent/17.2.6/InstallArea/${TDAQ_ARCH}/lib
PLUGIN_LIB_PATH=/afs/cern.ch/atlas/software/builds/${PLUGIN_VERSION}

export LCG_LIBRARY_PATH=${PLUGIN_LIB_PATH}:${BOOST_LIB_PATH}:${CASTOR_LIB_PATH}:${CGSI_LIB_PATH}:\
${DCAP_LIB_PATH}:${DPM_LIB_PATH}:${GFAL_LIB_PATH}:${GLOBUS_LIB_PATH}:${ROOT_LIB_PATH}:${VOMS_LIB_PATH}
export LCG_RFIO_TYPE=rfio
# ------------------------- common for all releases ------------------------- #

# ++++++++++++++++++++++++++++++ for dpm access +++++++++++++++++++++++++++++ #
# that is for site not using castor
# dirty tricks for dpm access
MY_DPM_LIB_SHIFT=${MY_INST_DIR}/${TDAQ_ARCH}/dpm/lib
mkdir -p ${MY_DPM_LIB_SHIFT}
ln -fs ${DPM_LIB_PATH}/libdpm.so ${MY_DPM_LIB_SHIFT}/libshift.so
ln -fs ${DPM_LIB_PATH}/libdpm.so ${MY_DPM_LIB_SHIFT}/libshift.so.2.1
ln -fs ${DPM_LIB_PATH}/liblcgdm.so ${MY_DPM_LIB_SHIFT}/liblcgdm.so
# in case you need dpm uncomment next 2 lines
#export LCG_LIBRARY_PATH=${MY_DPM_LIB_SHIFT}:${LCG_LIBRARY_PATH}
#export LCG_RFIO_TYPE=dpm
# ------------------------------ for dpm access ----------------------------- #

# ++++++++++++++++++++++++++ PATH & LD_LIBRARY_PATH +++++++++++++++++++++++++ #
# +++++++++++++++++++++++ add your own private stuffs +++++++++++++++++++++++ #
export LD_LIBRARY_PATH=${MY_INST_DIR}/${TDAQ_ARCH}/lib:${LCG_LIBRARY_PATH}:${LD_LIBRARY_PATH}
export PATH=.:${MY_INST_DIR}/${TDAQ_ARCH}/bin:${PATH}
echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
echo "PATH=${PATH}"
echo "****************** LD_LIBRARY_PATH ******************"
split $LD_LIBRARY_PATH
echo "****************** PATH ******************"
split $PATH
# ----------------------- add your own private stuffs ----------------------- #
# -------------------------- PATH & LD_LIBRARY_PATH ------------------------- #

