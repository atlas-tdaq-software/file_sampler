#ifndef __APPLE__
#include "ers/ers.h"
#include <fcntl.h>
#include <gfal_api.h>

#include "fReadGfal.h"
#include "EventStorage/EventStorageIssues.h"

fReadGfal::fReadGfal() 
{
  m_pfd = -1;
  m_size = 0;
} 

fReadGfal::~fReadGfal() 
{
  this->closeFile();
}

bool fReadGfal::isOpen() {return m_pfd != -1;}

bool fReadGfal::isEoF() 
{
  if(this->isOpen()) {
    return ( ::gfal_lseek64(m_pfd,0,SEEK_CUR) == m_size );
  } else {
    return false;
  }
}

bool fReadGfal::fileExists(std::string fName) const
{
  char * nm = new char[fName.size()+1];
  memcpy(nm,fName.c_str(),fName.size());
  nm[fName.size()] = 0;

  int ret = ::gfal_open64(nm, O_RDONLY, S_IREAD);


  bool isThere = (ret != -1);

  if (isThere) ::gfal_close(ret);
  delete[] nm;

  return isThere;
}

void fReadGfal::openFile(std::string fName) 
{
  if(this->isOpen()) this->closeFile();

  char * nm = new char[fName.size()+1];
  memcpy(nm,fName.c_str(),fName.size());
  nm[fName.size()] = 0;

  m_pfd = ::gfal_open64(nm, O_RDONLY, S_IREAD);
  if (m_pfd != -1)
   {
    ::gfal_lseek64(m_pfd,0,SEEK_END);
    m_size = ::gfal_lseek64(m_pfd,0,SEEK_CUR);
    ::gfal_lseek64(m_pfd,0,SEEK_SET);
   }

  delete[] nm;
}

void fReadGfal::closeFile() 
{
  if(m_pfd != -1) ::gfal_close(m_pfd);
  m_pfd = -1;
}

void fReadGfal::readData(char *buffer, unsigned int sizeBytes) 
{
  if (sizeBytes==0) return;
  if(this->isOpen())
    {
      unsigned int totalRead=0,ntry=0;
      while (sizeBytes > totalRead)
	{
	  int ret = ::gfal_read(m_pfd,buffer+totalRead,
                                sizeof(char)*(sizeBytes-totalRead));

	  totalRead += ret; ++ntry;
	  if(ntry>5) {
	    /* ERS_WARNING("Problem reading from the data file. "
			<<"fReadGfal::readData asked to read "<<sizeBytes
			<<" bytes and managed to read only "<<totalRead
			<<" bytes.");  
	    */
	    std::stringstream mystream;
	    mystream << "Problem reading from the data file. "
			<<"fReadGfal::readData asked to read "<<sizeBytes
			<<" bytes and managed to read only "<<totalRead
			<<" bytes.";  
	    EventStorage::ReadingIssue ci(ERS_HERE, mystream.str().c_str());
	    ers::warning(ci);

	    return;
	  }
	}
    }
}

int64_t fReadGfal::getPosition() 
{
  if(this->isOpen()) return ::gfal_lseek64(m_pfd,0,SEEK_CUR);
  return -1;
}

void fReadGfal::setPosition(int64_t p) 
{  
  if(this->isOpen()) ::gfal_lseek64(m_pfd,p,SEEK_SET);
}
 
void fReadGfal::setPositionFromEnd(int64_t p)
{  
  if(this->isOpen()) ::gfal_lseek64(m_pfd,p,SEEK_END);
}
 
fRead * fReadGfal::newReader() const
{
  fReadGfal * nfr = new fReadGfal();
  return (fRead *)nfr;
}

extern "C" {
  fRead * fReadFactory() 
  {
    fReadGfal * nfr = new fReadGfal();
    return (fRead *)nfr;
  }
}
#endif
