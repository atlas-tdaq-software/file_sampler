
#ifndef FREADGFAL_H
#define FREADGFAL_H

#include "EventStorage/fRead.h"

#include <stdio.h>

class fReadGfal : public fRead
{
 public:
  fReadGfal(); 
  ~fReadGfal();

  bool isOpen();
  bool isEoF();
  bool fileExists(std::string fName) const;
  void openFile(std::string fName);
  void closeFile();
  void readData(char *buffer, unsigned int sizeBytes);
  int64_t getPosition();
  void setPosition(int64_t p);
  void setPositionFromEnd(int64_t p);
  fRead * newReader() const; 

 private:
  int m_pfd; // current file
  off64_t m_size;
};

#endif 

