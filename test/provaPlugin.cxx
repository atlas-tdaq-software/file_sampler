
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include "gnamSampler/RawEvent.h"
#include "gnamSampler/FileEvent.h"

using namespace daq::gnamSampler;

int nargs(4);
const char* const args[4] = { "testSampler", "-x", "-n", "testSampler" };

RawEvent* m_re;
uint32_t* m_event;
size_t m_size;

// int main ( int argc, char** argv )
int main ()
 {
  SamplerVariables vars( nargs, args );
  vars.set_mode (FreeRunning);
  char* fl = getenv("GNAM_SAMPLER_FILE_LIST");
  vars.set_filelist (std::string(fl));
  vars.set_selection (ANY);
  m_re = new FileEvent(vars);
  uint32_t nevt(0);
  do 
   {
    nevt ++;
    m_size = m_re->readEvent ( m_event );
    if ((nevt % 10000) == 0) std::cout << nevt << std::endl;
   }
  while (m_size);
  delete m_re;
  return 0;
 }
