#ifndef _MYFREAD_H_
#define _MYFREAD_H_

#include <stdint.h>
#include <string>
#include <map>
#include <set>

#include "EventStorage/fRead.h"

namespace daq { namespace gnamSampler { class myfRead; } }

class daq::gnamSampler::myfRead
 {
  public:
   myfRead(); 
   ~myfRead();
   bool openFile( std::string fileName); 

   inline void closeFile() { if (m_reader) m_reader->closeFile(); }

   inline bool isOpen() { return (m_reader) ? m_reader->isOpen() : false; }

   inline bool isEoF() { return (m_reader) ? (m_reader->isEoF() || (m_reader->getPosition() == m_size)) : true; }

   inline bool fileExists() { return (m_reader) ? m_reader->fileExists(m_name) : false; }

   inline void readData(uint32_t* buffer, uint32_t nWords)
    { if (m_reader) m_reader->readData((char*)buffer, nWords*sizeof(uint32_t)); }

   inline int64_t getPosition() { return (m_reader) ? m_reader->getPosition() : 0; }

   inline void setPosition(int64_t p)
    { if (m_reader) (p > m_size) ? m_reader->setPositionFromEnd(0) : m_reader->setPosition(p); }

   inline void setPositionFromEnd(int64_t p)
    { if (m_reader) (p > m_size) ? m_reader->setPosition(0) : m_reader->setPositionFromEnd(p); }

   inline uint32_t id() { return (m_reader) ? m_idx : 0; }

   inline int64_t size() { return (m_reader) ? m_size : 0; }

   inline uint32_t firstw() { return m_firstw; }

   inline int64_t left() { return (m_reader) ? m_size-m_reader->getPosition() : 0; }

  private:
   enum rformat { NONE=0, XROOTD, GFAL, DISK, RFIO, DCACHE, ANY };
   std::string m_name;
   uint32_t m_idx;
   int64_t m_size;
   uint32_t m_firstw;
   rformat m_used;
   std::string m_libname;
   fRead* m_reader;
   std::set < rformat > m_plugins;
   std::map < rformat, fRead* > m_loaded;

   void dllSet( rformat rf );
   bool getFileInfo( std::string fileName );
 };

#endif // _MYFREAD_H_
