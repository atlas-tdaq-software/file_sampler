#ifndef _FILE_EVENT_H_
#define _FILE_EVENT_H_

#include <cstring>
#include <stdint.h>
#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <boost/shared_array.hpp>

#include "gnamSampler/SamplerVariables.h"

#include "RawEvent.h"
#include "SamplerUtils.h"
#include "myBuffer.h"
#include "myfRead.h"
#include "rawFileRecords.h"
#include "bcallback.h"

namespace daq { namespace gnamSampler { class FileEvent; } }
// using daq::gnamSampler::EventSelection;

class daq::gnamSampler::FileEvent : public RawEvent
 {
  public:
   enum fragMask { EMPTYMSK=0, CALIBMASK1=0xffffeeee, CALIBMASK2=0x1111,
                   FULLMSK=0xffffffff };
   enum { MAXSIZE=0x4000000 };

  private:
   myBuffer m_buffer1, m_buffer2;
   size_t m_bsize;

   myfRead * m_dfile;
   uint32_t* m_eventformatrecords;
   rawFileRecords* m_rawfr;
   int64_t m_fsize, m_taken, m_left;
   bool m_fileRecords;
   bool m_cycle;

   uint32_t m_evcnt;
   uint32_t m_evsel;
   uint32_t m_dtmask;
   uint32_t m_sdmask;
   uint32_t m_fword;
   fragMarker m_marker;
   fragMask m_mask1, m_mask2;
   size_t m_evsize;

   evtinfo m_info;

   typedef const uint32_t* uint32_tp;
   boost::shared_array<uint32_tp> m_children;
   size_t m_robmx;

   uint32_t m_linecounts;

   SamplerVariables & m_variables;
   EventSelection m_workLevel;

   uint32_t m_evmax;
   uint32_t m_nskip;

   const std::vector<uint32_t>* m_l1ids;
   const std::vector<uint32_t>* m_gids;
   const std::vector<uint32_t>* m_tts;
   const std::vector<uint32_t>* m_dets;
   const std::vector<uint32_t>* m_sids;
   std::vector<uint32_t> m_dtok;

   std::vector<std::string> m_flist;
   std::string::size_type m_itr;
   std::stack<uint32_t> m_stack;

   std::vector < TCallback <FileEvent> * > m_callback;

   TCallback<FileEvent>* m_l1sel;
   TCallback<FileEvent>* m_gdsel;
   TCallback<FileEvent>* m_ttsel;
   TCallback<FileEvent>* m_dtsel;
   TCallback<FileEvent>* m_sdsel;
   TCallback<FileEvent>* m_dtfrg;
   TCallback<FileEvent>* m_sdfrg;

   int32_t (*m_checker)( uint32_t* p, uint32_t size, evtinfo& info );

  public:
   FileEvent ( SamplerVariables & vars );
   ~FileEvent ();
   inline int64_t fsize () { return m_fsize; }
   inline int64_t ftaken () { return m_taken; }
   inline int64_t fleft () { return m_left; }
   inline uint32_t numevts () { return m_evsel; }
   friend std::ostream & operator<< (std::ostream& stream, FileEvent & data);
   size_t readEvent ( uint32_t* & evptr );
   inline uint32_t* dptr() { return m_buffer1.get(); }
   inline size_t dsize() { return m_evsize; }
   inline uint32_t linecounts() { return m_linecounts; }
   inline void linecounts( uint32_t lc ) { m_linecounts = lc; }

  private:
   inline bool isOpen () { return m_dfile->isOpen(); }
   inline bool isEoF () { return m_dfile->isEoF(); }
   inline void closeFile () { m_dfile->closeFile(); }
   void setList ( std::string lst );
   bool openRFile ( std::string & fname );
   bool nextFile ();
   void stackfill ();
   int64_t findMarker ();
   bool searchEvent ();
   bool resize ( size_t s );
   size_t getRod ();
   size_t getEvt ();
   size_t getFileRecordEvt ();
   bool selectEv ( const std::vector<uint32_t>* vlist, uint32_t* info );
   bool selectRobs ( const std::vector<uint32_t>* vlist, uint32_t* mask );
   bool selectRods ( const std::vector<uint32_t>* vlist, uint32_t* mask );
   uint32_t m_nevtot, m_nevtinfile;
 };

#endif // _FILE_EVENT_H_
