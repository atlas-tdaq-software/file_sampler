#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <exception>
#include <iostream>

#include <RunControl/ItemCtrl/ItemCtrl.h>
#include <ipc/core.h>

#include "gnamSampler/RawEvent.h"
#include "gnamSampler/FileEvent.h"

#include "gnamSampler/SamplerMessRep.h"
#include "gnamSampler/SamplerException.h"
#include "gnamSampler/SamplerFSM.h"
#include "gnamSampler/SamplerThread.h"

using namespace daq::gnamSampler;

extern "C" void m_unexpected ()
 {
  // if (!std::uncaught_exception()) std::cerr << "my_unexpected called\n";
  std::cerr << " fileSampler CLOSING UP: unexpected exception\n";
  abort();
 }

extern "C" void m_terminate ()
 {
  // std::cerr << "my_terminate called\n";
  std::cerr << " fileSampler CLOSING UP: terminate\n";
  abort();
 }

extern "C" void m_exit ()
 {
  std::cerr << " fileSampler CLOSING UP: exit\n";
 }

void runWithFSM ( SamplerVariables & vars , SamplerInternals & ints )
 {
  MR_INFO ("Starting LocalController client in " <<
               ( ( vars.get_isinteractive() ) ?
           "interactive mode" : "controlled mode" ));

  SamplerFSM* p = new SamplerFSM( vars, ints );

  std::string name = vars.get_processname();
  std::string parent = vars.get_parent();
  std::string segment = vars.get_segment();
  std::string partition = vars.get_partition();
  daq::rc::ItemCtrl myItem (name, parent, segment, partition, std::shared_ptr<daq::rc::Controllable>(p), vars.get_isinteractive());
  myItem.init();

  MR_DEBUG (0, "Starting program job");
  myItem.run(); 
  MR_INFO ("Done program job");
 }

void runWithoutFSM ( SamplerVariables & vars , SamplerInternals & ints )
 {
  MR_INFO ("Starting client without control");

  SamplerFSM* p = new SamplerFSM( vars, ints );
  std::string name = vars.get_processname();
  std::string parent = vars.get_parent();
  std::string segment = vars.get_segment();
  std::string partition = vars.get_partition();
  daq::rc::ItemCtrl myItem (name, parent, segment, partition, std::shared_ptr<daq::rc::Controllable>(p), true /* vars.get_isinteractive() */ );
  myItem.init();

  MR_DEBUG (0, "Starting program job");
  p->run();
  MR_INFO ("Done program job");
 }

int testSampler()
 {
  char* fl = getenv("GNAM_SAMPLER_FILE_LIST");
  MR_INFO ("gnamSampler testing file list " << fl);

  int nargs(4);
  const char* const args[4] = { "testSampler", "-x", "-n", "testSampler" };

  FileEvent* m_fe;
  RawEvent* m_re;
  uint32_t* m_event;
  size_t m_size;

  SamplerVariables vars( nargs, args );
  vars.set_mode (FreeRunning);
  vars.set_filelist (std::string(fl));
  vars.set_selection (ANY);
  m_re = m_fe = new FileEvent(vars);
  uint32_t nevt(0);
  int64_t fsz(0);
  do 
   {
    nevt ++;
    m_size = m_re->readEvent ( m_event );
    if (((nevt % 100000) == 0) && (fsz = m_fe->fsize()*sizeof(uint32_t)))
     {
      double q = (100.0*sizeof(uint32_t)*m_fe->ftaken())/fsz;
      uint32_t pc = uint32_t(q+0.5);
      std::cout << time2str() << " " << nevt << " events (read " << pc << "% of " << fsz << "-long file)" << std::endl;
     }
   }
  while (m_size);
  delete m_re;
  return 0;
 }

int main (int argc, char** argv)
 {
  if ((argc == 2) && (strcasecmp( argv[1], "test") == 0))
    return testSampler();

  // std::set_unexpected (m_unexpected);
  std::set_terminate (m_terminate);
  atexit (m_exit);

  SamplerVariables * variables(0);
  SamplerInternals * internals(0);

  SamplerThread * sthread(0);

  try
   {
    variables = new SamplerVariables (argc, argv);
    internals = new SamplerInternals (variables->withFSM());

    sthread = new SamplerThread (*variables, *internals);

    MR_INFO ("pid " << getpid() << " - creating new instance: "
             << variables->get_processname());

    variables->withFSM() ?
      runWithFSM (*variables, *internals) : runWithoutFSM (*variables, *internals);

   }
  catch (const std::exception &ex)
   {
    FatalError (ex);
   }
  catch (...)
   {
    FatalError (InternalError (ERS_HERE, "unknown exception caught"));
   }

  delete sthread;

  delete internals;
  delete variables;

  exit(0);
 }
