#ifndef _RAW_FILE_RECORDS_H_
#define _RAW_FILE_RECORDS_H_

#include <stdint.h>
#include <string>
#include <vector>
#include "compression/DataBuffer.h"
#include "myBuffer.h"
#include "myfRead.h"


struct file_start_record
 {
  uint32_t marker;         /* Start of record marker          */
  uint32_t record_size;    /* Total record size               */
  uint32_t version;        /* Format version number           */
  uint32_t file_number;    /* File number in the sequence     */
  uint32_t date;           /* Date of run start               */
  uint32_t time;           /* Time of run start               */
  uint32_t sizeLimit_dataBlocks;  /* Max number of data blocks in the file */
  uint32_t sizeLimit_MB;   /* Max number of data blocks in the file */
 };

struct name_string_record
 {
  uint32_t marker;
  std::string appName;
  std::string fileNameCore;
 };

struct metadata_string_record
 {
  uint32_t marker;
  uint32_t nfree;
  std::vector<std::string> metaStrings;
 };

namespace daq { namespace gnamSampler { class rawFileRecords; } }

namespace daq { namespace gnamSampler {
  typedef enum
   {
       NOMRK=0, CALIBMRK=0xabba0000, RODMRK=0xee1234ee,
       ROBMRK=0xdd1234dd, FULLMRK=0xaa1234aa, DSRMRK=0x1234cccc,
       RPRMRK=0x1234bbbb, MDTSMRK=0x1234aabc, FNSMRK=0x1234aabb,
       FSRMRK=0x1234aaaa, FENDMRK=0x1234dddd, MFSRMRK=0x1ba2baba
   } fragMarker;
 } }

class daq::gnamSampler::rawFileRecords
 {
  public:
   rawFileRecords( myfRead* reader );
   ~rawFileRecords();
   inline uint32_t* mergedfilestartR();
   inline uint32_t* fileStartR();
   inline uint32_t* runParR();
   inline uint32_t* dataSepR();
   inline uint32_t* fileEndR();
   inline uint32_t* rawDataR();
   inline bool haveData() { return m_havedata; }
   inline uint32_t evsize() { return m_rawsize/sizeof(uint32_t); }
   inline bool isEoF() { return m_reader->isEoF(); }
   uint32_t getRecord( uint32_t* & ptr, uint32_t & mrk );
   uint32_t getData( myBuffer* ptr, uint32_t & mrk );
  private:
   myfRead* m_reader;
   uint32_t* m_mfsr, * m_fsr, * m_rpr, * m_dsr, * m_fer;
   name_string_record* m_fns;
   metadata_string_record* m_mdts;
   bool m_havedata, m_zlib;
   uint32_t m_bfcmpsize, m_bfdecsize;
   char* m_buffer_in;
   uint32_t m_dblksize, m_rawsize;
   myBuffer* m_buffer_dec;
   uint32_t m_nevtot;
   compression::DataBuffer m_decompressed;
   uint32_t readMRFH( uint32_t mrk );
   uint32_t readFSR( uint32_t mrk );
   uint32_t readFNS( uint32_t mrk );
   uint32_t readMDTS( uint32_t mrk );
   uint32_t readRPR( uint32_t mrk );
   uint32_t readDSR( uint32_t mrk );
   uint32_t readFER( uint32_t mrk );
   uint32_t readEvent();
   uint32_t readComprEvent();
 };

#endif // _RAW_FILE_RECORDS_H_
