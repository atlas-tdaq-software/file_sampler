#ifndef _SAMPLER_FSM_H_
#define _SAMPLER_FSM_H_

#include <config/Configuration.h>
#include <owl/timer.h>
#include <RunControl/Common/Controllable.h>

#include "gnamSampler/SamplerVariables.h"
#include "gnamSampler/SamplerInternals.h"

namespace daq { namespace gnamSampler { class SamplerFSM; } }

class daq::gnamSampler::SamplerFSM
    : public daq::rc::Controllable
 {
  public:
    SamplerFSM (SamplerVariables & svar, SamplerInternals & sint);
    ~SamplerFSM () noexcept;
  
    void configure (const daq::rc::TransitionCmd& cmd);
    void unconfigure (const daq::rc::TransitionCmd& cmd);
    void prepareForRun (const daq::rc::TransitionCmd& cmd);
    void stopRecording (const daq::rc::TransitionCmd& cmd);
    void onExit (daq::rc::FSM_STATE state) noexcept;

    void run ();

  private:

    SamplerVariables & m_variables;
    SamplerInternals & m_internals;

    Configuration * m_confDB;

    OWLTimer timertotal;
    OWLTimer timerinrun;
 };

#endif // _SAMPLER_FSM_H_
