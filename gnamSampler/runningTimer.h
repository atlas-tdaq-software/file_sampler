#ifndef _RUNNING_TIMER_H_
#define _RUNNING_TIMER_H_

#include <sys/time.h>
#include <sys/resource.h>

class runningTimer
 {
  public:
    runningTimer( );

    void        eval( );
    double      userRunTime( ) const { return userRunTime_; }
    double      totalRunTime( ) const { return totalRunTime_; }
    double      systemRunTime( ) const { return systemRunTime_; }

    void        start( );
    void        reset( );
    void        stop( );
    double      userTime( ) const { return userTime_; }
    double      totalTime( ) const { return totalTime_; }
    double      systemTime( ) const { return systemTime_; }

  private:
    enum State { Stopped, Running };

    double              userTime_;
    double              systemTime_;
    double              totalTime_;
    struct timeval      start_, stop_;
    struct rusage       start_usage_, stop_usage_;
    State               state_;

    double              userRunTime_;
    double              systemRunTime_;
    double              totalRunTime_;

 };

#endif // _RUNNING_TIMER_H_
