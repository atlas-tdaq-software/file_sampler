
#include "gnamSampler/runningTimer.h"

#if defined(__sun__) && defined(__GNUG__)
  extern "C" int getrusage( int who, struct rusage *rusage );
#endif

runningTimer::runningTimer()
{
    userTime_ = systemTime_ = totalTime_ = 0.;
    state_ = Stopped;

    start_usage_.ru_utime.tv_sec = start_usage_.ru_utime.tv_usec =
        start_usage_.ru_stime.tv_sec = start_usage_.ru_stime.tv_usec = 
            start_.tv_sec = start_.tv_usec = 0;
    userRunTime_ = systemRunTime_ = totalRunTime_ = 0.;
}

void runningTimer::reset()
{
    userTime_ = systemTime_ = totalTime_ = 0.;
    state_ = Stopped;
}

void runningTimer::start()
{
    if ( state_ != Stopped )
    {
	return;
    }
    state_ = Running;
    gettimeofday( &start_, 0 );
    getrusage( RUSAGE_SELF, &start_usage_ );
}

void runningTimer::stop()
{
    if ( state_ != Running )
    {
	return;
    }
    state_ = Stopped;
    getrusage( RUSAGE_SELF, &stop_usage_ );
    gettimeofday( &stop_, 0 );
    userTime_   += ( stop_usage_.ru_utime.tv_sec - start_usage_.ru_utime.tv_sec ) 
    		+ (stop_usage_.ru_utime.tv_usec - start_usage_.ru_utime.tv_usec)/1000000.;
    systemTime_ += ( stop_usage_.ru_stime.tv_sec - start_usage_.ru_stime.tv_sec ) 
    		+ (stop_usage_.ru_stime.tv_usec - start_usage_.ru_stime.tv_usec)/1000000.;
    totalTime_  += ( stop_.tv_sec-start_.tv_sec ) 
    		+ (stop_.tv_usec-start_.tv_usec)/1000000.;
}

void runningTimer::eval( )
{
    if (start_.tv_sec == 0) return;

    struct rusage running_usage_;
    getrusage( RUSAGE_SELF, &running_usage_ );

    struct timeval running_time_;
    gettimeofday( &running_time_, 0 );

    userRunTime_   = ( running_usage_.ru_utime.tv_sec - start_usage_.ru_utime.tv_sec ) 
                   + (running_usage_.ru_utime.tv_usec - start_usage_.ru_utime.tv_usec)/1000000.;
    systemRunTime_ = ( running_usage_.ru_stime.tv_sec - start_usage_.ru_stime.tv_sec ) 
                   + (running_usage_.ru_stime.tv_usec - start_usage_.ru_stime.tv_usec)/1000000.;
    totalRunTime_  = ( running_time_.tv_sec-start_.tv_sec )
                   + (running_time_.tv_usec-start_.tv_usec)/1000000.;
}

