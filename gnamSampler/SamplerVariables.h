#ifndef _SAMPLER_VARIABLES_H_
#define _SAMPLER_VARIABLES_H_

#include "gnamsamplerdal/GnamSamplerApplication.h"
#include "gnamSampler/SamplerConsts.h"

#include <stdint.h>
#include <vector>
#include <string>

namespace daq { namespace gnamSampler { class SamplerVariables; } }

class daq::gnamSampler::SamplerVariables
 {
  public:
   SamplerVariables (int argc, const char * const *argv);
   ~SamplerVariables () noexcept;
   SamplerVariables& operator= (const gnamsamplerdal::GnamSamplerApplication* GnamDB);
   std::string Dump() noexcept;

  private:
   std::string m_partition;

   // cmdline
   bool m_withFSM;
   bool m_isinteractive;
   std::string m_parent;
   std::string m_segment;
   std::string m_processname;
    
   // sampler type, name
   std::string m_type, m_name;
   // sampler mode
   SamplingMode m_mode;
   // sampling period, rate
   uint32_t m_period, m_rate;
   // event to skip, to take
   uint32_t m_ntake, m_nskip;
   // list file name
   std::string m_filelist;
   // event selection
   EventSelection m_selection;
   // sampling (LVL1 ids or global ids) list
   const std::vector<uint32_t>* m_l1ids;
   const std::vector<uint32_t>* m_gids;
   // sampling trigger type list
   const std::vector<uint32_t>* m_tts;
   // sampling sub-detector id list
   const std::vector<uint32_t>* m_dets;
   // sampling source id list
   const std::vector<uint32_t>* m_sids;

  public:
   inline const std::string& get_partition () const { return m_partition; }
   inline bool withFSM () const { return m_withFSM; }
   inline bool get_isinteractive () const { return m_isinteractive; }
   inline const std::string& get_parent () const { return m_parent; }
   inline const std::string& get_segment () const { return m_segment; }
   inline const std::string& get_processname () const { return m_processname; }
   inline const std::string& get_type () const { return m_type; }
   inline const std::string& get_name () const { return m_name; }
   inline SamplingMode get_mode () const { return m_mode; }
   inline uint32_t get_period () const { return m_period; }
   inline uint32_t get_rate () const { return m_rate; }
   inline uint32_t get_ntake () const { return m_ntake; }
   inline uint32_t get_nskip () const { return m_nskip; }
   inline const std::string& get_filelist () const { return m_filelist; }
   inline EventSelection get_selection() const { return m_selection; }
   inline const std::vector<uint32_t>* get_l1ids () const { return m_l1ids; }
   inline const std::vector<uint32_t>* get_gids () const { return m_gids; }
   inline const std::vector<uint32_t>* get_tts () const { return m_tts; }
   inline const std::vector<uint32_t>* get_dets () const { return m_dets; }
   inline const std::vector<uint32_t>* get_sids () const { return m_sids; }
   inline void set_mode ( SamplingMode sm ) { m_mode = sm; }
   inline void set_filelist ( std::string fl ) { m_filelist = fl; }
   inline void set_selection( EventSelection es ) { m_selection = es; }
 };

#endif // _SAMPLER_VARIABLES_H_
