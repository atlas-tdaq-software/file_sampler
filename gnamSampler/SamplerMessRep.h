#ifndef SAMPLER_MESS_REP_HDR
#define SAMPLER_MESS_REP_HDR

#ifndef __cplusplus
#error SamplerMessRep cannot be included in plane C programs
#endif

#include <ers/ers.h>

#define MR_DEBUG( level, mess) do { \
    ERS_DEBUG (level, mess); \
} while (false)

#define MR_INFO(mess) do { \
    ERS_INFO (mess); \
} while (false) 

#define MR_WARNING(issue) do { \
    ers::warning (issue); \
} while (false)

#define MR_ERROR(issue) do { \
    ers::error (issue); \
} while (false)

#define MR_FATAL(issue) do { \
    ers::fatal (issue); \
} while (false)

#endif // SAMPLER_MESS_REP_HDR
