#ifndef _BCALLBACK_H_
#define _BCALLBACK_H_

#include <stdint.h>
#include <vector>

class bCallback
  {
    public:
      virtual bool Execute() const =0;
  };


template <class cInstance>
class TCallback : public bCallback
  {
    public:
      typedef bool (cInstance::*tFunction)(const std::vector <uint32_t>* p1, uint32_t* p2);

      TCallback (cInstance*                      cInstancePointer,
                 tFunction                       pFunctionPointer,
                 const std::vector <uint32_t>*   p1,
                 uint32_t*                       p2)
        {
          cInst     = cInstancePointer;
          pFunction = pFunctionPointer;
          par1      = p1;
          par2      = p2;
        }

      virtual inline bool Execute() const
        {
          if (cInst && pFunction) return (cInst->*pFunction)(par1,par2);
          return false;
        }

    private:
      cInstance*                      cInst;
      tFunction                       pFunction;
      const std::vector <uint32_t>*   par1;
      uint32_t*                       par2;
  };

#endif // _BCALLBACK_H_
