#ifndef _MY_BUFFER_H_
#define _MY_BUFFER_H_

#include <cstring>
#include <stdint.h>

namespace daq { namespace gnamSampler { class myBuffer; } }

class daq::gnamSampler::myBuffer
 {
  private:
   size_t m_unit, m_dim;
   uint32_t* m_ptr;
  public:
   myBuffer ();
   ~myBuffer ();
   bool resize ( uint32_t sz );
   uint32_t& operator[] ( size_t k );
   void fill ( const uint32_t* src, size_t n, size_t k=0 );
   void swap ( myBuffer& other );

   inline uint32_t* get ( size_t k=0 ) { return m_ptr+k; }
   inline uint32_t size () { return m_dim; }
 };

#endif // _MY_BUFFER_H_
