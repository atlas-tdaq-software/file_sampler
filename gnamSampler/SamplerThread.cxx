#include <iostream>

#include <ipc/partition.h>
#include <emon/EventSampler.h>

#include "gnamSampler/SamplerThread.h"
#include "gnamSampler/SamplerMessRep.h"
#include "gnamSampler/RawEvent.h"
#include "gnamSampler/FileEvent.h"

using namespace daq::gnamSampler;

typedef uint32_t (*evt_reader)(uint32_t**);

class MyPulledSampler : public emon::PullSampling
 {
  private:	
    SamplerInternals* m_internals;
    RawEvent* m_raw;

    uint32_t* m_event;
    size_t m_size;

  public:	
    MyPulledSampler ( SamplerInternals* ints, RawEvent* re ) :
      m_internals(ints),
      m_raw(re),
      m_event(0),
      m_size(0)
     {
      m_internals->addConsumer();
     }

    ~MyPulledSampler()
     {
      m_internals->removeConsumer();
     }

    inline void sampleEvent( emon::EventChannel & cc )
     {
      if (m_internals->grantEvent())
       {
        m_raw->read_lock();
        m_size = m_raw->readEvent ( m_event );
        m_size ? cc.pushEvent( m_event, m_size ) : m_internals->force_stop();
        m_raw->read_unlock();
       }
     }

 };

class MySamplingFactory : public emon::PullSamplingFactory
 {
  private:
    SamplerInternals* m_internals;
    RawEvent* m_re;

  public:
    MySamplingFactory ( SamplerVariables* vars, SamplerInternals* ints ) :
      m_internals ( ints )
     {
      (vars->get_selection() == NONE) ?
          (m_re = new DummyEvent) : (m_re = new FileEvent(*vars));
     }

    emon::PullSampling * startSampling( const emon::SelectionCriteria & ) 
                	/* throw ( emon::BadCriteria, emon::NoResources ) */
     {
      return new MyPulledSampler( m_internals, m_re );
     }
 };

extern "C" void* sampling_thr ( void* args )
 {
  SamplerThread* sthr = (SamplerThread*)args;
  return sthr->run();
 }

void* SamplerThread::run ()
 {
  SamplerVariables* vars = &m_variables;
  SamplerInternals* ints = &m_internals;

  ints->wait_configured();

  emon::SamplingAddress address ( vars->get_type(), vars->get_name() );

  emon::EventSampler* esampl(0);
  try
   {
    IPCPartition partition ( vars->get_partition() );
    esampl = new emon::EventSampler ( partition, address,
                                      new MySamplingFactory ( vars, ints ) );
   }
  catch ( emon::Exception & ex )
   {
    ers::fatal(ex);
   }

  sleep(UINT_MAX);

  pthread_exit(0);
 }

SamplerThread::SamplerThread ( SamplerVariables & vars, SamplerInternals & ints ) :
  m_variables ( vars ),
  m_internals ( ints )
 {
  pthread_attr_t attr;
  pthread_attr_init ( &attr );
  pthread_attr_setdetachstate ( &attr, PTHREAD_CREATE_JOINABLE );

  int status = pthread_create ( &m_thread, &attr, sampling_thr, (void*)this );

  pthread_attr_destroy ( &attr );

  if ( status < 0 ) throw status;
 }

SamplerThread::~SamplerThread ()
 {
  pthread_cancel ( m_thread );
  pthread_join ( m_thread, NULL );
 }

