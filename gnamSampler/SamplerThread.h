#ifndef _SAMPLER_THREAD_H_
#define _SAMPLER_THREAD_H_

#include <pthread.h>

#include "gnamSampler/SamplerVariables.h"
#include "gnamSampler/SamplerInternals.h"

namespace daq { namespace gnamSampler { class SamplerThread; } }

class daq::gnamSampler::SamplerThread
 {
  public:
   SamplerThread (SamplerVariables & svar, SamplerInternals & sint);
   ~SamplerThread();
   void* run();

  private:
   SamplerVariables & m_variables;
   SamplerInternals & m_internals;
   pthread_t m_thread;

 };

#endif // _SAMPLER_THREAD_H_
