#include <fstream>

#include "FileEvent.h"

using namespace daq::gnamSampler;

FileEvent::FileEvent( SamplerVariables & vars ) :
  m_dfile ( new myfRead ),
  m_eventformatrecords(NULL),
  m_rawfr ( new rawFileRecords(m_dfile) ),
  m_fsize ( 0 ), m_taken ( 0 ), m_left ( 0 ),
  m_fileRecords ( false ),
  m_cycle ( false ),
  m_evcnt ( 0 ),
  m_evsel ( 0 ),
  m_dtmask ( 0xff0000 ),
  m_sdmask ( 0xffffff ),
  m_fword ( 0 ),
  m_marker ( NOMRK ),
  m_mask1 ( EMPTYMSK ),
  m_mask2 ( EMPTYMSK ),
  m_evsize ( 0 ),
  m_children ( new uint32_tp[1024] ),
  m_robmx ( 1024 ),
  m_linecounts ( 0 ),
  m_variables ( vars ),
  m_workLevel ( vars.get_selection() ),
  m_evmax ( vars.get_ntake() ),
  m_nskip ( vars.get_nskip() ),
  m_l1ids ( vars.get_l1ids() ),
  m_gids ( vars.get_gids() ),
  m_tts ( vars.get_tts() ),
  m_dets ( vars.get_dets() ),
  m_sids ( vars.get_sids() )
 {
  m_nevtot = m_nevtinfile = 0;
  m_bsize = m_buffer1.size();
  m_info.l1_id = m_info.l1_tt = m_info.rod_sid = m_info.rob_sid = m_info.gl_id = 0;
  this->setList(m_variables.get_filelist());
  if ((m_l1ids) && (m_l1ids->size() == 0)) m_l1ids = NULL;
  if ((m_gids) && (m_gids->size() == 0)) m_gids = NULL;
  if ((m_tts) && (m_tts->size() == 0)) m_tts = NULL;
  if ((m_dets) && (m_dets->size() == 0)) m_dets = NULL;
  if ((m_dets) || ((m_sids) && (m_sids->size() == 0))) m_sids = NULL;
  if (m_dets) for ( uint i=0; i<m_dets->size(); i++ )
   {
    m_dtok.push_back((*m_dets)[i] << 16);
    m_dets = &m_dtok;
   }

#define SETCALLBACK(V,A,B,C) \
  do { V = (B) ? new TCallback<FileEvent> ( this, (A), (B), (C) ) : NULL; } while(0)

  SETCALLBACK ( m_l1sel, &FileEvent::selectEv, m_l1ids, &m_info.l1_id );
  SETCALLBACK ( m_gdsel, &FileEvent::selectEv, m_gids, &m_info.gl_id );
  SETCALLBACK ( m_ttsel, &FileEvent::selectEv, m_tts, &m_info.l1_tt );
  SETCALLBACK ( m_dtsel, &FileEvent::selectEv, m_dets, &m_info.det );
  bool isRob = (m_workLevel == ROB);
  SETCALLBACK ( m_sdsel, &FileEvent::selectEv, m_sids,
                isRob ? &m_info.rob_sid : &m_info.rod_sid );
  SETCALLBACK ( m_dtfrg, &FileEvent::selectRods, m_dets, &m_dtmask );
  SETCALLBACK ( m_sdfrg, isRob ? &FileEvent::selectRobs : &FileEvent::selectRods,
                m_sids, &m_sdmask );

  // bool ok = this->nextFile();
  // std::cout << time2str() << " Data file:" << (ok ? " " :  " NOT ") << "found\n";
 }

FileEvent::~FileEvent()
 {
  if (m_rawfr) delete m_rawfr;
  if (m_dfile) delete m_dfile;
  m_flist.clear();
  // if (m_l1sel) delete m_l1sel;
  // if (m_gdsel) delete m_gdsel;
  // if (m_ttsel) delete m_ttsel;
  // if (m_dtsel) delete m_dtsel;
  // if (m_sdsel) delete m_sdsel;
  // if (m_dtfrg) delete m_dtfrg;
  // if (m_sdfrg) delete m_sdfrg;
  std::cout << "Exiting after " << m_nevtot << " events sampled\n";
 }

std::ostream & operator<< (std::ostream& stream, FileEvent & data)
 {
  FileEvent * rwep = &data;
  uint32_t* ptr = rwep->dptr();
  size_t dim = rwep->dsize();
  uint32_t j = rwep->linecounts();
  uint16_t r = (j&3);
  if (r) stream << octPr((j-r)*4);
  while(r--) stream << "         ";
  for (size_t k=0; k<dim; k++)
   {
    if ((j&3) == 0) stream << octPr(j*4);
    stream << hexPr(ptr[k]);
    j++;
    if ((j&3) == 0) stream << "\n";
   }
  rwep->linecounts(j);
  if (j&3) stream << "\n";
  return stream;
 }

bool FileEvent::searchEvent ()
 {
  m_evsize = 0;
  if ((m_left == 0) || !m_dfile->isOpen() || m_dfile->isEoF() || (m_dfile->getPosition() == -1))
    if (!this->nextFile()) return false;
  if ((m_fword == 0) || ((m_evmax ) && (m_evsel >= m_evmax))) return false;

  m_evsize = (m_fileRecords) ? this->getFileRecordEvt()
            : (m_marker == RODMRK) ? this->getRod() : this->getEvt();

  if (m_evsize == 0) return this->searchEvent();

  m_evcnt ++;
  bool ok = (m_evcnt > m_nskip) && ((*m_checker)(m_buffer1.get(),m_evsize,m_info) == 0);

  size_t k = m_callback.size();
  for ( size_t i=0; ok && (i<k); i++) ok = ok && m_callback[i]->Execute();

  if (ok) m_evsel ++;

  return ok;
 }

size_t FileEvent::readEvent ( uint32_t* & evptr )
 {
  evptr = NULL;
  bool ok;

  do
    ok = this->searchEvent();
  while ((m_evsize) && (!ok));

  if (ok) evptr = m_buffer1.get();
  return m_evsize;
 }

void FileEvent::setList ( std::string lst )
 {
  m_flist.clear();
  std::ifstream xList( lst.c_str(), std::ifstream::in);
  if (xList.is_open())
   {
    char cbuf[65536];
    char* cptr;
    while(1)
     {
      xList.getline(cbuf,65536);
      if (xList.eof()) break;
      cptr=cbuf;
      while ((*cptr=='\t') || (*cptr==' ')) ++cptr;
      size_t len = strlen(cptr);
      char* eostr = cptr+len-1;
      while ((*eostr=='\t') || (*eostr==' ')) --eostr ;
      *(eostr+1) = '\0';
      if (*cptr!='#') m_flist.push_back(std::string(cptr));
     }
    xList.close();
   }
  m_itr = 0;
  uint i = m_flist.size();
  if ((i) && (m_flist[i-1].compare("-reload")==0))
   {
    m_flist.pop_back();
    m_cycle = (m_flist.size() != 0);
   }
 }

bool FileEvent::openRFile ( std::string & fname )
 {
  if (this->isOpen()) this->closeFile();
  EventSelection foundLevel;
  foundLevel = NONE;
  m_fileRecords = false;
  m_marker = NOMRK;
  m_mask1 = EMPTYMSK;
  m_mask2 = EMPTYMSK;
  m_evsize = 0;
  m_fword = 0;
  m_callback.clear();
  if ((m_workLevel == NONE) || ((m_evmax ) && (m_evsel >= m_evmax))) return false;
  if ( !m_dfile->openFile(fname) ) return false;
  uint32_t w = m_dfile->firstw();
  m_fsize = m_left = m_dfile->left()/sizeof(uint32_t);
  m_taken = 0;
  while (m_left && (m_fword==0))
   {
    if ((w == FSRMRK)||(w == MFSRMRK))
     {
      m_fileRecords = true;
     }
    else
     {
      m_dfile->readData(&w,1);
      m_taken ++; m_left --;
     }
    if (((w == FULLMRK)||(w == FSRMRK)||(w == MFSRMRK)) && (m_workLevel & (FULL|ROB|ROD)))
     {
      foundLevel = FULL;
      m_marker = FULLMRK;
      m_mask1 = FULLMSK;
      m_mask2 = FULLMSK;
      m_fword = w;
     }
    else if ((w == ROBMRK) && (m_workLevel & (ROB|ROD)))
     {
      foundLevel = ROB;
      m_marker = ROBMRK;
      m_mask1 = FULLMSK;
      m_mask2 = FULLMSK;
      m_fword = w;
     }
    else if ((w == RODMRK) && (m_workLevel & ROD))
     {
      foundLevel = ROD;
      m_marker = RODMRK;
      m_mask1 = FULLMSK;
      m_mask2 = FULLMSK;
      m_fword = w;
     }
    else if (((w & CALIBMASK1) == CALIBMRK) && (w & CALIBMASK2) &&
             (m_workLevel & CALIB))
     {
      foundLevel = CALIB;
      m_marker = CALIBMRK;
      m_mask1 = CALIBMASK1;
      m_mask2 = CALIBMASK2;
      m_fword = w;
     }
    else if (m_workLevel == CALIB) break;
   }
  if (foundLevel != NONE)
   {
    switch (foundLevel)
     {
      case FULL:
        m_checker = &::check_full;
        if (m_l1sel) m_callback.push_back(m_l1sel);
        if (m_gdsel) m_callback.push_back(m_gdsel);
        if (m_ttsel) m_callback.push_back(m_ttsel);
        if (m_dtfrg) m_callback.push_back(m_dtfrg);
        if (m_sdfrg) m_callback.push_back(m_sdfrg);
        break;
      case ROB:
        m_checker = &::check_rob;
        if (m_l1sel) m_callback.push_back(m_l1sel);
        if (m_ttsel) m_callback.push_back(m_ttsel);
        if (m_dtsel) m_callback.push_back(m_dtsel);
        if (m_sdsel) m_callback.push_back(m_sdsel);
        break;
      case ROD:
        m_checker = &::check_rod;
        if (m_l1sel) m_callback.push_back(m_l1sel);
        if (m_ttsel) m_callback.push_back(m_ttsel);
        if (m_dtsel) m_callback.push_back(m_dtsel);
        if (m_sdsel) m_callback.push_back(m_sdsel);
        break;
      case CALIB:
        m_checker = &::check_calib;
        break;
      default:
        m_checker = &::check_ok;
        break;
     }
   }
  else
    this->closeFile();

  return (foundLevel != NONE);
 }

bool FileEvent::nextFile()
 {
  std::cout << time2str() << " nextFile called after " << m_evcnt << " events\n";

  if (m_cycle && (m_itr == m_flist.size())) m_itr = 0;
  if (m_itr == m_flist.size()) return false;
  if ( ! this->openRFile(m_flist[m_itr]) )
   {
    m_flist.erase(m_flist.begin()+m_itr);
    return this->nextFile();
   }
  else
   {
    m_itr ++;
    return true;
   }
 }

int64_t FileEvent::findMarker()
 {
  uint32_t w = 0;
  while (m_left && (((w & m_mask1) != m_marker) || ((w & m_mask2) == 0)))
   {
    m_dfile->readData(&w,1);
    m_taken ++; m_left --;
   }
  if (m_left == 0)
   {
    this->closeFile();
    m_fword = 0;
   }
  else
    m_fword = w;
  return m_dfile->getPosition();
 }

bool FileEvent::resize ( size_t s )
 {
  if (m_bsize < s)
   {
    m_buffer1.resize(s);
    m_buffer2.resize(s);
    m_bsize = m_buffer1.size();
    return true;
   }
  return false;
 }

void FileEvent::stackfill()
 {
  if (m_fsize < 3) return;
  uint32_t hlen[3];
  m_dfile->readData(&hlen[0],1);
  int64_t tsize(3*sizeof(uint32_t));
  int64_t pos(0);
  uint32_t evsz(0);
  int64_t jsize(0);
  while (jsize < m_fsize)
   {
    pos = jsize*sizeof(uint32_t)+tsize;
    m_dfile->setPositionFromEnd(-pos);
    m_dfile->readData(&hlen[1],2);
    // int64_t posnow = m_dfile->getPosition();
    // std::cout << "pos now " << posnow << std::endl;
    evsz = hlen[0] + hlen[1] + hlen[2] + 3;
    jsize += evsz;
    if (jsize > m_fsize) break;
    m_stack.push(evsz);
   }
  std::cout << "ROD file # evt " << m_stack.size() << " size " << m_fsize << " / " << jsize << std::endl;
  return;
 }

size_t FileEvent::getRod()
 {
  if (m_stack.empty())
   {
    this->stackfill();
    if (m_stack.empty()) return 0;
    m_dfile->setPosition(0);
    m_taken = 0; m_left = m_fsize;
   }
  uint32_t evsz = m_stack.top();
  m_stack.pop();
  uint32_t* dest = m_buffer1.get();
  m_dfile->readData(dest,evsz);
  m_taken += evsz; m_left -= evsz;

  return size_t(evsz);
  
/*
  uint32_t hlen;
  m_dfile->readData(&hlen,1);
  m_taken ++; m_left --;
  uint32_t* dest = m_buffer1.get();
  *dest ++ = 0xee1234ee;
  *dest ++ = hlen;
  hlen += 1;                          // +3 (trailer) -2 (already read)
  m_dfile->readData(dest,hlen);
  m_taken += hlen; m_left -= hlen;
  dest += hlen;
  uint32_t nword = hlen+2;
  uint32_t w;
  do
   {
    nword ++;
    m_dfile->readData(&w,1);
    m_taken ++; m_left --;
    if (this->resize(nword)) dest = m_buffer1.get(nword-1);
    *dest ++ = w;
   }
  while ((w != m_marker) && (m_left > 0));
  if (w == m_marker) nword --;
  return size_t(nword);
*/
 }

size_t FileEvent::getFileRecordEvt()
 {
  if (!m_rawfr || m_rawfr->isEoF()) return 0;
  uint32_t* ptr;
  uint32_t mrk;
  uint32_t nw;
  while (!m_rawfr->haveData() && !m_rawfr->isEoF())
   {
    nw = m_rawfr->getRecord( ptr, mrk );
    m_taken += nw; m_left -= nw;
   }
  if (m_rawfr->isEoF()) return 0;
  nw = m_rawfr->getData( &m_buffer1, mrk );
  m_taken += nw; m_left -= nw;
  return m_rawfr->evsize();
 }

size_t FileEvent::getEvt()
 {
  uint32_t* dest = m_buffer1.get();
  uint32_t nword = 0;
  uint32_t sz, totsz;
  m_dfile->readData(&sz,1);
  m_taken ++; m_left --;
  if (m_marker == CALIBMRK)
   {
    *dest ++ = 0xee1234ee;
    nword ++;
    *dest ++ = m_fword;
    nword ++;
    *dest ++ = sz;
    nword ++;
    sz &= 0xffff;
    sz /= sizeof(uint32_t);
    totsz = sz+1;
   }
  else
   {
    *dest ++ = m_fword;
    nword ++;
    *dest ++ = sz;
    nword ++;
    totsz = sz;
   }
  if (this->resize(sz+1)) dest = m_buffer1.get(nword);
  m_dfile->readData(dest,sz-2);
  m_taken += sz-2; m_left -= sz-2;

  this->findMarker();

  if (1) if ((m_evcnt % 5000 ) == 0)
   {
    dest = m_buffer1.get();
    std::cout << time2str() << " **** " << m_evcnt << " size " << totsz
              << " dest [0] " << std::hex << dest[0]
              << " [1] " << dest[1]
              << " [2] " << dest[2]
              << " [3] " << dest[3]
              << " [" << totsz-4 << "] " << dest[totsz-4]
              << " [" << totsz-3 << "] " << dest[totsz-3]
              << " [" << totsz-2 << "] " << dest[totsz-2]
              << " [" << totsz-1 << "] " << dest[totsz-1]
              << std::dec << std::endl;
   }

  return size_t(totsz);
 }

bool FileEvent::selectEv ( const std::vector<uint32_t>* vlist, uint32_t* info )
 {
  size_t k=(*vlist).size();
  size_t i;
  for (i=0; i<k; i++) if (*info == (*vlist)[i]) break;
  return (k>i);
 }

bool FileEvent::selectRobs ( const std::vector<uint32_t>* vlist, uint32_t* mask )
 {
  FULLFRAGMENTT fe(m_buffer1.get());
  size_t nrob = fe.children( m_children.get(), m_robmx );
  if (nrob > m_robmx)
   {
    m_robmx = nrob;
    m_children.reset (new uint32_tp[m_robmx]);
    fe.children( m_children.get(), m_robmx );
   }

  bool somefound = false;
  uint32_t hl = m_buffer1[2];
  uint32_t offset = hl;
  m_buffer2.fill( m_buffer1.get(), hl );

  for ( size_t j=0; j<nrob; j++ )
   {
    ROBFRAGMENTT r(m_children[j]);
    uint32_t sd = r.rob_source_id();
    sd &= *mask;
    size_t k=(*vlist).size();
    size_t i;
    for (i=0; i<k; i++) if (sd == (*vlist)[i]) break;
    if (k>i)
     {
      uint32_t bl = m_children[j][1];
      m_buffer2.fill( m_children[j], bl, offset );
      offset += bl;
      somefound = true;
     }
   }
  if (somefound)
   {
    m_buffer2[1] = m_evsize = offset;
    m_buffer1.swap ( m_buffer2 );
   }
  return somefound;
 }

bool FileEvent::selectRods ( const std::vector<uint32_t>* vlist, uint32_t* mask )
 {
  FULLFRAGMENTT fe(m_buffer1.get());
  size_t nrob = fe.children( m_children.get(), m_robmx );
  if (nrob > m_robmx)
   {
    m_robmx = nrob;
    m_children.reset (new uint32_tp[m_robmx]);
    fe.children( m_children.get(), m_robmx );
   }

  bool somefound = false;
  uint32_t hl = m_buffer1[2];
  uint32_t offset = hl;
  m_buffer2.fill( m_buffer1.get(), hl );

  for ( size_t j=0; j<nrob; j++ )
   {
    ROBFRAGMENTT r(m_children[j]);
    uint32_t sd = r.rod_source_id();
    sd &= *mask;
    size_t k=(*vlist).size();
    size_t i;
    for (i=0; i<k; i++) if (sd == (*vlist)[i]) break;
    if (k>i)
     {
      uint32_t bl = m_children[j][1];
      m_buffer2.fill( m_children[j], bl, offset );
      offset += bl;
      somefound = true;
     }
   }
  if (somefound)
   {
    m_buffer2[1] = m_evsize = offset;
    m_buffer1.swap ( m_buffer2 );
   }
  return somefound;
 }
