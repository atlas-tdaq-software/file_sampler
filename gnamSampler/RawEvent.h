#ifndef _RAW_EVENT_H_
#define _RAW_EVENT_H_

#include <stdint.h>
#include <cstring>
#include <pthread.h>

namespace daq { namespace gnamSampler { class RawEvent; } }
namespace daq { namespace gnamSampler { class DummyEvent; } }

class daq::gnamSampler::RawEvent
 {
  private:
   pthread_mutex_t m_readmutex;

  public:
   RawEvent() { pthread_mutex_init ( &m_readmutex, NULL ); }
   virtual ~RawEvent() { pthread_mutex_destroy ( &m_readmutex ); }
   virtual size_t readEvent(uint32_t* & ptr) = 0;

   int read_lock()
    {
     pthread_testcancel();
     pthread_mutex_lock(&m_readmutex);
     return 0;
    }
   int read_unlock()
    {
     pthread_mutex_unlock(&m_readmutex);
     return 0;
    }

  protected:

 };

class daq::gnamSampler::DummyEvent: public RawEvent
 {
  uint32_t m_data;
  public:
   DummyEvent() { m_data=0xee1234ee; }
   virtual ~DummyEvent() {}
   size_t readEvent(uint32_t* & ptr)
    {
     ptr=&m_data;
     return 1;
    }
 };

#endif // _RAW_EVENT_H_
