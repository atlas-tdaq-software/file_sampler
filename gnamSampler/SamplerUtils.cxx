#include <cstring>
#include <dlfcn.h>
#include <iostream>
#include <sstream>

#include <boost/test/unit_test.hpp>

#include "SamplerUtils.h"

std::string daq::gnamSampler::time2str()
 {
  time_t now = time(NULL);
  char bufTime[32];
  ctime_r(&now, bufTime);
  memset(bufTime+24,0,1);
  return std::string(bufTime);
 }

std::string daq::gnamSampler::octPr ( uint32_t v )
 {
  std::stringstream ss;
  ss.str("");
  int k = (v < 010000000) ? 21 :
          (v < 0100000000) ? 24 :
          (v < 01000000000) ? 27 :
          (v < 010000000000) ? 30 : 33;
  for (int i=k; i; )
   {
    i -= 3;
    uint32_t w = (v>>i)&0x7;
    ss << w;
   }
  return std::string(ss.str());
 }

std::string daq::gnamSampler::hexPr ( uint32_t v )
 {
  std::stringstream ss;
  ss.str("");
  ss << std::hex << " ";
  for (int i=32; i; )
   {
    i -= 4;
    uint32_t w = (v>>i)&0xf;
    ss << w;
   }
  return std::string(ss.str());
 }

int daq::gnamSampler::check_ok ( uint32_t* , uint32_t , evtinfo& )
 {
  return 0;
 }

int daq::gnamSampler::check_calib ( uint32_t* , uint32_t , evtinfo& )
 {
  return 0;
 }

int daq::gnamSampler::check_rod ( uint32_t* p, uint32_t , evtinfo& info )
 {
  uint32_t* evt = p;
  info.l1_id = evt[5];
  info.l1_tt = evt[7];
  info.gl_id = 0;
  info.rod_sid = evt[3];
  info.det = info.rod_sid & 0xff0000;
  return 0;
 }

int daq::gnamSampler::check_rob ( uint32_t* p, uint32_t size, evtinfo& info )
 {
  int rcode = 0;
  ROBFRAGMENTT * f(NULL);
  try
   {
    f = new ROBFRAGMENTT(p);
    if (f->fragment_size_word() != size) throw 1;
    if (!f->check()) throw 2;
    info.l1_id = f->rod_lvl1_id();
    info.l1_tt = f->rod_lvl1_trigger_type();
    info.gl_id = 0;
    info.rod_sid = f->rod_source_id();
    info.rob_sid = f->rob_source_id();
    info.det = info.rob_sid & 0xff0000;
   }
  catch (...)
   {
    rcode = 3;
   }
  delete f;
  return rcode;
 }

int daq::gnamSampler::check_full ( uint32_t* p, uint32_t size, evtinfo& info )
 {
  int rcode = 0;
  FULLFRAGMENTT * f(NULL);
  try
   {
    f = new FULLFRAGMENTT(p);
    if (f->fragment_size_word() != size) throw 1;
    // if (!f->check()) throw 2;
    try
     {
      BOOST_CHECK(f->check());
     }
    catch (...)
     {
      // std::cout << " check_full ex.what() " << ex.what() << std::endl;
     }

    info.l1_id = f->lvl1_id();
    info.l1_tt = f->lvl1_trigger_type();
    info.gl_id = f->global_id();
   }
  catch (...)
   {
    rcode = 3;
   }
  delete f;
  return rcode;
 }

void* daq::gnamSampler::loadLib ( std::string libName )
 {
  ::dlerror();

  libName += ".so";
  libName = "lib" + libName;

  void * lib = ::dlopen(libName.c_str(), RTLD_NOW | RTLD_GLOBAL);

  char * st = ::dlerror();
  if (st)
   {
    std::cerr << libName << ": not opened *** " << st << std::endl;
    return NULL;
   }
  else if (!lib)
   {
    std::cerr << libName << ": dlopen fails for unknown reasons" << std::endl;
    return NULL;
   }
  else
    return lib;
 }

int daq::gnamSampler::unloadLib ( void* handle )
 {
  ::dlerror();
  return ::dlclose(handle);
 }


fRead* daq::gnamSampler::getReader ( void * lib )
 {
  if (lib==NULL) return NULL;

  ::dlerror();

  typedef fRead* (*fun)();
  fun sym;
  *(void **)(void*)(&sym) = ::dlsym(lib, "fReadFactory");

  char * st = ::dlerror();
  if (st)
   {
    std::cerr << "fReadFactory not found *** " << st << std::endl;
    return NULL;
   }
  else if (!sym)
   {
    std::cerr << "fReadFactory not found for unknown reasons" << std::endl;
    return NULL;
   }
  else
    return sym();
 }
