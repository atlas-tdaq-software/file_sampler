#ifndef _SAMPLER_INTERNALS_H_
#define _SAMPLER_INTERNALS_H_

#include <stdint.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#include <csignal>
#include <string>
#include <map>

#include "gnamSampler/SamplerConsts.h"
#include "gnamSampler/runningTimer.h"

namespace daq { namespace gnamSampler { class SamplerInternals; } }

namespace daq
 {
  namespace gnamSampler
   {
    enum RunStatus { UNLOADED=0, LOADED, CONFIGURED, RUNNING, PAUSED };
    enum kThreads { TH_MAIN, TH_SIGNAL, TH_LOOP, TH_FSM, TH_ESAMPLER };
   }
 }

#define PRINT_INTERVAL 120

class daq::gnamSampler::SamplerInternals
 {
  private:
    bool (SamplerInternals::*m_grant)();
    bool (SamplerInternals::*m_actual)();
    inline bool grant_exit () noexcept
     {
      this->wait_for_exit();
      return false;
     }
    inline bool grant_wait () noexcept
     {
      sem_wait(&m_run_sem);
      return false;
     }
    inline bool grant_any () noexcept
     {
      this->lock();
      m_eventsinrun++;
      m_eventstotal++;
      this->unlock();
      return true;
     }
    inline bool grant_clocked () noexcept
     {
      sem_wait(&m_clock_sem);
      this->lock();
      m_eventsinrun++;
      m_eventstotal++;
      this->unlock();
      return true;
     }
    inline bool grant_signalled () noexcept
     {
      sem_wait(&m_signal_sem);
      this->lock();
      m_eventsinrun++;
      m_eventstotal++;
      this->unlock();
      return true;
     }
    void checkStatus() noexcept;
    void lock() /* throw (int) */
     {
      int status = pthread_mutex_lock (&m_locker);
      if ( status ) throw status;
     }
    void unlock() /* throw (int) */
     {
      int status = pthread_mutex_unlock (&m_locker);
      if ( status ) throw status;
     }

  public:
    SamplerInternals ( bool withFSM );
    ~SamplerInternals ();

    void* sigcatch() noexcept;
    void* exit_thr() noexcept;
    inline bool exit() { return m_exit; }

    void set_Sampling ( SamplingMode md, uint32_t prd, uint32_t rt );

    inline void changeStatus ( RunStatus r ) noexcept { m_runstatus = r; this->checkStatus(); }
 

    inline void addConsumer () noexcept { m_nconsumers ++; this->checkStatus(); }
    inline void removeConsumer () noexcept { m_nconsumers --; this->checkStatus(); }

    inline bool grantEvent () noexcept { return (*this.*m_actual)(); }

    inline void reset_counters () noexcept
        {
          m_numsigint = m_numsigquit =
          m_numsigusr1 = m_numsigalrm =
          m_eventsinrun = 0;
        }

    inline uint32_t eventsinrun () const noexcept
        { return m_eventsinrun; }

    inline uint32_t eventstotal () const noexcept
        { return m_eventstotal; }

    inline uint32_t nsigint () const noexcept
        { return m_numsigint; }

    inline uint32_t nsigquit () const noexcept
        { return m_numsigquit; }

    inline uint32_t nsigusr1 () const noexcept
        { return m_numsigusr1; }

    inline uint32_t nsigalrm () const noexcept
        { return m_numsigalrm; }

    inline void sigadd ( int sig ) noexcept
        { sigaddset ( &m_sigset, sig ); }

    inline sigset_t * sigset () noexcept
        { return &m_sigset; }

    inline void threadreg ( kThreads key, pthread_t pid ) noexcept
        { m_pthid[key] = pid; }

    inline void threadunreg ( kThreads key ) noexcept
        { if (m_pthid.count(key)) m_pthid.erase(key); }

    void threadstart ( kThreads key, void* (*func)(void*), void* arg ); /* throw (int) */

    void threadstop ( kThreads key ) noexcept;

    std::string Dump() noexcept;
  
    void force_stop() noexcept;
    void signal_exit();
    void wait_for_exit();
    void signalmask(); /* throw (int); */

    inline void wait_configured() noexcept { sem_wait(&m_config_sem); }

  private:
    RunStatus m_runstatus;
    bool m_withFSM;
    SamplingMode m_mode;
    uint32_t m_period;
    uint32_t m_rate;
    uint32_t m_unext;
    uint64_t m_running;
    volatile uint32_t m_nconsumers;
    volatile uint32_t m_eventstotal;
    volatile uint32_t m_eventsinrun;
    time_t m_print;
    volatile uint32_t m_numsigint, m_numsigquit, m_numsigusr1, m_numsigalrm;
    sigset_t m_sigset;
    pthread_t m_sigthread2;
    pthread_mutex_t m_locker, m_exit_mutex;
    sem_t m_clock_sem, m_signal_sem, m_exit_sem, m_config_sem, m_run_sem;
    pthread_cond_t m_exit_cond;
    std::map<kThreads, pthread_t> m_pthid;
    bool m_exit;
    runningTimer timertotal, timerinrun;

 };

#endif // _SAMPLER_INTERNALS_H_
