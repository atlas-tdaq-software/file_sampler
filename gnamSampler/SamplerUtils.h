#ifndef _SAMPLER_UTILS_H_
#define _SAMPLER_UTILS_H_

#include <stdint.h>
#include <string>

#include "EventStorage/fRead.h"
#include "eformat/eformat.h"

#define ROBFRAGMENTT eformat::ROBFragment<const uint32_t *>
#define FULLFRAGMENTT eformat::FullEventFragment<const uint32_t *>

namespace daq
 {
  namespace gnamSampler
   {
    typedef struct evtinfo
      { uint32_t l1_id, l1_tt, det, rod_sid, rob_sid, gl_id; }
    evtinfo;

    std::string time2str();

    std::string octPr ( uint32_t v );
    std::string hexPr ( uint32_t v );

    int check_ok ( uint32_t* , uint32_t , evtinfo& );
    int check_calib ( uint32_t* , uint32_t , evtinfo& );
    int check_rod ( uint32_t* p, uint32_t , evtinfo& info );
    int check_rob ( uint32_t* p, uint32_t size, evtinfo& info );
    int check_full ( uint32_t* p, uint32_t size, evtinfo& info );

    void* loadLib ( std::string libName );
    int unloadLib ( void* handle );
    fRead* getReader ( void * lib );

   }
 }

#endif // _SAMPLER_UTILS_H_
