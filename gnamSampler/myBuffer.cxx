#include <cstdlib>

#include "myBuffer.h"

using namespace daq::gnamSampler;

myBuffer::myBuffer () :
  m_unit(sizeof(uint32_t)),
  m_dim(262144-1),
  m_ptr(new uint32_t[m_dim+1])
 { }

myBuffer::~myBuffer() { delete[] m_ptr; }

bool myBuffer::resize( uint32_t sz )
 {
  bool ok = (m_dim < sz);
  if (ok)
   {
    do m_dim += 262144; while (m_dim < sz);
    delete[] m_ptr;
    m_ptr = new uint32_t[m_dim+1];
   }
  return ok;
 }

uint32_t& myBuffer::operator[](size_t k)
 {
  this->resize(k);
  return m_ptr[k];
 }

void myBuffer::fill ( const uint32_t* src, size_t n, size_t k )
 {
  this->resize(n+k);
  memcpy ( m_ptr+k, src, m_unit*n );
 }

void myBuffer::swap ( myBuffer& other )
 {
  uint32_t* _p = m_ptr;
  uint32_t _s = m_dim;
  m_ptr = other.get();
  m_dim = other.size();
  other.m_ptr = _p;
  other.m_dim = _s;
 }
