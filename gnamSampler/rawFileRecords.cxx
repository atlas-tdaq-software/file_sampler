#include "compression/compression.h"
#include "rawFileRecords.h"

using namespace daq::gnamSampler;

static uint32_t readString( myfRead* reader, std::string& s )
 {
  uint32_t len;
  reader->readData(&len,1);
  uint32_t wsz = len / sizeof(uint32_t);
  uint32_t rem = len % sizeof(uint32_t);
  if (rem) wsz++;
  s = std::string(wsz*sizeof(uint32_t), '\0');
  char* b = const_cast<char*>(s.c_str());
  reader->readData((uint32_t*)b,wsz);
  if (rem) s.resize(len);
  return wsz+1;
 }

inline uint32_t* rawFileRecords::mergedfilestartR() { return m_mfsr; }
inline uint32_t* rawFileRecords::fileStartR() { return m_fsr; }
inline uint32_t* rawFileRecords::runParR() { return m_rpr; }
inline uint32_t* rawFileRecords::dataSepR() { return m_dsr; }
inline uint32_t* rawFileRecords::fileEndR() { return m_fer; }
inline uint32_t* rawFileRecords::rawDataR()
 { return (m_buffer_dec) ? m_buffer_dec->get() : NULL; }
// inline bool rawFileRecords::haveData() { return m_havedata; }
// inline bool rawFileRecords::isEoF() { return m_reader->isEoF(); }

rawFileRecords::rawFileRecords(  myfRead* reader )
     : m_reader(reader),
       m_mfsr(NULL), m_fsr(NULL), m_rpr(NULL), m_dsr(NULL), m_fer(NULL),
       m_fns(NULL), m_mdts(NULL),
       m_havedata(false), m_zlib(false),
       m_bfcmpsize(65536), m_bfdecsize(131072),
       m_buffer_in(new char[m_bfcmpsize]),
       m_dblksize(0), m_rawsize(0),
       m_buffer_dec(NULL),
       m_nevtot(0)
 {}
rawFileRecords::~rawFileRecords()
 {
  delete[] m_buffer_in;
  if (m_mdts) delete m_mdts;
  if (m_fns) delete m_fns;
  if (m_fer) delete[] m_fer;
  if (m_dsr) delete[] m_dsr;
  if (m_rpr) delete[] m_rpr;
  if (m_fsr) delete[] m_fsr;
  if (m_mfsr) delete[] m_mfsr;
 }
uint32_t rawFileRecords::readMRFH( uint32_t mrk )
 {
  if (m_mfsr) delete[] m_mfsr;
  m_zlib = false;
  m_havedata = false;
  uint32_t hd2[2];
  m_reader->readData(hd2,2);
  m_mfsr = new uint32_t[hd2[1]];
  m_mfsr[0] = mrk;
  m_mfsr[1] = hd2[0];
  m_mfsr[2] = hd2[1];
  m_reader->readData(&m_mfsr[3], hd2[1]-3);
  uint64_t filesize = m_mfsr[3];
  filesize |= uint64_t(m_mfsr[4]) << 32;
  uint32_t nevtinfile = m_mfsr[6];
  uint32_t filenr = m_mfsr[8];
  uint32_t runnr = m_mfsr[11];
  uint32_t lumiblnr = m_mfsr[12];
  std::cout << " readMRFH: size " << filesize
            << " # evt.s " << nevtinfile
            << " # files " << filenr
            << " run " << runnr
            << " lumiblock " << lumiblnr
            << std::endl;
  return hd2[1];
 }
uint32_t rawFileRecords::readFSR( uint32_t mrk )
 {
  if (m_fsr) delete[] m_fsr;
  m_zlib = false;
  m_havedata = false;
  uint32_t rsz;
  m_reader->readData(&rsz,1);
  m_fsr = new uint32_t[rsz];
  m_fsr[0] = mrk;
  m_fsr[1] = rsz;
  m_reader->readData(&m_fsr[2], rsz-2);
  std::cout << std::hex << " readFSR:";
  for (uint32_t j=0; j<rsz; j++)
   {
    std::cout << " [" << j << "] " << m_fsr[j];
   }
  std::cout << std::dec << std::endl;
  return rsz;
 }
uint32_t rawFileRecords::readFNS( uint32_t mrk )
 {
  if (m_fns) delete m_fns;
  m_fns = new name_string_record;
  m_fns->marker = mrk;
  uint32_t nw = 1;
  nw += readString ( m_reader, m_fns->appName);
  nw += readString ( m_reader, m_fns->fileNameCore);
  std::cout << std::hex << " readFNS: " << mrk << std::dec;
  std::cout << " appName " << m_fns->appName
            << " fileNameCore " << m_fns->fileNameCore << std::endl;
  return nw;
 }
uint32_t rawFileRecords::readMDTS( uint32_t mrk )
 {
  if (m_mdts) delete m_mdts;
  m_mdts = new metadata_string_record;
  m_mdts->marker = mrk;
  m_reader->readData(&(m_mdts->nfree),1);
  uint32_t nw = 2;
  bool foundzlib(false);
  std::cout << std::hex << " readMDTS: " << mrk << std::dec;
  for (uint32_t j=0; j<m_mdts->nfree; j++)
   {
    std::string s;
    nw += readString(m_reader, s);
    std::cout << " [" << j << "] " << s;
    if (s.find("Compression=zlib") != std::string::npos) foundzlib=true;
    m_mdts->metaStrings.push_back(s);
   }
  std::cout << std::endl;
  m_zlib = foundzlib;
  return nw;
 }
uint32_t rawFileRecords::readRPR( uint32_t mrk )
 {
  if (m_rpr) delete[] m_rpr;
  uint32_t rsz;
  m_reader->readData(&rsz,1);
  m_rpr = new uint32_t[rsz];
  m_rpr[0] = mrk;
  m_rpr[1] = rsz;
  m_reader->readData(&m_rpr[2], rsz-2);
  std::cout << std::hex << " readRPR:";
  for (uint32_t j=0; j<rsz; j++)
   {
    std::cout << " [" << j << "] " << m_rpr[j];
   }
  std::cout << std::dec << std::endl;
  return rsz;
 }
uint32_t rawFileRecords::readDSR( uint32_t mrk )
 {
  if (m_dsr) delete[] m_dsr;
  m_havedata = true;
  uint32_t rsz;
  m_reader->readData(&rsz,1);
  m_dsr = new uint32_t[rsz];
  m_dsr[0] = mrk;
  m_dsr[1] = rsz;
  m_reader->readData(&m_dsr[2], rsz-2);
  while(0)
   {
    std::cout << std::hex << m_nevtot << " readDSR:";
    for (uint32_t j=0; j<rsz; j++) std::cout << " [" << j << "] " << m_dsr[j];
    std::cout << std::dec << std::endl;
   }
  return rsz;
 }
uint32_t rawFileRecords::readFER( uint32_t mrk )
 {
  if (m_fer) delete[] m_fer;
  m_havedata = false;
  uint32_t rsz;
  m_reader->readData(&rsz,1);
  m_fer = new uint32_t[rsz];
  m_fer[0] = mrk;
  m_fer[1] = rsz;
  m_reader->readData(&m_fer[2], rsz-2);
  std::cout << std::hex << " readFER: " <<  m_reader->isEoF() << " pos " << m_reader->getPosition() << " size " <<  m_reader->size();
  for (uint32_t j=0; j<rsz; j++)
   {
    std::cout << " [" << j << "] " << m_fer[j];
   }
  std::cout << std::dec << std::endl;
  return rsz;
 }
uint32_t rawFileRecords::readEvent()
 {
  if ((!m_havedata) || (m_dsr == NULL)) return 0;
  m_rawsize = m_dsr[3];
  uint32_t nw = m_rawsize/sizeof(uint32_t);
  m_buffer_dec->resize(nw);
  uint32_t* buff = m_buffer_dec->get();
  m_reader->readData(buff,nw);
  return nw;
 }
uint32_t rawFileRecords::readComprEvent()
 {
  if ((!m_havedata) || (m_dsr == NULL)) return 0;
  m_dblksize = m_dsr[3];
  if (m_bfcmpsize < m_dblksize)
   {
    delete[] m_buffer_in;
    m_buffer_in = new char[m_dblksize];
    m_bfcmpsize = m_dblksize;
   }
  uint32_t nw = m_dblksize/sizeof(uint32_t);
  m_reader->readData((uint32_t*)m_buffer_in,nw);

  m_rawsize = 0;

  compression::zlibuncompress(m_decompressed, m_rawsize, (char*)m_buffer_in, m_dblksize);

  // uint32_t* xptr = static_cast<uint32_t*>(m_decompressed.handle());
  // uint32_t xnwr = m_rawsize/sizeof(uint32_t);
  m_buffer_dec->resize(m_rawsize/sizeof(uint32_t));
  memcpy(m_buffer_dec->get(), m_decompressed.handle(), m_rawsize);

  return nw;
 }
uint32_t rawFileRecords::getRecord( uint32_t* & ptr, uint32_t & mrk )
 {
  if (m_reader->isEoF()) return 0;
  uint32_t nw;
  m_reader->readData(&mrk,1);
  switch (mrk)
   {
    case MFSRMRK:
      if (m_mdts) delete m_mdts;
      if (m_fns) delete m_fns;
      if (m_fer) delete[] m_fer;
      if (m_dsr) delete[] m_dsr;
      if (m_rpr) delete[] m_rpr;
      if (m_fsr) delete[] m_fsr;
      if (m_mfsr) delete[] m_mfsr;
      m_mdts = NULL;
      m_fns = NULL;
      m_fer = NULL;
      m_dsr = NULL;
      m_rpr = NULL;
      m_fsr = NULL;
      m_mfsr = NULL;
      nw = this->readMRFH(mrk);
      ptr = m_mfsr;
      break;
    case FSRMRK:
      if (m_mdts) delete m_mdts;
      if (m_fns) delete m_fns;
      if (m_fer) delete[] m_fer;
      if (m_dsr) delete[] m_dsr;
      if (m_rpr) delete[] m_rpr;
      if (m_fsr) delete[] m_fsr;
      m_mdts = NULL;
      m_fns = NULL;
      m_fer = NULL;
      m_dsr = NULL;
      m_rpr = NULL;
      m_fsr = NULL;
      nw = this->readFSR(mrk);
      ptr = m_fsr;
      break;
    case FNSMRK:
      nw = this->readFNS(mrk);
      ptr = (uint32_t*)&m_fns;
      break;
    case MDTSMRK:
      nw = this->readMDTS(mrk);
      ptr = (uint32_t*)&m_mdts;
      break;
    case RPRMRK:
      nw = this->readRPR(mrk);
      ptr = m_rpr;
      break;
    case DSRMRK:
      nw = this->readDSR(mrk);
      ptr = m_dsr;
      break;
    case FENDMRK:
      nw = this->readFER(mrk);
      ptr = m_fer;
      break;
    default:
      ptr = NULL;
      nw = 1;
      break;
   }
  return nw;
 }
uint32_t rawFileRecords::getData( myBuffer* ptr, uint32_t & mrk )
 {
  if (m_reader->isEoF()) return 0;
  m_buffer_dec = ptr;
  uint32_t nw = (m_zlib) ? this->readComprEvent() : this->readEvent();
  if (nw)
   {
    mrk = (*ptr)[0];
    m_nevtot ++;
   }
  m_havedata = false;
  return nw;
 }

