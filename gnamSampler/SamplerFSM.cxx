#include <csignal>
#include <ctime>
#include <iostream>

#include <dal/Partition.h>
#include <dal/util.h>
#include <RunControl/FSM/FSMStates.h>
#include <RunControl/FSM/FSMCommands.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/OnlineServices.h>

#include "gnamSampler/SamplerException.h"
#include "gnamSampler/SamplerMessRep.h"
#include "gnamSampler/SamplerFSM.h"

using namespace daq::gnamSampler;

SamplerFSM::SamplerFSM ( SamplerVariables & vars, SamplerInternals & ints ) :
  daq::rc::Controllable(),
  m_variables ( vars ),
  m_internals ( ints ),
  m_confDB ( 0 )
 {
  m_internals.changeStatus(LOADED);
 }

SamplerFSM::~SamplerFSM () noexcept
 {
  if ( m_confDB != 0 )
   {
    m_confDB->unload();
    delete m_confDB;
    m_confDB=0;
   }
  m_internals.changeStatus(UNLOADED);
 }

void SamplerFSM::prepareForRun (const daq::rc::TransitionCmd&)
 {
  m_internals.reset_counters();
  m_internals.changeStatus(RUNNING);
 }

void SamplerFSM::stopRecording (const daq::rc::TransitionCmd&)
 {
  MR_INFO ("Events sent: " << m_internals.eventsinrun());
  m_internals.changeStatus(CONFIGURED);
 }

void SamplerFSM::configure (const daq::rc::TransitionCmd&)
 {
  timertotal.start();

  if (!m_confDB) m_confDB = new Configuration("");

  if (!m_confDB->loaded())
   {
    if (m_variables.get_isinteractive()) MR_WARNING (MissingTDAQ_DB (ERS_HERE));
    ConfDBNotFound issue (ERS_HERE);
    FatalError (issue);
   }
  else
   {
    MR_DEBUG (1, "Configuration DB loaded");
    const daq::core::Partition& partition =
        daq::rc::OnlineServices::instance().getPartition ();

    m_confDB->register_converter(new daq::core::SubstituteVariables (partition));

   }
  //looks for us in the DB
  const gnamsamplerdal::GnamSamplerApplication* m_gnamDB
      = m_confDB->get<gnamsamplerdal::GnamSamplerApplication>(m_variables.get_processname(),true);

  if (m_gnamDB)
    MR_DEBUG (1, "Configuration object found");
  else
   {
    ConfObjNotFound issue(ERS_HERE,m_variables.get_processname());
    FatalError(issue);
   }

  m_variables = m_gnamDB;

  MR_INFO ("Configuration loaded: " << m_variables.Dump());

  m_internals.set_Sampling ( m_variables.get_mode(), m_variables.get_period(),
                             m_variables.get_rate() );

  // signal(SIGUSR2, sigUsr2Handler);

  m_internals.threadreg ( TH_FSM, pthread_self() );

  m_internals.changeStatus(CONFIGURED);

  MR_INFO (m_variables.get_processname() << " configured ...");
 }

void SamplerFSM::unconfigure (const daq::rc::TransitionCmd&)
 {
  if (1) MR_INFO ("unconfigure ()");

  if ( m_confDB != 0 )
   {
    m_confDB->unload();
    delete m_confDB;
    m_confDB=0;
   }

  timertotal.stop();

  MR_INFO ("Total events sent: " << m_internals.eventsinrun());
  MR_INFO ("Total CPU Time is: "
           << (timertotal.userTime() + timertotal.systemTime()));
  MR_INFO ("Total Time is: " << timertotal.totalTime());

  m_internals.changeStatus(LOADED);
 }

void
SamplerFSM::onExit(daq::rc::FSM_STATE state) noexcept
{
    if (state == daq::rc::FSM_STATE::RUNNING) this->stopRecording( daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::STOPRECORDING) );
}

void SamplerFSM::run ()
 {
  MR_DEBUG (0,"run ()");
  MR_INFO ("SamplerFSM::run conf'ing");
  this->configure( daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::CONFIGURE) );
  MR_INFO ("SamplerFSM::run conf'd");
  this->prepareForRun( daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::START) );
  m_internals.wait_for_exit();
  this->stopRecording( daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::STOPRECORDING) );
  this->unconfigure( daq::rc::TransitionCmd(daq::rc::FSM_COMMAND::UNCONFIGURE) );
 }
