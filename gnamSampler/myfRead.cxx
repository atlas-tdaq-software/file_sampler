#include <ctime>
#include <cstdlib>
#include <iostream>
#include <map>

// #include "gnamSampler/SamplerVariables.h"
#include "SamplerUtils.h"
#include "myfRead.h"

using namespace daq::gnamSampler;
// using daq::gnamSampler::EventSelection;

myfRead::myfRead() : m_name(""), m_idx(0), m_size(0), m_firstw(0), m_used(NONE), m_libname(""), m_reader(0) {}
 
myfRead::~myfRead() {}

bool myfRead::openFile(std::string fileName)
 {
  m_name = "";
  m_size = 0;

  rformat trylib = ANY;

  // Control plugins libraries via file name prefixes.
  // Take the prefixes away from the name, in most cases anyway.
  if (fileName.find("gfal:")==0)
   {
    fileName.erase(0,std::string("gfal:").size());
    trylib = GFAL;
   }
  else if (fileName.find("eos:")==0)
   {
    fileName.erase(0,std::string("eos:").size());
    trylib = XROOTD;
   }
  else if (fileName.find("xrootd:")==0)
   {
    fileName.erase(0,std::string("xrootd:").size());
    trylib = XROOTD;
   }
  else if (fileName.find("file:")==0)
   {
    fileName.erase(0,std::string("file:").size());
    trylib = DISK;
   }
  else if (fileName.find("rfio:")==0)
   {
    fileName.erase(0,std::string("rfio:").size());
    trylib = RFIO;
   }
  else if (fileName.find("dcache:")==0)
   {
    fileName.erase(0,std::string("dcache:").size());
    trylib = DCACHE;
   }

  bool ok(false);
  if (trylib != ANY)
   {
    if (trylib != m_used) this->dllSet(trylib);
    ok = this->getFileInfo(fileName);
   }
  else
   {
    rformat kk[] = { DISK, GFAL, RFIO, DCACHE, XROOTD };
    for ( uint32_t i=0; i<sizeof(kk)/sizeof(rformat); i++ )
     {
      this->dllSet(kk[i]);
      ok = this->getFileInfo(fileName);
      if (ok) break;
     }
   }

  time_t now = time(NULL);
  char bufTime[32];
  ctime_r(&now, bufTime);
  memset(bufTime+24,0,1);

  std::cout << bufTime << " " << m_idx << " " << fileName;
  if (ok)
    std::cout << " size " << m_size  << " opened with "
              << m_libname << " " << getenv("LCG_RFIO_TYPE") << " first word: " << std::hex << m_firstw << std::dec << "\n";
  else if (trylib != ANY)
    std::cout << " NOT opened with " << m_libname << " " << getenv("LCG_RFIO_TYPE") << "\n";
  else
    std::cout << " NOT opened with ANY library " << getenv("LCG_RFIO_TYPE") << "\n";

  return ok;
 }

void myfRead::dllSet ( rformat rf )
 {
  if ((rf == NONE ) || (rf == ANY)) return;

  std::map < rformat, std::string > lname;
  lname[GFAL] = "fReadGfal";
  lname[XROOTD] = "fReadXRootD";
  lname[DISK] = "fReadPlain";
  lname[RFIO] = "fReadCastor";
  lname[DCACHE] = "fReaddCache";

  m_used = rf;
  m_libname = lname[m_used];
  if (m_plugins.count(rf) == 0)
   {
    m_plugins.insert(rf);
    void* libptr = loadLib(m_libname);
    m_loaded[rf] = getReader(libptr);
   }

  m_reader = m_loaded[rf];
 }

bool myfRead::getFileInfo(std::string fileName)
 {
  bool ok = false;
  if (m_reader)
   {
    m_reader->openFile(fileName);
    ok = m_reader->isOpen();
    if (ok)
     {
      m_name = fileName;
      m_idx ++;
      m_reader->readData((char*)&m_firstw, sizeof(uint32_t));
      m_reader->setPositionFromEnd(0);
      m_size=m_reader->getPosition();
      m_reader->setPosition(0);
     }
   }
  else
    std::cout << m_libname << " not available\n";

  return ok;
 }
