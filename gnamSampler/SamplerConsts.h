#ifndef _SAMPLER_CONSTS_H_
#define _SAMPLER_CONSTS_H_

namespace daq
 {
  namespace gnamSampler
   {
    enum EventSelection { INVALID=9999, NONE=0, CALIB=1, ROD=2, ROB=4, FULL=8, ANY=0xf };
    enum SamplingMode { Invalid=9999, FreeRunning=0, Clocked=1, Signalled=2 };
   }
 }

#endif // _SAMPLER_CONSTS_H_
