#ifndef SAMPLER_EXCEPTION
#define SAMPLER_EXCEPTION

#include <ers/ers.h>

#include <string.h>
#include <stdlib.h>

namespace daq
{

// Void Structure, Just For SemiColon
#define VSJFSC struct _VSJFSC_

    // ExitNow
    ERS_DECLARE_ISSUE (gnamSampler, ExitNow, "FATAL ERROR", ) VSJFSC;

    namespace gnamSampler
    {
        inline void FatalError (const std::exception &ex) /* throw (ExitNow) */
            __attribute__ ((noreturn));
        inline void FatalError (const std::exception &ex) /* throw (ExitNow) */
        {
            ExitNow issue (ERS_HERE, ex);
            ers::fatal (issue);
            //throw ExitNow (ERS_HERE, ex);
            abort();
        }
    }

    /***************
     * EXCEPTIONS
     ***************/

    ERS_DECLARE_ISSUE (gnamSampler, Exception, , ) VSJFSC;

    // Not Found

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, NotFound, Exception, , , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, ConfDBNotFound, NotFound,
     "Impossible to open the configuration DB",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, ConfObjNotFound, NotFound,
     "Configuration object '" << name << "' not found",
     , ((std::string) name)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, SamplerNotFound, NotFound,
     "Cannot connect to the sampler. " << message,
     , ((std::string) message)) VSJFSC;

    // Bad Configuration

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, BadConf, Exception, , , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, InvalidProcessName, BadConf
     , "Process name cannot contain '.' characters: " << name,
     , ((std::string) name)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, InvalidPartition, BadConf,
     "Invalid partition '" << part << "'",
     , ((std::string) part)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, NoPartition, BadConf,
     "environment variable TDAQ_PARTITION is not set",
     , ) VSJFSC;

    // Internal Error

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, InternalErrorBase, Exception, , , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, InternalError, InternalErrorBase,
     "INTERNAL ERROR (please contact the authors): " << message,
     , ((std::string) message)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, NeverHere, InternalErrorBase,
     "INTERNAL ERROR (please contact the authors): this line should never"
     " be executed",
     , ) VSJFSC;

    // miscellanea
     
    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, DLOpenError, Exception,
     "Error '" << error << "' loading library '" << library << "'",
     , ((std::string) error) ((std::string) library)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, ThreadTimeout, Exception,
     "Thread end timed out ",
     ,) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, BadROOTFileName, Exception,
     "invalid filename: " << filename,
     , ((std::string) filename)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, POSIXError, Exception,
     "system call '" << syscall << "'failed: " << strerror (errnum) << ": "
     << comment,
     , ((std::string) syscall) ((int) errnum) ((std::string) comment)) VSJFSC;

    /************
     * MESSAGES
     ***********/

    ERS_DECLARE_ISSUE (gnamSampler, Message, , ) VSJFSC;

    // OHS commands

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, BadCommand, Message,
     "bad command: " << command,
     , ((std::string) command)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, BadCommandHistogram, Message,
     "histogram " << histo << " not found: discarding command: " << command,
     , ((std::string) histo) ((std::string) command)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, BadCommandSyntax, Message,
     "bad syntax for command: " << command << " (" << message << ")",
     , ((std::string) command) ((std::string) message)) VSJFSC;

    // miscellanea
 
    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, EMONSamplerStopped, Message,
     "EventSampler stopped sampling; will try and reconnect later",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, dlcloseError, Message,
     "dlclose (" << lib << "): " << error,
     , ((std::string) lib) ((std::string) error)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, MissingTDAQ_DB, Message,
     "In interactive mode the TDAQ_DB variable must be set",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, OHSProvider, Message,
     "Error received while trying to create new OHRootProvider;"
     " histograms will not be published to OHS",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, ISRunNumber, Message,
     "Exception caught while trying to fetch the run number;"
     " setting run number to 0",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, ROOTFileExists, Message,
     "file " << file << " already exists; remaning it to " << backup,
     , ((std::string) file) ((std::string) backup)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, RepeatedHistoName, Message,
     "repeated histogram name: " << histo << "; discarding",
     , ((std::string) histo)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamSampler, HistoMemoryLeak, Message,
     "*** MEMORY LEAK RECOVERED ***: "
     << n_histo << " histograms left from " << dynalib,
     , ((unsigned int) n_histo) ((std::string) dynalib)) VSJFSC;
} 

#endif // SAMPLER_EXCEPTION
