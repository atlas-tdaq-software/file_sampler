#include <sys/time.h>
#include <cstring>
#include <string>
#include <sstream>
#include <iostream>

#include "gnamSampler/SamplerInternals.h"

using namespace daq::gnamSampler;

uint64_t xusleep ( uint32_t usec )
 {
  struct timespec treq, trem;
  treq.tv_sec = time_t(usec/1000000);
  treq.tv_nsec = (usec%1000000)*1000L;
  if ( nanosleep ( &treq, &trem ) != 0 )
   {
    uint64_t tm = trem.tv_sec;
    tm *= 1000000;
    tm += trem.tv_nsec / 1000;
    return tm;
   }
  return 0;
 }

uint64_t utime()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  uint64_t tm = tv.tv_sec;
  tm *= 1000000;
  tm += tv.tv_usec;
  return tm;
}

extern "C" void* sig_thread ( void* args )
 {
  SamplerInternals* ints = (SamplerInternals*)args;
  return ints->sigcatch();
 }

extern "C" void* sig_thread2 ( void* args )
 {
  SamplerInternals* ints = (SamplerInternals*)args;
  return ints->exit_thr();
 }

void* SamplerInternals::exit_thr () noexcept
 {
  while (!m_exit)
   {
    xusleep (100);
    time_t now = time(NULL);
    if ( now > m_print)
     {
      char bufTime[32];
      ctime_r(&now, bufTime);
      memset(bufTime+24,0,1);
      timertotal.eval();
      timerinrun.eval();
      uint32_t cput, wallt, cpur, wallr;
      cput = uint32_t((timertotal.userRunTime()+timertotal.systemRunTime())*1000+0.5);
      wallt = uint32_t(timertotal.totalRunTime()*1000+0.5);
      cpur = uint32_t((timerinrun.userRunTime()+timerinrun.systemRunTime())*1000+0.5);
      wallr = uint32_t(timerinrun.totalRunTime()*1000+0.5);
      uint32_t loadt, loadr;
      loadt = (wallt) ? (cput*10000)/wallt : 0;
      loadr = (wallr) ? (cpur*10000)/wallr : 0;
      std::cout << bufTime << " " << m_eventsinrun << " ev.s in "
                << wallr*0.001 << " sec " << " load(%) " << loadr*0.01 << "\n";
      m_print = now + PRINT_INTERVAL;
     }
    if (m_unext && m_nconsumers)
     {
      uint64_t now = utime();
      if (m_running == 0) m_running = now;
      while (m_running <= now)
       {
        ++ m_numsigalrm;
        sem_post(&m_clock_sem);
        m_running += m_unext;
       }
     }
   }
  sem_wait(&m_exit_sem);

  pthread_kill ( m_pthid[TH_MAIN], SIGTERM );
  // xusleep(1000);

  this->lock();
  this->signal_exit();
  this->unlock();

  this->threadunreg(TH_LOOP);

  pthread_exit(0);
 }

SamplerInternals::SamplerInternals ( bool withFSM ) :
        m_runstatus(UNLOADED),
        m_withFSM(withFSM),
        m_mode(FreeRunning),
        m_period(0),
        m_rate(0),
        m_nconsumers(0),
        m_eventstotal(0),
        m_eventsinrun(0),
        m_print(0),
        m_numsigint(0),
        m_numsigquit(0),
        m_numsigusr1(0),
        m_numsigalrm(0),
        m_exit(0)
 {
  timertotal.start();

  this->signalmask();

  sem_init(&m_clock_sem, 0, 0);
  sem_init(&m_signal_sem, 0, 0);
  sem_init(&m_exit_sem, 0, 0);
  sem_init(&m_run_sem, 0, 0);
  sem_init(&m_config_sem, 0, 0);

  pthread_mutex_init(&m_locker, 0);
  pthread_mutex_init(&m_exit_mutex, 0);
  pthread_cond_init(&m_exit_cond, 0);

  this->threadreg ( TH_MAIN, pthread_self() );

  this->threadstart ( TH_SIGNAL, sig_thread, (void*)this );
  this->threadstart ( TH_LOOP, sig_thread2, (void*)this );
 }

void SamplerInternals::set_Sampling
  ( SamplingMode md, uint32_t prd, uint32_t rt )
 {
  m_mode = md;
  m_period = prd;
  m_rate = rt;

  switch ( md )
   {
    case FreeRunning:
      m_grant = &SamplerInternals::grant_any;
      break;
    case Clocked:
      m_grant = &SamplerInternals::grant_clocked;
      break;
    case Signalled:
      m_grant = &SamplerInternals::grant_signalled;
      break;
    default:
      m_grant = &SamplerInternals::grant_wait;
      break;
   }
 
  m_running = 0;
  m_unext = 0;
  if (md == Clocked)
   {
    if (m_period)
      m_unext = m_period * 1000000;
    else if (m_rate) 
     {
      double p = 0.5+1E6/m_rate;
      m_unext = uint32_t(p);
     }
   }
 }

SamplerInternals::~SamplerInternals ()
 {
  this->threadstop ( TH_SIGNAL );
  this->threadstop ( TH_LOOP );
  timertotal.stop();

  pthread_cond_destroy(&m_exit_cond);

  pthread_mutex_destroy(&m_exit_mutex);
  pthread_mutex_destroy(&m_locker);

  sem_destroy(&m_config_sem);
  sem_destroy(&m_run_sem);
  sem_destroy(&m_exit_sem);
  sem_destroy(&m_signal_sem);
  sem_destroy(&m_clock_sem);

  std::ostringstream buf;
  if ( !m_withFSM )
   {
    // buf << " #SIGINT " << m_numsigint << " #SIGQUIT " << m_numsigquit;
   }
  // buf << " #SIGUSR1 " << m_numsigusr1 << " #SIGALRM " << m_numsigalrm;
  buf << "Total events received: " << m_eventstotal << " last run: " << m_eventsinrun
      << "\nTotal CPU Time is: " << (timertotal.userTime() + timertotal.systemTime())
      << "\nTotal Time is: " << timertotal.totalTime() << "\n";

  if (1) std::cout << "DONE " << buf.str() << std::endl;
 }

void SamplerInternals::checkStatus () noexcept
 {
  bool ok = (m_runstatus == RUNNING) && (m_nconsumers);
  if (ok)
   {
    m_actual = m_grant;
    timerinrun.start();
    sem_post(&m_run_sem);
   }
  else
   {
    timerinrun.stop();
    m_actual = &SamplerInternals::grant_wait;
   }
  if (m_runstatus == CONFIGURED) sem_post(&m_config_sem);
 }

std::string SamplerInternals::Dump () noexcept
 {
  std::ostringstream buf;

  buf << "\n  eventstotal = \"" << m_eventstotal << "\"";
  buf << "\n  eventsinrun = \"" << m_eventsinrun << "\"";
  if ( !m_withFSM )
   {
    buf << "\n  nsigint = \"" << m_numsigint << "\"";
    buf << "\n  nsigquit = \"" << m_numsigquit << "\"";
   }
  buf << "\n  nsigusr1 = \"" << m_numsigusr1 << "\"";
  buf << "\n  nsigalrm = \"" << m_numsigalrm << "\"";

  return buf.str();
 }

void SamplerInternals::signalmask() /* throw (int) */
 {
  sigemptyset ( &m_sigset );
  if (1 || !m_withFSM )
   {
    sigaddset ( &m_sigset, SIGINT );
    sigaddset ( &m_sigset, SIGQUIT );
   }
  sigaddset ( &m_sigset, SIGUSR1 );
  sigaddset ( &m_sigset, SIGALRM );
  int status = pthread_sigmask ( SIG_BLOCK, &m_sigset, NULL );
  if ( status ) throw status;
 }

void* SamplerInternals::sigcatch () noexcept
 {
  int signum;

  while(1)
   {
    sigwait ( &m_sigset, &signum );

    switch ( signum )
     {
      case SIGINT:
       ++ m_numsigint;
        break;
      case SIGQUIT:
        ++ m_numsigquit;
        break;
      case SIGUSR1:
        if (m_nconsumers)
         {
          ++ m_numsigusr1;
          sem_post(&m_signal_sem);
         }
        break;
      case SIGALRM:
        if (m_nconsumers)
         {
          ++ m_numsigalrm;
          sem_post(&m_clock_sem);
         }
        break;
     }
    if ( (signum==SIGINT) || (signum==SIGQUIT) )
     {
      this->force_stop ();
      break;
     }
   }

  this->threadunreg(TH_SIGNAL);
  pthread_exit(0);
 }

void SamplerInternals::threadstart ( kThreads key, void* (*func)(void*), void* arg ) /* throw(int) */
 {
  pthread_attr_t attr;
  pthread_attr_init ( &attr );
  pthread_attr_setdetachstate ( &attr, PTHREAD_CREATE_JOINABLE );

  pthread_t mthr;
  int status = pthread_create ( &mthr, &attr, func, arg );

  pthread_attr_destroy ( &attr );

  if ( status < 0 ) throw status;

  this->threadreg ( key, mthr );
 }

void SamplerInternals::threadstop ( kThreads key ) throw()
 {
  if (m_pthid.count(key))
   {
    pthread_cancel ( m_pthid[key] );
    pthread_join ( m_pthid[key], NULL );
    m_pthid.erase ( key );
   }
 }
 
void SamplerInternals::force_stop () noexcept
 {
  this->lock();
  m_grant = &SamplerInternals::grant_exit;
  m_actual = m_grant;
  m_exit = true;
  sem_post(&m_exit_sem);
  this->unlock();
 }

void SamplerInternals::signal_exit () 
 {
  int status = pthread_mutex_lock (&m_exit_mutex);
  if ( status ) throw status;
  status = pthread_cond_signal (&m_exit_cond);
  if ( status ) throw status;
  status = pthread_mutex_unlock (&m_exit_mutex);
  if ( status ) throw status;
 }

void SamplerInternals::wait_for_exit () 
 {
  int status = pthread_mutex_lock (&m_exit_mutex);
  if ( status ) throw status;
  status = pthread_cond_wait (&m_exit_cond, &m_exit_mutex);
  if ( status ) throw status;
  status = pthread_mutex_unlock (&m_exit_mutex);
  if ( status ) throw status;
 }
