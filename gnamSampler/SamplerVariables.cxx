#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <sstream>

#include <cmdl/cmdargs.h>
#include <RunControl/Common/CmdLineParser.h>
#include <RunControl/Common/Exceptions.h>

#include "gnamSampler/SamplerMessRep.h"
#include "gnamSampler/SamplerException.h"
#include "gnamSampler/SamplerVariables.h"

#include <boost/program_options.hpp>

std::string SelectModes[] = { "NONE", "CALIB", "ROD", "ROB", "FULL", "ANY" };
std::string SampleModes[] = { "FreeRunning", "Clocked", "Signalled" };

using namespace daq::gnamSampler;
namespace po = boost::program_options;

std::string env_var( const std::string & s )
 {
  std::string ret( s );
  std::string::size_type start = s.find( '$', 0 );
  if (start != std::string::npos)
   {
    start += 1;
    bool gr = ( s[start] == '{' );
    std::string::size_type end = s.find ( gr ? '}' : '/', start );
    if (end == std::string::npos) end = s.length();
    size_t len = end-start;
    std::string ss = gr ? s.substr(start+1,len-1) : s.substr(start,len);
    if (gr) len+=1;
    char* ev = getenv ( ss.c_str() );
    (ev) ?  ret.replace ( start-1, len+1, ev ) : ret.erase ( start-1, len+1 );
    return env_var(ret);
   }
  else
   {
    return ret;
   }
 }

SamplerVariables::SamplerVariables (int argc, const char * const *argv) 
 {
  // get partition from environment
  char * p = getenv ("TDAQ_PARTITION");
  std::string part( (p == NULL || *p == 0) ?  "PRIVATE" : p );
  m_partition = part;

  po::options_description desc("GNAM Event Sampler.");
  try {
       desc.add_options()("noFSM,x", "flag to turn off FSM");
       po::variables_map vm;
       po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
       po::notify(vm);

       bool using_FSM_arg = !(vm.count("noFSM"));

       // Parser for the command line options required by the Run Control framework
       // Note that the "help" functionality is enabled
       daq::rc::CmdLineParser cmdParser(argc, (char**)argv, true);

       /*
        * fill the variables from command line
        */
       m_withFSM = using_FSM_arg;
       m_isinteractive = cmdParser.interactiveMode() && using_FSM_arg;
       m_processname = cmdParser.applicationName();
       m_parent = cmdParser.parentName();
       m_segment = cmdParser.segmentName();
  }
  catch(daq::rc::CmdLineHelp& ex) {
       // Show the help message: note that both messages from the CmdLineParser and the specific parser are reported
       std::cout << desc << std::endl;
       std::cout << ex.message() << std::endl;
       throw;
  }
  catch(ers::Issue& ex) {
       // Any exception thrown during the ItemCtrl construction or initialization is a fatal error
       ers::fatal(ex);
       throw;
  }
  catch(boost::program_options::error& ex) {
       // This may come from the library used to parse this application's specific options
       ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
       throw;
  }

  m_type = "Mona";
  m_name = "Isa";
  m_mode = FreeRunning;
  m_period = 0;
  m_rate = 0;
  m_ntake = 0;
  m_nskip = 0;
  m_filelist = "";
  m_selection = NONE;
  m_l1ids = NULL;
  m_gids = NULL;
  m_tts = NULL;
  m_dets = NULL;
  m_sids = NULL;

 }

inline bool strCaseCmp (std::string & A, std::string & B)
 { 
  return ( strcasecmp ( A.c_str(), B.c_str() ) == 0 );
 }

inline bool strCaseCmp (std::string & A, const char* B)
 { 
  return ( strcasecmp ( A.c_str(), B ) == 0 );
 }

SamplerVariables::~SamplerVariables () noexcept {}

SamplerVariables& SamplerVariables::operator= (const gnamsamplerdal::GnamSamplerApplication* GnamDB) 
 {
  m_type = GnamDB->get_SamplerType();
  m_name = GnamDB->get_SamplerName();
  std::string md = GnamDB->get_SamplingMode();
  m_mode = (md.compare("") == 0) ||
           strCaseCmp(md,SampleModes[FreeRunning]) ? FreeRunning :
           strCaseCmp(md,SampleModes[Clocked]) ? Clocked :
           strCaseCmp(md,SampleModes[Signalled]) ? Signalled : Invalid;
  if (m_mode == Invalid) throw "INVALID RUNNING MODE";
  m_period = GnamDB->get_SamplingPeriod();
  m_rate = GnamDB->get_SamplingRate();
  m_ntake = GnamDB->get_NTake();
  m_nskip = GnamDB->get_NSkip();
  m_filelist = env_var( GnamDB->get_FileList() );
  std::string esel = GnamDB->get_EventTypeRequest();
  m_selection = (esel.compare("") == 0) ||
                strCaseCmp(esel,"ANY") ? ANY :
                strCaseCmp(esel,"FULL") ? FULL :
                strCaseCmp(esel,"ROB") ? ROB :
                strCaseCmp(esel,"ROD") ? ROD :
                strCaseCmp(esel,"CALIB") ? CALIB :
                strCaseCmp(esel,"NONE") ? NONE : INVALID;
  if (m_selection == INVALID) throw "INVALID SELECTION CRITERIA";
  if (m_filelist.compare("") == 0) m_selection = NONE;
  if ((m_selection == NONE) && (m_mode == FreeRunning)) m_mode = Clocked;
  m_period = GnamDB->get_SamplingPeriod();
  m_l1ids = &(GnamDB->get_Level1Ids());
  m_gids = &(GnamDB->get_GlobalIds());
  m_tts = &(GnamDB->get_TriggerTypes());
  m_dets = &(GnamDB->get_DetIds());
  m_sids = &(GnamDB->get_SourceIds());

  return *this;
 }

std::string SamplerVariables::Dump () noexcept
 {
  std::ostringstream buf;

  buf << "\n  isinteractive = \"" << (m_isinteractive ? "true" : "false") << "\"";
  buf << "\n  partition = \"" << m_partition << "\"";
  buf << "\n  parent = \"" << m_parent << "\"";
  buf << "\n  segment = \"" << m_segment << "\n  \"";
  buf << "\n  process name = \"" << m_processname << "\"";
  buf << "\n  sampler type = \"" << m_type << "\"";
  buf << "\n  sampler name = \"" << m_name << "\"";
  buf << "\n  sampler mode = \"" << m_mode << "\"";
  buf << "\n  sampler period = \"" << m_period << "\"";
  buf << "\n  sampler rate = \"" << m_rate << "\"";
  buf << "\n  ntake = \"" << m_ntake << "\"";
  buf << "\n  nskip = \"" << m_nskip << "\"";
  buf << "\n  file list = \"" << m_filelist << "\"";
  buf << "\n  selection = \"" << m_selection << "\"";

  return buf.str();
 }

