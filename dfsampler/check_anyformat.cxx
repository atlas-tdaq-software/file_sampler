//$Id$

//Dear emacs, this is -*- c++ -*-

/**
 * @file check.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andr� Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implements a dummy program that checks the event structure in
 * a file. Uncompression is done on-the-fly if needed
 */

#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"

/*int check_RODfragment( char * argv[])
{

  //file handling
  EventFormat::FileStorage fs(argv[1]);

  uint32_t next = 0;
  while ( !fs.eof() && fs.good() && next < fs.size()) { //keep reading ...
    EventFormat::Abstract::RODFragment f(&fs, next);
    EventFormat::Error my_error;
    if (!f.is_valid(&my_error)) {
      std::cout << "Found invalid event (traceback):" << std::endl;
      std::cout << my_error;
      std::exit(1);
    }
    next += 4*f.size_word(); //this gives the size in bytes!!!
  }
  std::cout << "The file " << argv[1] << " seems in order."
	    << std::endl;
  return 0;
}*/

#define CHECK(F) F.check()

int check_ROBfragment( char * argv[])
{

  //file handling
  //EventFormat::FileStorage fs(argv[1]);

  DataReader *pDR = pickDataReader(argv[1]);

  if(!pDR) {
    std::cout << "Problem opening or reading this file!\n";
    return -1;
  }


  while (pDR->good() && !pDR->endOfFile()) { //keep reading ...
    
    unsigned int eventSize;
    char *buf;

    DRError ecode = pDR->getData(eventSize,&buf);

    if(ecode==DRNOOK){
    std::cout << "Problem reading a fragment!\n";
    return 1;
    }

    uint32_t * event = (uint32_t *)buf;

    eformat::ROBFragment<uint32_t *> f(event);
    
    try{
      if (!CHECK(f)) {
	std::cout << "Found invalid event" << std::endl;
	return 1;
      }

    } catch (eformat::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught eformat issue: " << ex.what() << std::endl;
    } catch (ers::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught ERS issue: " << ex.what() << std::endl;
    } catch (std::exception& ex) {
      std::cerr << std::endl
		<< "Uncaught std exception: " << ex.what() << std::endl;
    } catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    delete[] buf;
  }
  
  std::cout << "The file " << argv[1] << " seems in order."
	    << std::endl;
  return 0;
}

int check_fullevent( char * argv[])
{
  DataReader *pDR = pickDataReader(argv[1]);

  if(!pDR) {
    std::cout << "Problem opening or reading this file!\n";
    return -1;
  }


  while (pDR->good() && !pDR->endOfFile()) { //keep reading ...
    
    unsigned int eventSize;
    char *buf;

    DRError ecode = pDR->getData(eventSize,&buf);

    if(ecode==DRNOOK){
    std::cout << "Problem reading a fragment!\n";
    return 1;
    }
    
    uint32_t * event = (uint32_t *)buf;

    eformat::FullEventFragment<uint32_t *> f(event);
    
    try{
      if (!CHECK(f)) {
	std::cout << "Found invalid event" << std::endl;
	return 1;
      }

    } catch (eformat::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught eformat issue: " << ex.what() << std::endl;
    } catch (ers::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught ERS issue: " << ex.what() << std::endl;
    } catch (std::exception& ex) {
      std::cerr << std::endl
		<< "Uncaught std exception: " << ex.what() << std::endl;
    } catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

    delete[] buf;
  }
  
  std::cout << "The file " << argv[1] << " seems in order."
	    << std::endl;
  return 0;



}

/**
 * The main application
 */
int main (int argc, char * argv[])
{
  unsigned char level;

  if ( argc < 2 ) {
    std::cerr << "usage: " << argv[0] << " <file> [<LEVEL (9ABCD)>]" << std::endl;
    std::exit(1);
  }

  level = (argc == 3) ? argv[2][0] : 'a';
  std::cout << "level " << level << std::endl;
  switch (level)
    {
      case '9':			break;
      case 'a': case 'A':	check_fullevent( argv); break;
      case 'd': case 'D':	check_ROBfragment( argv); break;
      //case 'e': case 'E':	check_RODfragment( argv); break;
      default:			check_fullevent( argv); break;
    }

  return 0;
}
