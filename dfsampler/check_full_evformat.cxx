//$Id$

//Dear emacs, this is -*- c++ -*-

/**
 * @file check.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andr� Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implements a dummy program that checks the event structure in
 * a file. Uncompression is done on-the-fly if needed
 */

#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"

#define CHECK(F) F.check()

/**
 * The main application
 */
int main (int argc, char** argv)
{
  if ( argc != 2 ) {
    std::cerr << "usage: " << argv[0] << " <file>" << std::endl;
    std::exit(1);
  }

   DataReader *pDR = pickDataReader(argv[1]);

  if(!pDR) {
    std::cout << "Problem opening or reading this file!\n";
    return -1;
  }


  while (pDR->good() && !pDR->endOfFile()) { //keep reading ...
    
    unsigned int eventSize;
    char *buf;

    DRError ecode = pDR->getData(eventSize,&buf);

    if(ecode==DRNOOK){
    std::cout << "Problem reading a fragment!\n";
    return 1;
    }
    
    uint32_t * event = (uint32_t *)buf;

    eformat::FullEventFragment<uint32_t *> f(event);
    
    try{
      if (!CHECK(f)) {
	std::cout << "Found invalid event" << std::endl;
	return 1;
      }

    } catch (eformat::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught eformat issue: " << ex.what() << std::endl;
    } catch (ers::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught ERS issue: " << ex.what() << std::endl;
    } catch (std::exception& ex) {
      std::cerr << std::endl
		<< "Uncaught std exception: " << ex.what() << std::endl;
    } catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
    }

  }

  std::cout << "The file " << argv[1] << " seems in order."
	    << std::endl;
  return 0;
}
