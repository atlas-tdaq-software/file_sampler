//$Id$

//Dear emacs, this is -*- c++ -*-

/**
 * @file check.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch>Andr� Rabello dos
 * ANJOS</a>
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Implements a dummy program that checks the event structure in
 * a file. Uncompression is done on-the-fly if needed
 */

#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <stdint.h>
#include <stdlib.h>
#include "EventStorage/pickDataReader.h"
#include "eformat/eformat.h"

DataReader * fs;
uint32_t (*reader)(unsigned long **,uint32_t &);
uint32_t next=0;  

#define CHECK(F) (F).check()

uint32_t ROBevent_reader(unsigned long ** buffer, uint32_t & evsize)
{
  bool repeat = true;
  unsigned int byteSize = 0;
  uint32_t * tmpbffr = NULL;

  evsize=0;

  while (fs->good() && !fs->endOfFile() && repeat) { //keep reading ...
    
    DRError ecode = fs->getData (byteSize, (char **) &tmpbffr);

    if(ecode==DRNOOK){
      std::cout << "Problem reading a fragment!\n";
      return evsize;
    }

    eformat::ROBFragment<uint32_t *> * f =
      new eformat::ROBFragment<uint32_t *>(tmpbffr);

    try{
      if (!CHECK(*f)) {
	std::cout << "Found invalid event" << std::endl;
	
      } else {
	repeat=false;
      }
      
    } catch (eformat::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught eformat issue: " << ex.what() << std::endl;
      
    } catch (ers::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught ERS issue: " << ex.what() << std::endl;
      
    } catch (std::exception& ex) {
      std::cerr << std::endl
		<< "Uncaught std exception: " << ex.what() << std::endl;
      
    } catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
      
    }

    delete f;
  }

  if(!repeat)
    {
      
      *buffer =(unsigned long *)realloc(*buffer,byteSize);
      
      memcpy(*buffer,tmpbffr,byteSize);

      evsize=byteSize/sizeof(unsigned long);
    }

  return evsize;
}

uint32_t fullevent_reader(unsigned long ** buffer, uint32_t & evsize)
{
  bool repeat = true;
  unsigned int byteSize = 0;
  uint32_t * tmpbffr = NULL;

  evsize=0;

  while (fs->good() && !fs->endOfFile() && repeat) { //keep reading ...
    
    DRError ecode = fs->getData (byteSize, (char **) &tmpbffr);

    if(ecode==DRNOOK){
      std::cout << "Problem reading a fragment!\n";
      return evsize;
    }

    eformat::FullEventFragment<uint32_t *> * f =
      new eformat::FullEventFragment<uint32_t *>(tmpbffr);

    try{
      if (!CHECK(*f)) {
	std::cout << "Found invalid event" << std::endl;
	
      } else {
	repeat=false;
      }
      
    } catch (eformat::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught eformat issue: " << ex.what() << std::endl;
      
    } catch (ers::Issue& ex) {
      std::cerr << std::endl
                << "Uncaught ERS issue: " << ex.what() << std::endl;
      
    } catch (std::exception& ex) {
      std::cerr << std::endl
		<< "Uncaught std exception: " << ex.what() << std::endl;
      
    } catch (...) {
      std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
      
    }

    delete f;
  }

  if(!repeat)
    {
      
      *buffer =(unsigned long *)realloc(*buffer,byteSize);
      
      memcpy(*buffer,tmpbffr,byteSize);

      evsize=byteSize/sizeof(unsigned long);
    }

  return evsize;
}

int init(char * filename, char level)
{
  fs = pickDataReader(filename);

  if(!fs) {
    std::cout << "Problem opening or reading this file!" << std::endl;
    return -1;
  }

  if(!fs->endOfFile() && fs->good())
    {
      switch (level)
	{
	case '9':			break;
	case 'a': case 'A': default :	reader = fullevent_reader; break;
	case 'd': case 'D':	reader = ROBevent_reader; break;
	  //case 'e': case 'E':	check_RODfragment( argv); break;
	}
      return 0;
    }
  else
    return 1;
}

/**
 * The main application
 */
int main (int argc, char * argv[])
{
  unsigned char level;
  unsigned long *buffer=NULL;
  uint32_t size;
  int i, j;
    
  if ( argc < 2 ) {
    std::cerr << "usage: " << argv[0] << " <file> [<LEVEL (9ABCDE)>]" << std::endl;
    std::exit(1);
  }

  level = (argc == 3) ? argv[2][0] : 'a';
  std::cout << "level " << level << std::endl;
  j = 0;
  if(!init(argv[1],level)){
    while(reader(&buffer,size)){
      std::cout << j++ << " size " << size << " data "; 
      
      for(i=0;i<2;i++)
	std::cout << "  " << std::hex << buffer[i] << std::dec; 
      
      std::cout <<" "<< std::endl;
      
      eformat::helper::SourceIdentifier source(buffer[4]);
      
      std::cout << " D " << std::hex << source.subdetector_id() <<std::dec
	<< " M " << std::hex << (int)source.module_id() << std::dec
	<< std::endl;
      
    }
  }
  return 0;
}

