//$Id$

//Dear emacs, this is -*- c++ -*-

/*
 * $Author$
 * $Revision$
 * $Date$
 *
 * Copies the input file to the output file, checking for event format errors.
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <stdint.h>
#include <string>
#include <stdio.h>
#include "eformat/eformat.h"
#include "EventStorage/pickDataReader.h"
#include "EventStorage/DataWriter.h"
#include "cmdl/cmdargs.h"

#define DEFAULT '0'
#define FULL    'a'
#define SUB     'b'
#define ROSL    'c'
#define ROBL    'd'
#define RODL    'e'

#define MAX_SUB   64
#define MAX_ROS  256
#define MAX_ROB 2048

#define SOURCE_OFFSET     4
#define ROD_SOURCE_OFFSET 3
#define LVL1_TYPE_OFFSET  7

DataWriter * pDW;
std::ofstream textFile;

char level;
bool text;
long writenr=0, trashnr=0, oknr=0;
long readnr=0;

int modID, subID;
unsigned int lvl1Type;
int nevts;
std::vector<int> lvl1_list;

#define CHECK(F) (F).check()

#define TFRAGPOINTER const uint32_t *

bool check_l1list( eformat::FullEventFragment<TFRAGPOINTER> * full )
 {
  bool got = (lvl1_list.size()==0);
  std::vector<int>::iterator it = lvl1_list.begin();
  int ll1 = full->lvl1_id();
  int glid = full->global_id();
  for (uint i=0; i<lvl1_list.size(); i++)
   {
    got |= (lvl1_list[i] == ll1);
    got |= (lvl1_list[i] == glid);
    if (got)
     {
      lvl1_list.erase(it);
      if (lvl1_list.size() == 0) nevts=1;
      break;
     }
    it ++;
   }
  return got;
 }

bool select(eformat::helper::SourceIdentifier source){
  
  bool selected=true;
  
  if(modID>0)
    if(source.module_id()!=modID)
      selected &=false;
  
  if(subID>0)
    if(source.subdetector_id()!=subID)
      selected &=false;

  return selected;
}


bool select_lvl1(uint32_t lvl1_type){

  if(lvl1Type>0)
    if(lvl1_type!=lvl1Type)
      return false;
    else
      return true;
  else
    return true;
}


unsigned int rob_write(eformat::ROBFragment<TFRAGPOINTER> * rob,
		       TFRAGPOINTER event, bool initial){

  eformat::helper::SourceIdentifier source(rob->source_id());
  eformat::helper::SourceIdentifier source_rod(rob->rod_source_id());

  if(level==ROBL || initial){
    std::cout << "Good ROB fragment " << ++oknr <<std::endl;
    
    if(select(source)){
      std::cout << "Selected ROB fragment " << ++writenr <<std::endl;
      
      pDW->putData((rob->fragment_size_word())*sizeof(uint32_t)
		   ,const_cast<uint32_t*>(event));
    } else {
      std::cout << "Not in the selection range ROB fragment " <<std::endl;
    }

  } else {
    std::cout << "Good ROD fragment " << ++oknr <<std::endl;

    if(text){
      textFile << "\t\t\tROB" <<std::endl;
      textFile << "\t\t\tVersion: 0x" << std::hex 
	       << rob->version() << std::dec << std::endl;
      textFile << "\t\t\tSource ID: 0x" << std::hex 
	       << rob->source_id() << std::dec <<std::endl;
      textFile << "\t\t\tModule ID: 0x" << std::hex 
	       << source.module_id() << std::dec <<std::endl;
      textFile << "\t\t\tSubDet ID: 0x" << std::hex 
	       << source.subdetector_id() << std::dec <<std::endl;
      	
      textFile << "\t\t\t\tROD" <<std::endl;
      textFile << "\t\t\t\tVersion: 0x" << std::hex 
	       << rob->rod_version() << std::dec << std::endl;
      textFile << "\t\t\t\tSource ID: 0x" << std::hex 
	       << rob->rod_source_id() << std::dec <<std::endl;
      textFile << "\t\t\t\tModule ID: 0x" << std::hex 
	       << source_rod.module_id() << std::dec <<std::endl;
      textFile << "\t\t\t\tSubDet ID: 0x" << std::hex 
	       << source_rod.subdetector_id() << std::dec <<std::endl;
      textFile << "\t\t\t\tRun No: " << rob->rod_run_no() << std::endl;
      textFile << "\t\t\t\tLvl1 ID: 0x" << std::hex
	       << rob->rod_lvl1_id() << std::dec << std::endl;
      textFile << "\t\t\t\tBC ID: 0x" << std::hex 
	       <<rob->rod_bc_id() << std::dec << std::endl;
      textFile << "\t\t\t\tLvl1 Type: 0x" << 
	std::hex << rob->rod_lvl1_trigger_type() << std::dec << std::endl;
      textFile << "\t\t\t\tDet Type: 0x" << std::hex 
	       << rob->rod_detev_type() << std::dec << std::endl;
    } else {
      if(select_lvl1(rob->rod_lvl1_trigger_type()) && select(source_rod)){
	std::cout << "Selected ROD fragment " << ++writenr <<std::endl;
	pDW->putData(((rob->fragment_size_word())-(rob->header_size_word()))
		     *sizeof(uint32_t),const_cast<uint32_t*>(event+rob->header_size_word()));
      }else {
	std::cout << "Not in the selection range ROD fragment " <<std::endl;
      }
    }
  }
  return 0;
}

unsigned int full_write(eformat::FullEventFragment<TFRAGPOINTER> * full,
			TFRAGPOINTER event, bool initial){
  
  eformat::helper::SourceIdentifier source(full->source_id());

  if(level==FULL || initial){
    std::cout << "Good FULL fragment " << ++oknr <<std::endl;

    if(select(source)){
      std::cout << "Selected FULL fragment " << ++writenr <<std::endl;
      pDW->putData((full->fragment_size_word())*sizeof(uint32_t)
		   ,const_cast<uint32_t*>(event));
    }else
      std::cout << "Not in the selection range FULL fragment " <<std::endl;
      
  } else {

    if(text){
      TFRAGPOINTER it;

      textFile << "FULLEVENT" <<std::endl;
      textFile << "Version: 0x" << std::hex 
	       << full->version() << std::dec << std::endl;
      textFile << "Source ID: 0x" << std::hex 
	       << full->source_id() << std::dec <<std::endl;
      textFile << "Module ID: 0x" << std::hex 
	       << source.module_id() << std::dec <<std::endl;
      textFile << "SubDet ID: 0x" << std::hex 
	       << source.subdetector_id() << std::dec <<std::endl;
      textFile << "Global ID: 0x" << std::hex 
	       << full->global_id() << std::dec <<std::endl;
      textFile << "Run NO: " 
	       << full->run_no() << std::endl;
      textFile << "LVL1 ID: 0x" << std::hex 
	       << full->lvl1_id() << std::dec <<std::endl;
      textFile << "LVL1 Type: 0x" << std::hex 
	       << (uint32_t)full->lvl1_trigger_type() << std::dec <<std::endl;
      textFile << "# LVL2 Info: "
	       << full->nlvl2_trigger_info()<<std::endl;
      full->lvl2_trigger_info(it);
      for(uint32_t i=0;i < full->nlvl2_trigger_info();i++){
	textFile << "LVL2 Info: 0x" << std::hex 
		 << it[i] << std::dec <<std::endl;
      }
      textFile << "# EF Info: "
	       << full->nevent_filter_info()<<std::endl;
      full->event_filter_info(it);
      for(uint32_t i=0;i < full->nevent_filter_info();i++){
	textFile << "EF Info: 0x" << std::hex 
		 << it[i] << std::dec <<std::endl;
      }
    }
    
    TFRAGPOINTER robp[MAX_ROB];
    uint32_t nrob = full->children(robp, MAX_ROB);
    
    for (uint32_t i=0; i<nrob;i++) {
      eformat::ROBFragment<TFRAGPOINTER> * rob = 
	new eformat::ROBFragment<TFRAGPOINTER>(robp[i]);
      rob_write(rob,robp[i],false);
      delete rob;
    }

  }
  
  return 0;
}

int main (int argc, char** argv)
{

  CmdArgStr in_file ('i', "file", "input_file", 
		     "file to be analysed",CmdArg::isREQ);
  CmdArgStr out_app ('a', "application tag", "application_tag", 
		     "application tag for the output file name");
  CmdArgStr out_file ('o', "file tag", "output_file_tag", 
		      "tag for the file to be written", CmdArg::isREQ);
  CmdArgStr out_dir ('d', "directory", "output_directory", 
		     "output directory",CmdArg::isREQ);
  CmdArgChar level_in ('l',"level","output_level",
		       "output level [abcde]");
  CmdArgInt offevents ('f',"offset","offset",
		       "event offset");
  CmdArgInt nevents ('n',"events","event_number",
		     "number of events");
  CmdArgInt module ('m',"module","module_id",
		    "module id for event selection");
  CmdArgInt subd ('s',"subdetector","subdetector_id",
		  "subdetector id for event selection");
  CmdArgInt lvl1 ('t',"trigger","trigger_type",
		  "lvl1 trigger type for event selection (only if the output level is 'e')");
  CmdArgIntList l1List ('I', "l1Ids", "level1_ids", "level1 ids");

  CmdArgBool text_out ('w',"text",
		       "write the event properties in a text file (not yet completed).");

  CmdLine       cmd(*argv, &in_file, &out_app, &out_file, &out_dir,
		    &level_in, &offevents, &nevents, &module, 
		    &subd, &lvl1,&l1List,&text_out,0);

  CmdArgvIter   arg_iter(--argc, ++argv);

  cmd.description("Copy a data file selecting events");
 
  level_in=DEFAULT;
  out_app="";
  out_file="";
  nevents=0;
  subd=0;
  module=0;
  lvl1=0;
  offevents=0;
     
  cmd.parse(arg_iter);
 
  level=level_in;
  nevts=nevents;
  modID=module;
  subID=subd;
  lvl1Type=lvl1;
  text=text_out;
  if (l1List.flags() && CmdArg::GIVEN)
    for (uint i=0; i<l1List.count(); i++)
      lvl1_list.push_back(l1List[i]);

  DataReader *pDR = pickDataReader((std::string(in_file)).c_str());

  if(!pDR) {
    std::cout << "Problem opening or reading this file: " 
	      << in_file <<std::endl;;
    return -1;
  }

  // Optional meta data in free strings
  EventStorage::freeMetaDataStrings fmds = pDR->freeMetaDataStrings();

  std::cout << "Found " << fmds.size() << " strings with meta data.\n";

  run_parameters_record rPar;
  rPar.marker = RUN_PARAMETERS_MARKER;
  rPar.record_size = sizeof(run_parameters_record)/sizeof(uint32_t);
  rPar.run_number = pDR->runNumber();
  rPar.max_events = pDR->maxEvents();
  rPar.rec_enable = pDR->recEnable();
  rPar.trigger_type = pDR->triggerType();
  rPar.beam_type = pDR->beamType();
  rPar.beam_energy = pDR->beamEnergy();

  std::string app,tag,dir;

  dir=std::string(out_dir);

  if(out_app!="")
    app=out_app;
  else
    app=pDR->appName();
  
  if(app=="")
    app="unk-app";

  std::string fileName;
  
  if(text){
    fileName=dir +"/";
    fileName=fileName+std::string(out_file)+"_";
    fileName=fileName+app+".txt";
    
    textFile.open(fileName.c_str());
    
    if(!textFile){
      std::cout << "Problem opening file in directory "<<out_dir<<"."<<std::endl;
      return 1;
    }
    
    level='e';
    
  }else{
    pDW = new DataWriter(dir,std::string(out_file),rPar,fmds);
    
    if (!pDW->good()) {
      std::cout << "Problem opening file in directory "<<out_dir<<"."<<std::endl;
      std::cout << "Is the directory there and writable for you?"<<std::endl;
      return -1;
    }
  }
  while (pDR->good() && !pDR->endOfFile() && 
	 ((readnr-offevents)<nevts || nevts==0)) { //keep reading ...
      
    unsigned int eventSize;
    char *buf;
    
    DRError ecode = pDR->getData(eventSize,&buf);
    
    while(DRWAIT == ecode){ // wait for more data to come
      usleep(500000);
      std::cout << "Waiting for more data.\n" << std::flush;
      ecode = pDR->getData(eventSize,&buf);
    }
    
    if(ecode==DRNOOK){
      std::cout << "Not expecting any more data. Exit.\n" <<std::endl;
      break;
    }
    
    readnr++;

    if(readnr>offevents){
	
      std::cout << std::endl;
      std::cout <<  "Event nr: " << readnr << std::endl;
    
      textFile << "\t====\t Event: "<< readnr <<"\t===="<<std::endl;
      
      
      uint32_t * event = (uint32_t *)buf;
      
      bool initial = (level==DEFAULT);
	
      try{
	switch((eformat::HeaderMarker)event[0]) {
	case eformat::FULL_EVENT:{
	  std::cout << "Full Event Fragment" << std::endl;
	    
	  eformat::FullEventFragment<TFRAGPOINTER> * full =
	    new eformat::FullEventFragment<TFRAGPOINTER>(event);
	    
	  if(CHECK(*full) && check_l1list(full))
           {
	    std::cout << "Valid event ... writing " << full->global_id() << " l1 " << full->lvl1_id() << std::endl;
	    full_write(full,event,initial);
           }
	  else
           {
	    std::cout << "Invalid event " << full->global_id() << " l1 " << full->lvl1_id() << ++trashnr << std::endl;
           }
	    
	  delete full;
	  break;
	}
	case eformat::ROB:{
	  std::cout << "ROB Fragment" << std::endl;
	  if(level<'d' && level != '0'){
	    std::cout << "Wrong output level. Asked " 
		      << level << " file is d"<< std::endl;
	    return 1;
	  }
	  eformat::ROBFragment<TFRAGPOINTER> * rob =
	    new eformat::ROBFragment<TFRAGPOINTER>(event);
	  if(CHECK(*rob))
	    rob_write(rob,event,initial);
	  else
	    std::cout << "Invalid event " << ++trashnr << std::endl;
	    
	  delete rob;
	  break;
	}
	case eformat::ROD:
	  std::cout << "ROD Fragment" << std::endl;
	  if(level<'e' && level != '0'){
	    std::cout << "Wrong output level. Asked " 
		      << level << " file is e"<< std::endl;
	    return 1;
	  }
	  pDW->putData(eventSize,event);
	  break;
	default:
	  std::cout << "Unknown fragment. Skipping" << std::endl;
	}
      } catch (eformat::Issue& ex) {
	  
	std::cerr << std::endl
		  << "Uncaught eformat issue: " 
		  << ex.what() << std::endl;
	trashnr++;
	delete[] buf;
	continue;
	
      } catch (ers::Issue& ex) {
	
	std::cerr << std::endl
		  << "Uncaught ERS issue: " 
		  << ex.what() << std::endl;
	trashnr++;
	delete[] buf;
	continue;
	  
      } catch (std::exception& ex) {
	  
	std::cerr << std::endl
		  << "Uncaught std exception: " 
		  << ex.what() << std::endl;
	trashnr++;
	delete[] buf;
	continue;
	  
      } catch (...) {
	  
	std::cerr << std::endl 
		  << "Uncaught unknown exception" 
		  << std::endl;
	trashnr++;
	delete[] buf;
	continue;
      }
	
      textFile << "\t====\t END Event: "<< readnr <<"\t===="<<std::endl;
      textFile << std::endl;
      textFile << std::endl;
    }
      
    delete[] buf;
  }
 
  std::cout << std::endl;
  std::cout << std::endl;
    
  if(!text){
    EventStorage::FileStatus fs = EventStorage::FINISHED;
   
    std::cout << "The file " << pDW->nameFile(fs) 
	      << " was just created."
	      << std::endl;
  }else{
    std::cout << "The file " << fileName 
	      << " was just created."
	      << std::endl;
  }
 
  std::cout << " read " << readnr 
	    << " good " << oknr
	    << " trashed " << trashnr 
	    << " selected "<< writenr 
	    << std::endl;
 
  delete pDR;
 
  if(text)
    textFile.close();
  else {
    pDW->closeFile();
    delete pDW;
  }
}
