#!/bin/bash -f

if [ -d "/opt/exp_soft/atlas/TDAQ/sw/lcg/external/castor" ] ; then
  LCG_EXT_PATH="/opt/exp_soft/atlas/TDAQ/sw/lcg/external"
elif [ -d "/afs/cern.ch/sw/lcg/external/castor" ] ; then
  LCG_EXT_PATH="/afs/cern.ch/sw/lcg/external"
elif [ -d "${LCG_INST_PATH}/external/castor" ] ; then
  LCG_EXT_PATH="${LCG_INST_PATH}/external"
else
  LCG_EXT_PATH="castor_path_not_found"
fi
echo ${LCG_EXT_PATH}
