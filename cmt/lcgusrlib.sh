#!/bin/bash -f

if [ "${CMTCONFIG:0:6}" = "x86_64" ] ; then
  LCG_USR_LIB="usr/lib64"
else
  LCG_USR_LIB="usr/lib"
fi
echo ${LCG_USR_LIB}
