#!/bin/bash -f
if [ $# -ne 1 ]
then
  echo "ERROR: `basename $0` N"
  echo "`basename $0` will (possibly) raise SIGUSR1 at N Hz"
  exit 0
fi
if [ $1 -le 0 ]
then
  echo "ERROR: $1 <= 0"
  echo "usage: `basename $0` N"
  echo "`basename $0` will (possibly) raise SIGUSR1 at X Hz"
  exit 0
fi
echo "`basename $0` (possibly) raising SIGUSR1 at $1 Hz"
let p=(1000000/$1)
let i=0
let k=0
let t0=$p
let z0=(`date +%s`)
while [ 1 ]; do
  let s1=(`date +%s`)-$z0
  let m1=(10#`date +%N` / 1000)
  let t1=($s1*1000000+$m1)
  if [ $t1 -ge $t0 ]
  then
    let i=i+1
    ps -aef | grep ^`whoami` | grep "FileSampler" | grep -v grep | \
    awk '{ print "kill -USR1 " $2 }' | bash -
    if [ $i -eq 100 ]
    then
      let i=0
      let k=$k+100
      let r=($k*1000000 / $t1)
      echo "$k signals ($r Hz average rate)"
    fi
    let t0=($t0+$p)
  fi
done
exit 0
