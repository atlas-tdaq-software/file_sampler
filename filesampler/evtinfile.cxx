
#define _LARGEFILE64_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <unistd.h>
#include <rfio_api.h>

#include <cstring>
#include <string>
#include <iostream>
#include <sstream>

#define U32LEN (sizeof(uint32_t))
#define U16LEN (sizeof(uint16_t))

off64_t file_pos, file_size, nread;
int32_t file_des;
struct stat file_info;
uint32_t n_evt = 0, n_good = 0, n_bad = 0;

#define INFO(A) do { std::cerr << A << std::endl; } while(0)

#define yMSGOUT(A) INFO(A)
#define nMSGOUT(A) {}

#define MSGOUT(A) nMSGOUT(A)

off64_t rread( void * ptr, int32_t size)
 {
  if ((size == U16LEN) && (ptr != NULL))
   {
    char * cbuf = (char *)ptr;
    int32_t ret = rfio_read( file_des, cbuf, U16LEN);
    if (ret == U16LEN) file_pos += ret;
    return ret;
   }
  else if ((size == U32LEN) && (ptr != NULL))
   {
    char * cbuf = (char *)ptr;
    int32_t ret = rfio_read( file_des, cbuf, U32LEN);
    if (ret == U32LEN) file_pos += ret;
    return ret;
   }
  else
   {
    off64_t at = file_pos;
    off64_t nr = rfio_lseek64 ( file_des, size, SEEK_CUR);
    if (nr == -1) return -1;
    file_pos = nr;
    return file_pos-at;
   }
 }

void filein( char * filename)
 {
  if (! filename) throw 1;
  file_pos = 0;
  file_des = (strcmp(filename, "/dev/stdin") == 0 || strcmp(filename, "-") == 0) ? 0 :
             rfio_open64 ( filename, O_RDONLY);
  if (file_des == -1) throw 4;
  rfio_fstat( file_des, &file_info);
  file_size = file_info.st_size;
  if ((file_des > 0) && (file_size < off64_t(U32LEN)))
   {
    rfio_close(file_des);
    file_des = 0;
    throw 5;
   }
  MSGOUT(filename << " size " << file_size);
 }

int32_t readone()
 {
  off64_t ng;
  uint32_t hdr, csz;
  int32_t datasize;
  uint16_t hlo, hhi;
  ng = rread( &hlo, U16LEN);
  if (ng != off64_t(U16LEN)) return 1;
  do
   {
    ng = rread( &hhi, U16LEN);
    if (ng != off64_t(U16LEN)) return 1;
    hdr = (hhi << 16) | hlo;
    hlo = hhi;
   }
  while (hdr != 0xabba0011);
  ++ n_evt;
  off64_t file_last = file_pos; 
  ng = rread( &csz, U32LEN);
  if (ng != off64_t(U32LEN)) return 2;
  datasize = csz & 0xffff;
  datasize -= 3*U32LEN;
  off64_t ret = rread( NULL, datasize);
  if (ret != off64_t(datasize))
   {
    INFO("readone ERROR ret " << ret);
    throw 8;
   }
  uint32_t trl;
  ng = rread( &trl, U32LEN);
  if (ng != off64_t(U32LEN)) return 1;
  if (trl != 0x0011abba)
   {
    n_bad ++;
    off64_t nr = rfio_lseek64 ( file_des, file_last, SEEK_SET);
    if (nr == -1) return -1;
    file_pos = file_last;
   }
  datasize += 3*U32LEN;
  MSGOUT(std::hex << " " << hdr << " " << trl << " 2 " << csz << " S " << datasize << std::dec << " 2 " << csz << " S " << datasize);
  return 0;
 }
 
int main( int argc, char** argv)
{
  if (argc < 2) return 1;
  filein( argv[1]);
  uint32_t evt_max = 0;
  if (argc == 3)
   {
    std::stringstream ss;
    ss << argv[2];
    ss >> evt_max;
   }
  int32_t ret;
  do
   {
    ret = readone();
    MSGOUT(std::oct << file_pos << " " << std::hex << file_pos << " " << std::dec << file_pos << "/" << file_size << " # frags " << n_evt << " # good " << n_good << " # bad " << n_bad);
   }
  while ((ret == 0) && (n_evt != evt_max) && (file_pos < file_size));
  if (file_des) rfio_close(file_des);
  n_good = n_evt - n_bad;
  INFO("bytes read " << file_pos << " / file size " << file_size << " # frags " << n_evt << " # good " << n_good << " # bad " << n_bad);
  return 0;
}

