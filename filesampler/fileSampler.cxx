
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <ctime>
#include <sched.h>
#include <unistd.h>
#include <string>
#include <pthread.h>

#include "ers/ers.h"
#include "cmdl/cmdargs.h"
#include "emon/EventSampler.h"
#include "ipc/core.h"

#include "fileReader.h"

#define RODHMRKR 0xee1234ee

#define ERR_MESS0(A) { }
#define ERR_MESS1(A) { std::cout << A << std::endl; }

volatile static uint32_t n_chan_ = 0;
volatile static uint32_t sampled_ = 0;
volatile static uint32_t skipped_ = 0;
volatile static uint32_t how_many_ = 0;
volatile static uint32_t n_sig_ = 0;
volatile static bool free_run_ = true;
volatile static bool no_more_ = false;
volatile static bool is_open_ = false;

static void (* sginth)(int32_t) = NULL;
static void (* sgquith)(int32_t) = NULL;
static void (* sgtermh)(int32_t) = NULL;
static void (* sgalrmh)(int32_t) = NULL;

pthread_mutex_t varmtx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rdrmtx = PTHREAD_MUTEX_INITIALIZER;

class MyPullSampling : public emon::PullSampling
{
 private:
  uint32_t * event_;
  uint32_t size_;
  emon::SelectionCriteria * csel_;
  fileReader * freader_;

 public:	

  MyPullSampling( const emon::SelectionCriteria &sc, fileReader * flRdr )
   : freader_(flRdr)
   {
    ERR_MESS0 ("MyPullSampling 1");
    pthread_mutex_lock(&varmtx);
    n_chan_ ++;
    pthread_mutex_unlock(&varmtx);
    event_ = NULL;
    size_ = 0;
    csel_ = new emon::SelectionCriteria ( sc);
    ERR_MESS0 ("MyPullSampling n_chan_ " << n_chan_ << " free_run_ "
               << free_run_ << " n_sig_ " << n_sig_
               << " sampled_ " << sampled_ << " skipped_ " << skipped_);
   }

  ~MyPullSampling( )
   {
    ERR_MESS0 ("~MyPullSampling 2");
    delete csel_;
    csel_ = 0;
    pthread_mutex_lock(&varmtx);
    -- n_chan_;
    pthread_mutex_unlock(&varmtx);
    ERR_MESS0 ("~MyPullSampling n_chan_ " << n_chan_ << " free_run_ "
               << free_run_ << " n_sig_ " << n_sig_
               << " sampled_ " << sampled_ << " skipped_ " << skipped_);
   }

  void sampleEvent( emon::EventChannel & cc )
   {
    if ( how_many_ )
     {
      if ( ! free_run_ )
       {
        pthread_mutex_lock(&varmtx);
        how_many_ --;
        pthread_mutex_unlock(&varmtx);
       }
      pthread_mutex_lock(&rdrmtx);
      uint32_t nw = freader_->event_reader( &event_, size_ );
      pthread_mutex_unlock(&rdrmtx);
      if ( nw )
       {
        pthread_mutex_lock(&varmtx);
	sampled_ ++;
        pthread_mutex_unlock(&varmtx);
	cc.pushEvent( (unsigned int *)event_, size_ );
        ERR_MESS0 (size_ << std::hex << " " << event_[0]
          << " " << event_[1] << " " << event_[2]
          << " " << event_[3] << " " << event_[4]
          << " " << event_[5] << " " << event_[6]
          << " " << event_[7] << " " << event_[8]
          << " " << event_[9] << std::dec);
        ERR_MESS0 (size_ << std::hex << " " << event_[size_-10]
          << " " << event_[size_-9] << " " << event_[size_-8]
          << " " << event_[size_-7] << " " << event_[size_-6]
          << " " << event_[size_-5] << " " << event_[size_-4]
          << " " << event_[size_-3] << " " << event_[size_-2]
          << " " << event_[size_-1] << std::dec);
       }
      else
       {
	ERR_MESS0 (std::endl << "No more events available" << std::endl);
	pthread_mutex_lock(&varmtx);
	free_run_ = true;
	how_many_ = 0;
        no_more_ = true;
        pthread_mutex_unlock(&varmtx);
        alarm(1);
       }
     } else {
      pthread_mutex_lock(&varmtx);
      skipped_ ++;
      pthread_mutex_unlock(&varmtx);
      if ( ! free_run_ ) sched_yield();
     }
   }
};


class MyPullSamplingFactory : public emon::PullSamplingFactory
{
 private:
  fileReader * flRdr;

 public:
  MyPullSamplingFactory( const char * event_file, const uint32_t & event_type,
                         const uint32_t & ev_max, const bool & wrap_around)
   {
    ERR_MESS0 ("MyPullSamplingFactory");
    const uint32_t rlevel = (event_type >> 24) & 0x7;
    const uint32_t source_id = event_type & 0xffffff;
    uint32_t retcode;
    pthread_mutex_lock(&varmtx);
    how_many_ = 0;
    pthread_mutex_unlock(&varmtx);
    flRdr = new fileReader( event_file, source_id, rlevel, ev_max, wrap_around,
                            retcode );
    if (retcode)
     {
      std::string errMess =
         "MyPullSamplingFactory::MyPullSamplingFactory: " +
         std::string(event_file) + " file reader error " +
         std::string( 1, retcode+'0');
      throw errMess;
     }
   }
  ~MyPullSamplingFactory()
   {
    ERR_MESS0 ("~Factory 2");
    delete flRdr;
    pthread_mutex_lock(&varmtx);
    how_many_ = 0;
    pthread_mutex_unlock(&varmtx);
    time_t t0 = time(NULL)+5;
    while ((n_chan_) && (time(NULL)<t0)) sched_yield();
   }
  emon::PullSampling * startSampling( const emon::SelectionCriteria & criteria )
        throw( emon::BadCriteria, emon::NoResources )
   {
    ERR_MESS0 ("startSampling 3");
    return new MyPullSampling( criteria, flRdr );
   }
  void go()
   {
    ERR_MESS0 ("go 4 " << how_many_ << " sampled_ " << sampled_ << " skipped_ " << skipped_);
    pthread_mutex_lock(&varmtx);
    sampled_ = skipped_ = 0; if (free_run_) how_many_ = 1;
    pthread_mutex_unlock(&varmtx);
   }
  void halt()
   {
    ERR_MESS0 ("halt 5 " << how_many_ << " sampled_ " << sampled_ << " skipped_ " << skipped_);
    time_t t0 = time(NULL)+5;
    while ((n_chan_) && (time(NULL)<t0)) sched_yield();
   }
};

typedef struct {
  std::string partition;
  // std::string key;
  // std::string value;
  std::string type;
  std::string name;
  int32_t etype;
  std::string dfile;
  uint32_t numevt;
  bool wraparound;
  bool rfree;
} cmdargx;

class MonaIsaSampler {

private:
  bool   m_running;

  emon::EventSampler * eSampler;
  MyPullSamplingFactory * factory;

  void destroy()
   {
    pthread_mutex_lock(&varmtx);
    free_run_ = true;
    how_many_ = 0;
    pthread_mutex_unlock(&varmtx);
    if (m_running)
     {
      eSampler->stop();
      factory->halt();
      m_running = false;
      eSampler = 0;
     }
    if (eSampler) delete eSampler;
    eSampler = 0;
   }

public:
  MonaIsaSampler( const cmdargx & x )
  {
   ERR_MESS0 ("Do MonaIsaSampler()");
   // emon::SamplingAddress address;
   // address.addComponent( x.key, x.value );
   emon::SamplingAddress address ( x.type.c_str(), x.name.c_str() );
   uint32_t max_channels = 10;
   try
    {
     IPCPartition partition(x.partition);
     factory = new MyPullSamplingFactory( x.dfile.c_str(), x.etype, x.numevt, x.wraparound);
     eSampler = new emon::EventSampler ( x.partition, address, factory, max_channels );
    }
   catch ( emon::Exception & ex ) { ers::fatal(ex); }
   catch ( daq::ipc::InvalidPartition & ex) { ers::fatal(ex); }
  }

  ~MonaIsaSampler()
  {
    ERR_MESS0 ("Do ~MonaIsaSampler()");
    destroy();
  }

  void stop()
  {
    ERR_MESS0 ("Do stop()");
    destroy();
  }

  void execute()
  {
    ERR_MESS0 ("Do execute()");
    m_running = true;
    factory->go();
    eSampler->wait();
  }

};

static MonaIsaSampler * sampler = NULL;

cmdargx parse_cmx(const std::string& text, int32_t argc, char* argv[]) {
  CmdArgStr     partition('p', "partition", "partition", "partition name",
       getenv("TDAQ_PARTITION") ? CmdArg::isOPT : CmdArg::isREQ);
 
  // CmdArgStr     key('k', "key", "key", "sampler address key");
  // CmdArgStr     value('v', "value", "value", "sampler address value");
  CmdArgStr     type ('t', "type", "type", "address type");
  CmdArgStr     name ('n', "name", "name","sampler name");
  CmdArgInt     evType ('c', "criteria", "sel-criteria", "event selection criteria (level<<24 | source_id)");
  CmdArgStr     evFile ('f', "file", "data-file",
                "'filename' for a data file, 'list:filename' for a file list",
                CmdArg::isREQ);
  CmdArgInt     mxEvt ('N', "n_events", "n_events", "# events to sample (0=all)");
  CmdArgInt     wrAround ('w', "wrap-around", "wrap-around", "cycle over (0=false)");
  
  CmdArgInt     rMode('r', "run-mode", "run-mode", "1(default) = free run, 0 = SIGUSR1");
 
  partition = getenv("TDAQ_PARTITION");
  // key = "mona";
  // value = "isa";
  type = "mona";
  name = "isa";
  evType = 0;
  mxEvt = 0;
  wrAround = 0;
  rMode = 1;

  // CmdLine       cmd(*argv, &partition, &key, &value, &evType, &evFile, &mxEvt, &wrAround, &rMode, NULL);
  CmdLine       cmd(*argv, &partition, &type, &name, &evType, &evFile, &mxEvt, &wrAround, &rMode, NULL);
  CmdArgvIter	argv_iter(--argc, (const char * const *) ++argv);

  cmd.description(text.c_str());

  uint32_t status = cmd.parse(argv_iter);
  if (status) {
    cmd.error() << argv[0] << ": parsing errors occurred!" << std::endl ;
    exit(EXIT_FAILURE);
  }

  cmdargx c;

  setenv( "TDAQ_PARTITION", partition, 1);

  c.partition = partition;
  // c.key = key;
  // c.value = value;
  c.type = type;
  c.name = name;
  c.etype = evType;
  c.dfile = evFile;
  c.numevt = mxEvt;
  c.wraparound = (wrAround != 0);
  c.rfree = (rMode != 0);

  return c;
}

void closeup()
 {
  std::stringstream sst;
  char * b;
  sst << " CLOSING UP: n_sig_ " << n_sig_ << " how_many_ " << how_many_ << " sampled_ " << sampled_ << " skipped_ " << skipped_ << std::endl;
  b = const_cast<char *>(sst.str().c_str());
  write( 2, b, strlen(b));
  if ( sampler )
   {
    sampler->stop();
    delete sampler;
    sampler = 0;
   }
  sst.str("");
  sst << "   *** DONE ***" << std::endl;
  b = const_cast<char *>(sst.str().c_str());
  write( 2, b, strlen(b));
 }

void now( char * tx)
 {
  time_t tt;
  struct tm st;
  tt = time(NULL);
  localtime_r( &tt, &st);
  st.tm_mon += 1;
  st.tm_year += 1900;
  snprintf( tx, 24, "%.2d/%.2d/%4d %.2d:%.2d:%.2d", st.tm_mday, st.tm_mon,            st.tm_year, st.tm_hour, st.tm_min, st.tm_sec);
 }

static time_t tn = time(NULL)+TIMEOUT;

void signal_handler ( int32_t sig )
 {
  if ( sig == SIGUSR1 )
   {
    signal(SIGUSR1,signal_handler);
    pthread_mutex_lock(&varmtx);
    if ( ( ! free_run_ ) && ( n_chan_ > 0 ) ) how_many_ ++;
    n_sig_ ++;
    pthread_mutex_unlock(&varmtx);
    return;
   }
  if ( sig == SIGALRM )
   {
    if ( ( ! is_open_ ) || ( time(NULL) >= tn ) )
     {
      std::stringstream sst;
      char * b;
      sst << " TIMEOUT : file has not been opened" << std::endl;
      b = const_cast<char *>(sst.str().c_str());
      write( 2, b, strlen(b));
      exit(0);
     }
    else if ( ! no_more_ ) 
     {
      tn = time(NULL)+TIMEOUT;
      std::stringstream sst;
      char tx[24];
      now(tx);
      sst << " ALARM: " << tx
          << " n_sig_ " << n_sig_ << " how_many_ " << how_many_ << " sampled_ "
          << sampled_ << " skipped_ " << skipped_ << std::endl;
      char * b = const_cast<char *>(sst.str().c_str());
      write( 2, b, strlen(b));
      signal(SIGALRM,signal_handler);
      alarm(NOTIFYPERIOD);
      return;
     }
   }
  if ((sig == SIGINT) && (sginth)) (*sginth)(sig);
  if ((sig == SIGQUIT) && (sgquith)) (*sgquith)(sig);
  if ((sig == SIGTERM) && (sgtermh)) (*sgtermh)(sig);
  exit(0);
 }

int
main( int32_t argc, char** argv )
{
  IPCCore::init ( argc, argv );

  std::stringstream strstr;
  strstr << std::endl << "Running with " << argc << " args:" << std::endl << argv[0];
  for (int32_t n=1; n < argc; n++) strstr << " " << argv[n];
  strstr << std::endl;
  ERR_MESS1 (strstr.str());

  cmdargx x = parse_cmx("File Sampler Application", argc, argv);
  free_run_ = x.rfree;

  atexit(closeup);

  sginth = signal(SIGINT,signal_handler);
  sgquith = signal(SIGQUIT,signal_handler);
  sgtermh = signal(SIGTERM,signal_handler);
  sgalrmh = signal(SIGALRM,signal_handler);
  if ( ! free_run_ ) signal(SIGUSR1,signal_handler);

  alarm(TIMEOUT);

  try
    {
     sampler = new MonaIsaSampler(x);
     is_open_ = true;
    }
  catch (std::string & a)
    {
     ERR_MESS1 (a);
     exit(0);
    }

  alarm(NOTIFYPERIOD);

  try
    {
        ERR_MESS1 ("running ...");
	sampler->execute();
    }
  catch (ers::Issue &e)
    {
	ERR_MESS1 ("Unknown issue caught: " << e );
	ers::error(e);
    }

  return 0;
}

