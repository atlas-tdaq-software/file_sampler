
#define _LARGEFILE64_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <unistd.h>
#include <rfio_api.h>

#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

#define ROBMARKER 0xdd1234dd
#define HDRLEN 8
#define RODMARKER 0xee1234ee
#define U32LEN (sizeof(uint32_t))
#define U16LEN (sizeof(uint16_t))
#define HDRSIZE (U32LEN*HDRLEN)

uint32_t robh[HDRLEN] = { ROBMARKER, 0, HDRLEN-1, 0xbeccabac, 0, 0, 0, RODMARKER };

#define myllong int64_t
myllong file_pos, file_size, to_be_read, nread, nwritten, bufsize;
myllong wherefound;
int32_t file_des, file_out;
struct stat file_info;
uint32_t nfrag = 0;
uint32_t n_evt = 0, n_good = 0, n_bad = 0;

#define INFO(A) do { std::cerr << A << std::endl; } while(0)

#define yMSGOUT(A) INFO(A)
#define nMSGOUT(A) {}

#define MSGOUT(A) nMSGOUT(A)

myllong rread( void * ptr, myllong size)
 {
  if (ptr == NULL)
   {
    myllong from = file_pos;
    myllong to = file_pos+size;
    myllong nr = rfio_lseek64 ( file_des, to, SEEK_SET);
    if (nr != to) return -1;
    file_pos = to;
    to_be_read = file_size - file_pos;
    return file_pos-from;
   }
  else if (size == U16LEN)
   {
    char * cbuf = (char *)ptr;
    myllong ret = rfio_read( file_des, cbuf, U16LEN);
    if (ret == U16LEN) file_pos += ret;
    to_be_read = file_size - file_pos;
    return ret;
   }
  else if (size == U32LEN)
   {
    char * cbuf = (char *)ptr;
    myllong ret = rfio_read( file_des, cbuf, U32LEN);
    if (ret == U32LEN) file_pos += ret;
    to_be_read = file_size - file_pos;
    return ret;
   }
  else
   {
    char * cbuf = (char *)ptr;
    myllong ret = rfio_read( file_des, cbuf, int(size));
    myllong tot = ret;
    while ((ret != 0) && (ret != -1) && (tot != size))
     {
      ret = rfio_read( file_des, cbuf+tot, int(size-tot));
      tot += ret;
     }
    file_pos += tot;
    to_be_read = file_size - file_pos;
    return tot;
   }
 }

void filein( char * filename)
 {
  if (! filename) throw 1;
  file_pos = 0;
  file_des = (strcmp(filename, "/dev/stdin") == 0 || strcmp(filename, "-") == 0) ? 0 :
             rfio_open64 ( filename, O_RDONLY);
  if (file_des == -1) throw 4;
  rfio_fstat( file_des, &file_info);
  file_size = file_info.st_size & 0xffffffff;
  if ((file_des > 0) && (file_size < myllong(U32LEN)))
   {
    rfio_close(file_des);
    file_des = 0;
    throw 5;
   }
  to_be_read = file_size;
  MSGOUT(filename << " size " << file_size);
 }

myllong next_pos()
 {
  myllong ng;
  uint32_t hdr, det_mask, csz;
  int32_t datasize;
  uint16_t hlo, hhi;
  ng = rread( &hhi, U16LEN);
  if (ng != U16LEN) return -1;
  do
   {
    hlo = hhi;
    ng = rread( &hhi, U16LEN);
    if (ng != U16LEN) return -2;
    hdr = (hhi << 16) | hlo;
    det_mask = hlo & 0x1111;
   }
  while ((hhi != 0xabba) || (hlo & 0xeeee) || (det_mask == 0));
  wherefound = file_pos-U32LEN;
  ng = rread( &csz, U32LEN);
  if (ng != U32LEN) return -3;
  datasize = csz & 0xffff;
  datasize -= 3*U32LEN;
  ng = rread( NULL, datasize);
  if (ng != datasize)
   {
    INFO("next_pos ERROR ret " << ng);
    return -4;
   }
  uint32_t trl;
  ng = rread( &trl, U32LEN);
  if (ng != U32LEN) return -5;
  datasize += 3*U32LEN;
  uint32_t hdrswp = (det_mask << 16) | 0xabba;
  if (trl != hdrswp)
   {
    n_bad ++;
    ng = rread( NULL, wherefound+U32LEN-file_pos);
    if (ng == -1) return -6;
    return next_pos();
   }
  else
   {
    ng = rread( NULL, wherefound-file_pos);
    return file_pos;
   }
 }

void readone( void* & buffer, int32_t & size)
 {
  myllong ng;
  uint32_t hdr, csz;
  int32_t datasize;
  if (next_pos() < 0) return;
  ng = rread( &hdr, U32LEN);
  if (ng != U32LEN) return;
  ng = rread( &csz, U32LEN);
  datasize = csz & 0xffff;
  if (datasize <= int32_t(2*U32LEN)) INFO(" *** csz " << csz << " datasize " << datasize);
  size = datasize + HDRSIZE;
  int32_t align = size % U32LEN;
  if (align) size += U32LEN-align;
  while (bufsize < size)
   {
    bufsize += 1024;
    buffer = realloc( buffer, bufsize);
   }
  uint32_t * ubuf = (uint32_t *)buffer;
  ubuf[1] = size/U32LEN;
  ubuf += HDRSIZE/U32LEN;
  *ubuf ++ = hdr;
  *ubuf ++ = csz;
  datasize -= 2*U32LEN;
  char * cbuf = (char *)ubuf;
  myllong ret = rread( cbuf, datasize);
  if (ret != myllong(datasize))
   {
    INFO("readone ERROR ret " << ret << " size " << datasize);
    throw 8;
   }
  for (int32_t j=0; j<align; j++) cbuf[datasize+j] = 0;
  nread = file_pos;
 }
 
void fileout( char * filename)
 {
  if (! filename) throw 1;
  file_out = ((strcmp(filename, "/dev/stdout") == 0) ||
              (strcmp(filename, "-") == 0)) ? 1 :
                 rfio_open64 ( filename, O_WRONLY | O_CREAT, 0644);
  if (file_out == -1) throw 4;
 }

void storeone( void * buffer, int32_t & size)
 {
  if (size == 0) return;
  int32_t ret = rfio_write( file_out, buffer, size);
  if (ret != size)
   {
    uint32_t * ptr = (uint32_t *)buffer;
    INFO("storeone ERROR ret " << ret << " size " << size
         << std::hex << ptr[0] << " " << ptr[1] << " " << ptr[2] << " ");
    throw 10;
   }
  nwritten += size;
 }

int main( int argc, char** argv)
{
  void * buffer = NULL;
  bufsize = 1024;
  buffer = malloc(bufsize);
  memcpy( buffer, (const void *)robh, HDRSIZE);
  int32_t size;
  if (argc < 3) return 1;
  filein( argv[1]);
  fileout( argv[2]);
  nread = nwritten = 0;
  uint32_t evt_max = 0;
  if (argc == 4)
   {
    std::stringstream ss;
    ss << argv[3];
    ss >> evt_max;
   }
  do
   {
    size = 0;
    readone( buffer, size);
    storeone( buffer, size);
    MSGOUT(wherefound << " now " << file_pos << "/" << file_size << " frag # " << ++nfrag
           << " in " << size << " out " << size << " - tot in " << nread
           << " out " << nwritten);
   }
  while ((size > 0) && (++ n_good != evt_max) && (file_pos < file_size));
  if (file_des) rfio_close(file_des);
  if (file_out > 1) rfio_close(file_out);
  free(buffer);
  n_evt = n_good + n_bad;
  INFO("bytes read " << file_pos << " / file size " << file_size << " # frags " << n_evt << " # good " << n_good << " # bad " << n_bad);
  return 0;
}

