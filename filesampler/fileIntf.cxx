
#define _LARGEFILE64_SOURCE
#include "fileIntf.h"
#include <rfio_api.h>

#include <cstdlib>
#include <cstring>
#include <iostream>

#define ROBMARKER 0xdd1234dd
#define RBHLENGTH 10
#define RODMARKER 0xee1234ee
#define DATALEN (sizeof(uint32_t))
#define ROBHSIZE (DATALEN*RBHLENGTH)

uint32_t rodh[10] = { ROBMARKER, 0, RBHLENGTH, 0, 0, 3, 0, 0, 0, 0};

#define ERR_MESS0(A) { }
#define ERR_MESS1(A) { std::cout << A << std::endl; }
#define ERR_MESSE(A) { std::cerr << A << std::endl; }

int32_t rread(  int32_t file_des, void * ptr, int32_t size)
 {
  char * cbuf = (char *)ptr;
  int32_t ret = rfio_read( file_des, cbuf, size);
  int32_t tot = ret;
  while ((ret != 0) && (ret != -1) && (tot != size))
   {
    ret = rfio_read( file_des, cbuf+tot, size-tot);
    tot += ret;
   }
  return tot;
 }

fileIntf::fileIntf( const char * filename)
 {
  if (! filename) throw 1;
  file_name = std::string(filename);
  evt_pos = file_pos = 0;
  file_des = (strcmp(filename, "/dev/stdin") == 0 || strcmp(filename, "-") == 0) ? 0 :
             rfio_open64 ( const_cast<char *>(filename), O_RDONLY);
  if (file_des == -1) throw 4;
  rfio_fstat( file_des, &file_info);
  file_size = file_info.st_size;
  if ((file_des > 0) && (file_size < DATALEN))
   {
    rfio_close(file_des);
    file_des = 0;
    throw 5;
   }

  getrods = false;
  rodbuffer = NULL;
  rodbufsize = 0;
  rodevsize = 0;
  rodnwread = 0;
  uint32_t i;
  bool found;
  size_t ng, pos;
  uint32_t data;
  uint32_t allmrkrs[6] = { LV1MARKER, EVTMARKER, DETMARKER,
                           ROSMARKER, ROBMARKER, RODMARKER };
  pos = 0;
  do
   {
    pos += (ng = rread(file_des, &data, DATALEN));
    for (i=0; i<6; i++) if ((found = (data == allmrkrs[i]))) break;
   }
  while ((ng == DATALEN) && !found);
  file_level = (found) ? i+LV1LEVEL : 0;
  if (found) file_marker = data;
  if (data == RODMARKER)
   {
    file_level = ROBLEVEL;
    // rob_hsize = RBHLENGTH;
    getrods = true;
    rodbufsize = 1024;
    rodbuffer = (uint32_t *)malloc(rodbufsize);
    memcpy( rodbuffer, (const void *)rodh, ROBHSIZE);
   }
  else
   {
/*
    if ((file_level > 0) && (file_level < ROBLEVEL)) do
     {
      ng = rread(file_des, &data, DATALEN);
     }
    while ((ng == DATALEN) || (data == ROBMARKER));
    if (data == ROBMARKER)
     {
      ng = rread(file_des, &data, DATALEN);
      ng = rread(file_des, &data, DATALEN);
      rob_hsize = data;
     }
*/
   }
//  rfio_lseek( file_des, 0, SEEK_SET);

  file_pos = pos;
  evt_pos = 0;
 }

fileIntf::~fileIntf()
 {
  if (file_des > 0) rfio_close(file_des);
  getrods = false;
  if (rodbuffer) free(rodbuffer);
  rodbuffer = NULL;
  rodbufsize = 0;
  rodevsize = 0;
  rodnwread = 0;
  file_des = 0;
  evt_pos = file_pos = 0;
  file_level = 0;
  file_name.clear();
 }

void fileIntf::readone( uint32_t & bufsize, uint32_t* & buffer, uint32_t & size)
 {
  size_t ng;
  uint32_t data;
  uint32_t * ubuf = buffer;
  size = 0;
  file_pos = rodnwread;
  ng = rread(file_des, &data, DATALEN);
  if (ng != DATALEN)
   {
    ubuf[1] = RBHLENGTH;
    ubuf[3] = 0;
    ubuf[4] = 0;
    return;
   }
  size += ng;
  ubuf += RBHLENGTH;
  *ubuf ++ = data;
  do
   {
    size += (ng = rread(file_des, &data, DATALEN));
    if (bufsize < size+ROBHSIZE)
     {
      bufsize += 1024;
      buffer = (uint32_t *)realloc( buffer, bufsize);
     }
    *ubuf ++ = data;
   }
  while ((ng == DATALEN) && (data != RODMARKER));
  if (ng == DATALEN) size -= ng;
  ubuf = buffer;
  ubuf[1] = RBHLENGTH+size/DATALEN;
  ubuf[3] = ubuf[RBHLENGTH+2];
  ubuf[4] = ubuf[RBHLENGTH+3];
  if (ng != DATALEN)
   {
    ERR_MESSE("readone ERROR ng " << ng << " size " << size)
    throw 8;
   }
  rodnwread += size;
 }

uint32_t fileIntf::next( const uint32_t mk, uint32_t & val)
 {
  uint32_t w, ng;
  if (getrods)
   {
    readone( rodbufsize, rodbuffer, rodevsize);
    val = (rodevsize) ? rodbuffer[0] : 0;
    return file_pos;
   }
  else
   {
    if ((evt_pos == 0) && (mk == file_marker))
     {
      val = mk;
      evt_pos = file_pos;
      evt_hdr[0] = val;
      for (uint32_t i=1; i<5; i++)
       {
        file_pos += (ng = rread( file_des, &w, DATALEN));
        evt_hdr[i] = (ng == DATALEN) ? w : 0;
       }
      return evt_pos;
     }
    do
     {
      file_pos += (ng = rread( file_des, &w, DATALEN));
     }
    while ((ng == DATALEN) && (w != mk));
    evt_pos = file_pos;
    val = (ng == DATALEN) ? w : 0;
    evt_hdr[0] = val;
    for (uint32_t i=1; i<5; i++)
     {
      file_pos += (ng = rread( file_des, &w, DATALEN));
      evt_hdr[i] = (ng == DATALEN) ? w : 0;
     }
    return (ng == DATALEN) ? evt_pos : 0;
   }
 }

uint32_t fileIntf::sseek( const uint32_t pos)
 {
  if (file_des)
   {
    int32_t ret = rfio_lseek( file_des, pos, SEEK_SET);
    file_pos = (ret != -1) ? pos : 0xffffffff;
   }
  else
   {
    uint32_t w, ng;
    ng = DATALEN;
    while ((file_pos < pos) && (ng == DATALEN))
     {
      file_pos += (ng = rread( file_des, &w, DATALEN));
     }
    if (ng != DATALEN) file_pos = 0xffffffff;
   }
  return file_pos;
 }

int32_t fileIntf::readsize( uint32_t & w)
 {
  if (getrods)
   {
    w = rodbuffer[1];
    return DATALEN;
   }
  else
   {
    w = evt_hdr[1];
    return (w) ? DATALEN : 0;
   }
 }

int32_t fileIntf::readsid( uint32_t & w)
 {
  if (getrods)
   {
    w = rodbuffer[4];
    return DATALEN;
   }
  else
   {
    w = evt_hdr[4];
    return DATALEN;
   }
 }

int32_t fileIntf::readb( const void * buff, const uint32_t nby)
 {
  if (getrods)
   {
    uint32_t * dest = (uint32_t *)buff;
    uint32_t * srce = (uint32_t *)rodbuffer;
    uint32_t nwords = nby/DATALEN;
    if (nwords == rodbuffer[1]) while ( nwords --) *dest ++ = *srce ++;
    return (nwords == rodbuffer[1]) ? nwords*DATALEN : 0;
   }
  else
   {
    uint32_t * dest = (uint32_t *)buff;
    for (uint32_t i=0; i<5; i++) *dest ++ = evt_hdr[i];
    uint32_t nget = nby-5*DATALEN;
    int32_t ret = rread( file_des, dest, nget);
    file_pos = (ret == int32_t(nget)) ? file_pos + nget : 0xffffffff;
    return ret+5*DATALEN;
   }
 }

