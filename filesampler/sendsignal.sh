#!/bin/bash -f
  me=$( echo `whoami` )
  pid=$( ps -aef | grep ^$me | grep "fileSampler" | grep -v grep | awk '{print $2}' )
  if [ "$pid" = "" ]
  then
    echo "fileSampler not found for user $me"
    exit 1;
  fi
  echo "./sendsignal $pid $1"
  ./sendsignal $pid $1
exit 0
