
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctime>
#include <cstdlib>
#include <csignal>
#include <iostream>
#include <string>
#include <sstream>

int32_t on_req( int32_t pid)
 {
  std::stringstream mess;
  const char * b;
  int32_t i=0;
  do
   {
    mess.str("");
    mess << "Press Return for event # " << ++ i << " ...";
    b = mess.str().c_str();
    write (2, b, strlen(b));
    getchar();
    if (kill( pid, SIGUSR1) != 0) exit(0);
   }
  while(1);
 }

int32_t
main( int32_t argc, char** argv )
 {
  std::stringstream mess;
  const char * b;
  mess << "usage: " << argv[0] << " pid [rate(Hertz)]" << std::endl;
  b = mess.str().c_str();
  write (2, b, strlen(b));
  if ((argc < 2) || (argc > 3)) exit(0);
  int32_t pid;
  int32_t r = sscanf(argv[1], "%d", &pid);
  if (r != 1) exit(0);
  if (pid <= 0) exit(0);
  if (argc == 2) return on_req( pid);
  int32_t rate;
  r = sscanf(argv[2], "%d", &rate);
  if (r != 1) exit(0);
  if (rate <= 0) exit(0);
  double dt=1.0/rate;
  struct timeval tv; 
  gettimeofday( &tv, NULL);
  double s0=tv.tv_sec;
  double t0=(tv.tv_usec/1000000.0)+dt;
  do
   {
    gettimeofday( &tv, NULL);
    double t1=(tv.tv_sec-s0)+(tv.tv_usec/1000000.0);
    if (t1 >= t0)
     {
      if (kill( pid, SIGUSR1) != 0) exit(0);
      t0+=dt;
     }
   }
  while (1);
  return 0;
 }
