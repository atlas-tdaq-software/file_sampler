
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "eformat/eformat.h"

#include "fileReader.h"

#define ERR_MESS0(A) { }
#define ERR_MESS1(A) { \
  char tx[24]; \
  tm2str(tx); \
  std::cout << tx << " " << A << std::endl; \
 }

#define REALLOC(P,S) if ((P==NULL) || (S > buffersize)) \
	{ P = bufferaddr = (uint32_t *)realloc(P,S); buffersize = S; }

#define ROBFRAGMENTT  eformat::ROBFragment<uint32_t *>
#define EVTFRAGMENTT  eformat::FullEventFragment<uint32_t *>

#define CHECK(F) (F).check()
// #define CHECK(F) (F).check_tree()

#define xVERIFY(FRAGTYPE,_P_,_S_,_F_) do { \
  FRAGTYPE * f = new FRAGTYPE (_P_); \
  try { \
    _F_ = CHECK(*f) && (f->fragment_size_word() == (_S_)); \
    if (! _F_) ERR_MESS1 ("Found invalid event"); \
   } catch (eformat::Issue& ex) { \
	ERR_MESS1 (std::endl << "Uncaught eformat issue: " << ex.what()); \
   } catch (ers::Issue& ex) { \
	ERR_MESS1 (std::endl << "Uncaught ERS issue: " << ex.what()); \
   } catch (std::exception& ex) { \
	ERR_MESS1 (std::endl << "Uncaught std exception: " << ex.what()); \
   } catch (...) { \
	ERR_MESS1 (std::endl << "Uncaught unknown exception: "); \
   } \
  delete f; \
} while(0);

#define VERIFY(FRAGTYPE,_P_,_S_,_F_) do { _F_=true; if (0) ERR_MESS1(_P_<<" "<<_S_<<" "<<_F_); } while(0)

void tm2str( char * tx)
 {
  time_t tt;
  struct tm st;
  tt = time(NULL);
  localtime_r( &tt, &st);
  st.tm_mon += 1;
  st.tm_year += 1900;
  snprintf( tx, 24, "%.2d/%.2d/%4d %.2d:%.2d:%.2d", st.tm_mday, st.tm_mon,
            st.tm_year, st.tm_hour, st.tm_min, st.tm_sec);
 }

bool check_rob( uint32_t * addr, uint32_t wsize)
 {
  bool is_good = false;
  VERIFY ( ROBFRAGMENTT, addr, wsize, is_good);
  return is_good;
 }

bool check_evt( uint32_t * addr, uint32_t wsize)
 {
  bool is_good = false;
  VERIFY ( EVTFRAGMENTT, addr, wsize, is_good);
  return is_good;
 }

bool fileReader::nextfile()
 {
  if (fd) delete fd;
  fd = 0;
  if (fileid == filelist.size())
   {
    if ((nevread > 0) && wraparound)
      fileid = 0;
    else
      return false;
   }

  const char * fname = filelist[fileid].c_str();
  ERR_MESS1 ("INFORMATION[fileReader::nextfile] - fname " << fname);

  uint32_t rm = alarm(TIMEOUT);
  try
   {
    fd = new fileIntf(fname);
   }
  catch (...)
   {
    ERR_MESS1 ("INFORMATION[fileReader::nextfile] - error " << fname);
    filelist.erase(filelist.begin()+fileid);
    return this->nextfile();
   }
  if ((rm > 0) && (rm <= NOTIFYPERIOD)) alarm(rm);

  uint32_t flevel = fd->flevel();

  if ((flevel == 0) || ((reqlevel != 0) && (reqlevel < flevel)))
   {
    ERR_MESS1 ("INFORMATION[fileReader::nextfile] - error " << fname
               << " reqlevel " << reqlevel << " flevel " << flevel);
    filelist.erase(filelist.begin()+fileid);
    return this->nextfile();
   }

  if (reqlevel == 0) reqlevel = flevel;

  ERR_MESS1 ("INFORMATION[fileReader::nextfile] - file # " << fileid+1
             << "/" << filelist.size() << " " << fname << " size " << fd->size()
             << " file level " << flevel << " - req. " << reqlevel);

  // robhdrsize = (reqlevel < RODLEVEL) ? fd->rbhsize() : 0;
  sidpos = 4;

  return true;
 }

uint32_t fileReader::nexttreeevent( uint32_t & wsize)
{
 uint32_t mk, ng=0, w, evpos;
 mk = marker;
 evpos = fd->next( mk, w);
 if (w == mk)
  {
   uint32_t tsize, sid;
   fd->readsize( tsize);
   ng = fd->readsid( sid);
   if (ng == DATALEN)
    {
     if ((reqsource) && (sid != reqsource))
      {
       evpos = fd->sseek( evpos+tsize*DATALEN);
       return this->nexttreeevent( wsize);
      }
     else
      {
       wsize = tsize;
       nevread ++;
       return evpos;
      }
    }
  }
   fileid ++;
   if ( this->nextfile() )
    {
     return this->nexttreeevent( wsize);
    }
 wsize = 0;
 return 0;
}

void hex ( uint32_t v)
 {
  uint32_t s[8];
  uint32_t i;
  std::cout << std::hex << " ";
  for (i=0; i<8; i++)
   {
    s[i] = v&0xf;
    v >>= 4;
   }
  do
    std::cout << s[--i];
  while(i);
 }

uint32_t fileReader::event_reader( uint32_t * * buffer, uint32_t & evsize)
{
  if ((nevmax > 0) && (nevread == nevmax))
   {
    evsize = 0;
    *buffer = NULL;
    return 0;
   }

  bool ev_good = false;
  uint32_t next, wsize, bsize;
  uint32_t * addr = bufferaddr;

  do
    {
      next = nexttreeevent( wsize);

      if (wsize == 0) break;			// end of file

      bsize = wsize * DATALEN;

      REALLOC( addr, bsize)

      fd->readb( addr, bsize);

      #ifdef DEBUGGG
      std::cout << "*** DEBUG PRINTOUT 1 ***" << std::endl;
      for (uint32_t i=0; i<16; i++)
       {
        hex(addr[i]);
        if ((i&3) == 3) std::cout << std::endl;
       }
      std::cout << "------------------------" << std::endl;
      #endif // DEBUGGG

      ev_good = (addr[3]==0xbeccabac) || (*(checker))( addr, wsize);

    }
  while (! ev_good);

  if (ev_good)
   {
    evsize = wsize - robhdrsize;
    *buffer = addr + robhdrsize;

    #ifdef DEBUGGG
    std::cout << "*** DEBUG PRINTOUT 2 ***" << std::endl;
    for (uint32_t i=0; i<16; i++)
     {
      hex((*buffer)[i]);
      if ((i&3) == 3) std::cout << std::endl;
     }
    std::cout << "------------------------" << std::endl;
    #endif // DEBUGGG

   }
  else 
   {
    evsize = 0;
    *buffer = NULL;
    if (fd) delete fd;
    fd = 0;
   }

  return evsize;
}

fileReader::fileReader(const char * filename, const uint32_t source_id,
                       const uint32_t & rlevel, const uint32_t & ev_max,
                       const bool & wrap_around, uint32_t & retcode)
{
  fd = 0;
  reqlevel = 0;
  reqsource = 0;
  nevread = 0;
  bufferaddr = NULL;
  buffersize = 0;
  marker = 0;
  robhdrsize = 0;
  sidpos = 0;
  nevmax = ev_max;
  wraparound = wrap_around;
  fileid = 0;
  filelist.clear();
  checker = NULL;

  ERR_MESS1 ("INFORMATION[fileReader] - file is " << filename << std::hex
             << " - level " << rlevel << " - source_id " << source_id
             << std::dec);

  retcode = 1;
  if (rlevel > 6) return;

  if ( strstr( filename, "list:") == filename )
   {
    std::ifstream xList( &filename[5] );
    std::string b;
    while ( xList >> b )
     {
      filelist.push_back(b);
     }
    xList.close();
   }
  else
   filelist.push_back( std::string(filename));

  reqsource = source_id;
  reqlevel = rlevel;

  fileid = 0;

  this->nextfile();

  retcode = 2;
  if ( filelist.size() == 0) return;

//  fileid = 0;
//  this->nextfile();
  ERR_MESS1 (" req level " << reqlevel << " - source id " << std::hex
             << source_id << std::dec);

  switch (reqlevel)
   {
    case  2:
	marker = EVTMARKER;
	checker = &check_evt;
	break;
    case  5:
    case  6:
	marker = ROBMARKER;
	checker = &check_rob;
	break;
    default:
	marker = 0;
	checker = NULL;
	break;
   }

  retcode = 0;
  return;
}

fileReader::~fileReader()
{
  if (fd) delete fd;
  if (bufferaddr) delete []bufferaddr;
  fd = 0;
  reqlevel = 0;
  reqsource = 0;
  nevread = 0;
  bufferaddr = NULL;
  buffersize = 0;
  marker = 0;
  robhdrsize = 0;
  sidpos = 0;
  nevmax = 0;
  wraparound = 0;
  fileid = 0;
  for (uint32_t i=0; i<filelist.size(); i++) filelist[i].clear();
  filelist.clear();
  checker = NULL;
}

