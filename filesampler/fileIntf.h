#ifndef _FILEINTF_H
#define _FILEINTF_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <unistd.h>
#include <string>

#define DATALEN (sizeof(uint32_t))

enum { LV1LEVEL=1, EVTLEVEL, DETLEVEL, ROSLEVEL, ROBLEVEL, RODLEVEL };

#define LV1MARKER 0x99123499
#define EVTMARKER 0xaa1234aa
#define DETMARKER 0xbb1234bb
#define ROSMARKER 0xcc1234cc
#define ROBMARKER 0xdd1234dd
#define RODMARKER 0xee1234ee

class fileIntf
 {
  protected:
   std::string file_name;
   struct stat file_info;
   int32_t file_des;
   uint32_t file_pos;
   uint32_t file_size;
   uint32_t file_level;
   uint32_t file_marker;
   // uint32_t rob_hsize;
   uint32_t evt_pos;
   uint32_t evt_hdr[5];
   bool getrods;
   uint32_t * rodbuffer;
   uint32_t rodbufsize;
   uint32_t rodevsize;
   uint32_t rodnwread;
   void readone( uint32_t & bufsize, uint32_t* & buffer, uint32_t & size);
 
  public:
   fileIntf( const char * filename);
   virtual ~fileIntf();
   virtual uint32_t next( const uint32_t mk, uint32_t & val);
   virtual uint32_t sseek( const uint32_t pos);
   virtual int32_t readsize( uint32_t & w);
   virtual int32_t readsid( uint32_t & w);
   virtual int32_t readb( const void * buff, const uint32_t nby);
   uint32_t flevel() { return file_level; };
   // uint32_t rbhsize() { return rob_hsize; };
   uint32_t size() { return file_size; };
 };

#endif // _FILEINTF_H
