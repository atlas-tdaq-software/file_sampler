#ifndef _FILEREADER_H
#define _FILEREADER_H

#include <stdint.h>
#include <string>
#include <vector>
#include "fileIntf.h"

#define TIMEOUT 300
#define NOTIFYPERIOD 90

class fileReader
 {
  private:
   fileIntf * fd;
   uint32_t reqlevel;
   uint32_t reqsource;
   uint32_t nevread;
   uint32_t * bufferaddr;
   size_t buffersize;
   uint32_t marker;
   uint32_t robhdrsize;
   uint32_t sidpos;
   uint32_t nevmax;
   bool wraparound;
   uint32_t fileid;
   std::vector<std::string> filelist;
   bool (* checker)( uint32_t * addr, uint32_t wsize);

   uint32_t fullevent_reader( uint32_t * * buffer, uint32_t & evsize);
   uint32_t subdetevent_reader( uint32_t * * buffer, uint32_t & evsize);
   uint32_t ROSevent_reader( uint32_t * * buffer, uint32_t & evsize);
   uint32_t ROBevent_reader( uint32_t * * buffer, uint32_t & evsize);
   uint32_t RODevent_reader( uint32_t * * buffer, uint32_t & evsize);
   uint32_t nexttreeevent( uint32_t & wsize);
   bool nextfile();

  public:
   fileReader(const char * filename, const uint32_t source_id,
              const uint32_t & rlevel, const uint32_t & ev_max,
              const bool & wrap_around, uint32_t & retcode);
   ~fileReader();
   uint32_t event_reader( uint32_t * * buffer, uint32_t & evsize);

 };

typedef uint32_t (fileReader::* readFn)( uint32_t * * buffer, uint32_t & evsize);
#endif // _FILEREADER_H
